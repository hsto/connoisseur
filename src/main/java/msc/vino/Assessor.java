package msc.vino; // used to be called gourmand - very confusing... changed to VINO for VIsual NOtation Assessment Theory

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.EstimatableObject;
import msc.connoisseur.model.LanguageSystem;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.bertin.LabelShape;
import msc.connoisseur.model.bertin.VisualVariable;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.language.semantics.Concept;
import msc.connoisseur.model.language.semantics.Elements;
import msc.connoisseur.model.language.semantics.GraphemeMapping;
import msc.connoisseur.model.language.syntax.Grapheme;
import msc.connoisseur.model.language.syntax.Graphemes;

public class Assessor {

    //Semiotic clarity from PoN
    public static double assess(Assessment assessment, ConnoisseurObject object){
        return assessment.getWeights().getWeight(object)*1d;
    }
    public static double assess(Assessment assessment){
        if(assessment.getScope()==null)return -1;
        return assessment.getScope().assess(assessment);
    }
    public static double assess(Assessment assessment, Container<? extends ConnoisseurObject> object){
        int weight = assessment.getWeights().getWeight((ConnoisseurObject) object);
        List<ConnoisseurObject> list = (List<ConnoisseurObject>) object.getList();
        double sum = 0d;//list.stream().collect(Collectors.summingDouble(x -> x.doAssess(msc.connoisseur.tests.assessment)));
        double number = 0d;
        for(ConnoisseurObject c : list) {
            sum += c.assess(assessment);
            number += assessment.getWeights().getWeight(c);
        }
        double number2 = object.getList().stream().mapToInt(x -> assessment.getWeights().getWeight(x)).sum();
        if(number != number2){
            throw new IllegalStateException("Number was " + number + " but number2 was " + number2);
        }
        return (weight * sum) / number;
    }

    public static double assess(Assessment assessment, GraphemeMapping graphemeMapping){
        Graphemes gs = graphemeMapping.getParent().getParent().getSyntax().getGraphemes();
        Elements cs = graphemeMapping.getParent().getElements();
        double visualized = graphemeMapping.getMappedValues().size();
        double meaningful = graphemeMapping.getMappedKeys().size();
        double graphemes = gs.getList().size();
        //if(graphemes==0)return "Error: No graphemes present";
        double concepts = cs.getConceptList().size();
        //if(concepts==0)return "Error: No concepts present";

        ToDoubleFunction<Concept> redundancy = c -> {
            Set<Grapheme> set = graphemeMapping.getMappedKeys(c);
            if(set.isEmpty())
                return 0D;
            else
                return 1D-(1D/set.size());
        };
        double symbolRedundancy = 0;
        if(visualized != 0d) {
            symbolRedundancy = (1D / visualized) *
                    cs.getConceptList().stream().collect(Collectors.summingDouble(redundancy));
        }
        ToDoubleFunction<Grapheme> overload = g ->{
            Set<Concept> set = graphemeMapping.getMappedValues(g);
            if(set.isEmpty())
                return 0D;
            else
                return 1D-(1D/set.size());
        };
        double symbolOverload = 0;
        if(meaningful != 0d) {
            symbolOverload = (1D/meaningful)*
                    gs.getList().stream().collect(Collectors.summingDouble(overload));
        }
        double symbolExcess = 0;
        if(graphemes != 0d)
            symbolExcess = 1D-(meaningful/graphemes);
        double symbolDeficit = 0;
        if(concepts != 0d)
            symbolDeficit = 1D-(visualized/concepts);
        return (assessment.getWeights().getWeight(graphemeMapping) * (1-(symbolOverload + symbolExcess + symbolDeficit + symbolRedundancy) / 4));
    }
    public static double assess(Assessment assessment, LanguageSystem ls) {
        double languageTotal = ls.getLanguages().stream().collect(Collectors.summingDouble(x -> x.assess(assessment)));
        double languageweights = ls.getLanguages().stream().collect(Collectors.summingDouble(x -> assessment.getWeights().getWeight(x)));
        double lRelationTotal = ls.getLanguageRelationship().assess(assessment);
        double lRelationWeight = assessment.getWeights().getWeight(ls.getLanguageRelationship());
        return assessment.getWeights().getWeight(ls)*((languageTotal+lRelationTotal)/(languageweights+lRelationWeight));
    }
    public static double assess(Assessment assessment, EstimatableObject eo){
        return eo.getEstimate();
    }
    public static double assess(Assessment assessment, Graphemes gs){
        if(gs.getList().isEmpty())return 0d;
        //lambda function which applies a function to all pairs and collects the sum
        Function<BiFunction<Grapheme,Grapheme,Double>,Double> toAllPairs = func -> {
            double sum = 0;
            for(int i = 0; i < gs.getList().size()-1; i++){
                for(int j = i+1; j < gs.getList().size(); j++){
                    sum += func.apply(gs.getList().get(i), gs.getList().get(j));
                }
            }
            return sum;
        };

        double visualDistance = visualDistance(assessment,gs, toAllPairs);
        double redundantCoding = redundantCoding(assessment,gs, toAllPairs);
        double perceptualPop = perceptualPop(assessment,gs);
        double textualDifferentiation = textualDifferentiation(assessment,gs,toAllPairs);
        return (visualDistance+redundantCoding+perceptualPop+textualDifferentiation)/4;
    }
    static int counter = 0;
    private static double visualDistance(Assessment assessment,Graphemes gs,
                                         Function<BiFunction<Grapheme, Grapheme, Double>, Double> toAllPairs) {
        counter++;
        BiFunction<Grapheme,Grapheme,Double> vd = (g1, g2) -> g1.similarity(assessment,g2);
        double sum = toAllPairs.apply(vd);
        int sizeMin1 = gs.getList().size();
        int comb = ((sizeMin1-1)*sizeMin1)/2;
        double visualDistance = 1d-sum/comb;

        return visualDistance;
    }
    private static double redundantCoding(Assessment assessment,Graphemes gs,
                                          Function<BiFunction<Grapheme, Grapheme, Double>, Double> toAllPairs) {
        double sum;
        BiFunction<Grapheme,Grapheme,Double> vr = (g1,g2) ->{
            VisualVariable[] vec1 = g1.getVisualVector().asArray();
            VisualVariable[] vec2 = g2.getVisualVector().asArray();
            double vrSum = 0;
            for(int i = 0; i < vec1.length; i++)
                vrSum += vec1[i].similarity(vec2[i])==1d
                        &&assessment.getWeights().getWeight(vec1[i].getClass()) != 0d
                        ?0:1;
            return vrSum/assessment.getWeights().getNumberOfEnabledVVs();
        };
        sum = toAllPairs.apply(vr);

        double redundantCoding = sum/Math.pow(gs.getList().size(), 2);
        return redundantCoding;
    }
    private static double perceptualPop(Assessment assessment,Graphemes gs) {
        double sum;
        boolean[][] uniquenessGrid = new boolean[gs.getList().size()][7];
        for(boolean[] sub : uniquenessGrid)
            Arrays.fill(sub, true);

        HashMap<Object, Grapheme> hm = new HashMap<>();
        for(int i = 0; i < uniquenessGrid.length; i++){
            Grapheme current = gs.getList().get(i);
            VisualVariable[] vector = current.getVisualVector().asArray();
            for(int j = 0; j < vector.length; j++){
                if(assessment.getWeights().getWeight(vector[j].getClass())>0d){
                    Grapheme other = hm.put(vector[j].getValue(), current);
                    if(other != null){
                        uniquenessGrid[gs.getList().indexOf(current)][j] = false;
                        uniquenessGrid[gs.getList().indexOf(other)][j] = false;
                    }
                }else{
                    uniquenessGrid[i][j] = false;
                }
            }
        }
        ToDoubleFunction<Grapheme> ppo = g -> {
            boolean hasUnique = true;
            for(int i = 0; i < 7; i++)
                hasUnique &= uniquenessGrid[gs.getList().indexOf(g)][i];
            return hasUnique?1d:0d;
        };
        sum = gs.getList().stream().collect(Collectors.summingDouble(ppo));
        double perceptualPop = sum/gs.getList().size();
        return perceptualPop;
    }
    private static double textualDifferentiation(Assessment assessment,Graphemes gs,
                                                 Function<BiFunction<Grapheme, Grapheme, Double>, Double> toAllPairs) {
        ArrayList<Grapheme> found = new ArrayList<>();
        BiFunction<Grapheme,Grapheme,Double> td = (g1,g2) -> {
            if(found.contains(g1)&&found.contains(g2)) return 0d;
            VisualVariable[] vec1 = g1.getVisualVector().asArray();
            VisualVariable[] vec2 = g2.getVisualVector().asArray();
            for(int i = 0; i < vec1.length; i++){
                if(assessment.getWeights().getWeight(vec1[i].getClass())>0)
                    if(!(vec1[i] instanceof LabelShape) && vec1[i].equals(vec2[i]))
                        return 0d;
            }
            found.add(g1);
            found.add(g2);
            return 1d;
        };
        toAllPairs.apply(td);
        double textualDifferentiation = found.size();
        return textualDifferentiation;
    }
}
