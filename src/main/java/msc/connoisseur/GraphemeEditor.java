package msc.connoisseur;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import msc.connoisseur.control.Controller;
import msc.connoisseur.control.CopyPasteController;
import msc.connoisseur.control.UndoRedoController;
import msc.connoisseur.view.EditorPanel;
import msc.connoisseur.view.utility.DragDropController;

import java.util.prefs.Preferences;

public class GraphemeEditor extends Application {
	private static GraphemeEditor instance;

	public static void main(String[] args){
		launch(args);
	}
	@Override
	public void start(Stage primaryStage) throws Exception {
		instance = this;

		Preferences preferences = Preferences.userRoot().node("Connoisseur");
		preferences.put("ConnoisseurVersion", "0.3");
		preferences.put("ConnoisseurDate", "2020-01-06");
		preferences.put("OnlineInfo", "https://www.pst.ifi.lmu.de/~stoerrle/");
		preferences.flush();

		StringBuilder sb = new StringBuilder();
		sb.append("Connoisseur v").append(preferences.get("ConnoisseurVersion", "0"));
		sb.append(" - ").append(preferences.get("ConnoisseurDate", ""));
		primaryStage.setTitle(sb.toString());
		//primaryStage.setTitle("Connoisseur v0.3 - 2020-01-05");

		primaryStage.getIcons().add(new Image(this.getClass().getResourceAsStream("/img/wine.png")));

		//injecting dependencies
		UndoRedoController undoRedoController = new UndoRedoController();
		Controller controller = new Controller(null,undoRedoController);
		DragDropController dragDropController = new DragDropController(controller);
		CopyPasteController copyPasteController = new CopyPasteController(controller);
		EditorPanel ep = new EditorPanel(controller, undoRedoController, dragDropController, copyPasteController);
		controller.registerEditor(ep);
		Scene scene = new Scene(ep,1200,800);

		String cssUrl = this.getClass().getResource("/style.css").toExternalForm();
		scene.getStylesheets().add(cssUrl);

		primaryStage.setScene(scene);
		primaryStage.setMaximized(preferences.getBoolean("StartMaximized", false));
		//primaryStage.setMaximized(true);
		primaryStage.show();
		if (preferences.getBoolean("StartOnRecent", false)) {
			controller.openLastFile();
		}
	}
	public static GraphemeEditor getInstance(){
		return instance;
	}

}
