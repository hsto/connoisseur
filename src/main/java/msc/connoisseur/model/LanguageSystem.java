package msc.connoisseur.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.language.Language;
import msc.vino.Assessor;

public class LanguageSystem extends ConnoisseurObject implements Container<ConnoisseurObject> {
	private List<Language> languages = new ArrayList<>();
	private LanguageRelationship languageRelationships;
	private ConnoisseurProject parent;

	public LanguageSystem(){
		setLanguageRelationship(new LanguageRelationship());
	}
	public void add(Language l){
		languages.add(l);
	}
	public void add(int index, Language language) {languages.add(index,language);}
	public void remove(Language l){
		languages.remove(l);
	}
	public List<Language> getLanguages() {
		return languages;
	}
	public void setLanguages(List<Language> languages){
		this.languages = languages;
	}
	public LanguageRelationship getLanguageRelationship() {
		return this.languageRelationships;
	}
	public void setLanguageRelationship(LanguageRelationship languageRelationships){
		this.languageRelationships = languageRelationships;
		languageRelationships.setParent(this);
	}
	@Override
	public LanguageSystem clone(){
		LanguageSystem ls = new LanguageSystem();
		ls.setDescription(getDescription());
		ls.setId(getId());
		ls.setLanguageRelationship(getLanguageRelationship().clone());
		ls.setLanguages(getLanguages());
		ls.setName(getName());
		return ls;
	}
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof LanguageSystem){
			this.setLanguageRelationship(((LanguageSystem) other).getLanguageRelationship().clone());
			this.setLanguages(((LanguageSystem) other).getLanguages());
		}
	}

    public void setParent(ConnoisseurProject parent){
		this.parent = parent;
	}
	public ConnoisseurProject getParent(){
		return this.parent;
	}
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,(ConnoisseurObject)this);
    }

	@Override
	public List<ConnoisseurObject> getList() {
		return Arrays.asList(getLanguageRelationship());
	}
}
