package msc.connoisseur.model.dialect;

import java.util.List;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.ConnoisseurProject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.DynamicContainer;
import msc.connoisseur.model.dialect.Dialect.Extension;
import msc.connoisseur.model.language.Language;
import msc.vino.Assessor;

public class Dialect extends ConnoisseurObject implements DynamicContainer<Extension> {
	public enum ExtensionType{addition,restriction}
	private Language base;
	private List<Extension> list;
	private ConnoisseurProject parent;
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,this);
    }
	@Override
	public Dialect clone() {
		Dialect clone = new Dialect();
		clone.setDescription(getDescription());
		clone.setId(getId());
		clone.setName(getName());
		clone.setBaseLanguage(getBaseLanguage());
		clone.setList(cloneList(x -> x.clone()));
		return clone;
	}
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Dialect){
			setList(((Dialect) other).cloneList(x -> x.clone()));
			setBaseLanguage(((Dialect) other).getBaseLanguage());
		}
	}

    //region getters/setters
	public Language getBaseLanguage(){
		return base;
	}
	public void setBaseLanguage(Language l){
		this.base = l;
	}
	public List<Extension> getList() {
		return list;
	}
	@Override
	public void setList(List<Extension> list) {
		this.list = list;
	}

	@Override
	public Class getContainedClass() {
		return Extension.class;
	}

	public void setParent(ConnoisseurProject cp){
		this.parent = cp;
	}
	@Override
	public ConnoisseurProject getParent() {
		return this.parent;
	}
	//endregion
	public class Extension{
		private Language owner;
		private ExtensionType eType;
		private ConnoisseurObject element;
		private String description;
		//region getters/setters
		public void setOwner(Language l){
			this.owner = l;
		}
		public void setEType(ExtensionType type){
			this.eType = type;
		}
		public void setElement(ConnoisseurObject element){
			this.element = element;
		}
		public void setDescription(String description){
			this.description = description;
		}
		public Language getOwner(){
			return owner;
		}
		public ExtensionType getEType(){
			return eType;
		}
		public ConnoisseurObject getElement(){
			return element;
		}
		public String getDescription(){
			return description;
		}
		//endregion
		@Override
		public Extension clone(){
			Extension clone = new Extension();
			clone.setDescription(this.getDescription());
			clone.setElement(this.getElement());
			clone.setEType(this.getEType());
			clone.setOwner(this.getOwner());
			return clone;
		}
	}
}
