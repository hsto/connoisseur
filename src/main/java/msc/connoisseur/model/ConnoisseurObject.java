package msc.connoisseur.model;

import msc.connoisseur.model.assessment.Assessment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.prefs.Preferences;

public abstract class ConnoisseurObject implements Serializable {
    private String name = "";
    private String id = "";
    private String description = "";
    private Preferences preferences = Preferences.userRoot().node("Connoisseur");

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public abstract ConnoisseurObject getParent();

    public final double assess(Assessment assessment) {
        visualQuality = doAssess(assessment);
        return visualQuality;
    }

    protected abstract double doAssess(Assessment assessment);

    private double visualQuality = 0d;

    public double getVisualQuality() {
        return visualQuality;
    }

    // don't mess with setScale - it's weird.
    public double getVisualQualityAsPercentage() {
        BigDecimal visualQualityPercentage = new BigDecimal(getVisualQuality()).multiply(new BigDecimal(100));
        BigDecimal visualQuality = visualQualityPercentage.setScale(2, RoundingMode.HALF_UP);//.doubleValue();
        return visualQuality.doubleValue();
    }

    @Override
    public String toString() {
        //return toFancyString(false);
        return toFancyString(preferences.getBoolean("ShowAssessment", false));
    }

    public String toStringWithValue() {
        return toFancyString(true);
    }

    private String toFancyString(boolean withValues) {
        StringBuilder fancyString = new StringBuilder();

        // show element type if desired
        if (preferences.getBoolean("ShowCOType", true)) {
            fancyString.append(this.getClass().getSimpleName()).append(" ");
        } // wird ausgeführt, aber Desription ist falsch. Sollte tool tipp sein!!

        // show name; if none, say "<unnamed>"
        String theName = getName();
        if (theName.isEmpty()) {
            fancyString.append("<unnamed>");
        } else {
            fancyString.append("\"").append(theName).append("\" ");
        } // korrekt

        // show id if available and desired
        String theID = getId();
        if (!theID.isEmpty() && preferences.getBoolean("ShowCOID", false)) {
            fancyString.append("(").append(theID).append(") ");
        } // korrekt

        // show assessment value if desired; replace 0.0 by --- for readability
        if (preferences.getBoolean("ShowAssessment", false)) {
            // Do not change this line! Other usages of setScale may not work as expected.
            BigDecimal visualQualityAssessment = new BigDecimal(getVisualQuality()).setScale(2, RoundingMode.HALF_UP);

            if (preferences.getBoolean("HideEmptyAssessment", false) && visualQualityAssessment.floatValue() == 0.0) {
                //fancyString.append("---");// indicates "zero" assessment (empty or missing)
            } else { // non-zero or shall show anyway
                fancyString.append("[").append(getVisualQualityAsPercentage()).append("%]");
                //fancyString.append(" [").append(visualQualityAssessment).append("]");
            }
        }
        return fancyString.toString();
    }

    public String toValueString() {
        BigDecimal bd = new BigDecimal(getVisualQuality()).setScale(2, RoundingMode.HALF_EVEN);
        return bd.toString();
    }

    public abstract ConnoisseurObject clone();

    public void loadInto(ConnoisseurObject other) {
        this.setDescription(other.getDescription());
        this.setId(other.getId());
        this.setName(other.getName());
    }
}
