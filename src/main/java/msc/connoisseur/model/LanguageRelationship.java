package msc.connoisseur.model;

import java.util.ArrayList;
import java.util.List;

import msc.connoisseur.model.LanguageRelationship.Relationship;
import msc.connoisseur.model.common.DynamicContainer;

public class LanguageRelationship extends EstimatableObject implements DynamicContainer<Relationship> {
	private LanguageSystem parent;
	private List<Relationship> list = new ArrayList<Relationship>();

	public LanguageRelationship() {
		setName("Language Relationship");
	}

	public void setParent(LanguageSystem parent) {
		this.parent = parent;
	}

	@Override
	public LanguageSystem getParent() {
		return this.parent;
	}

	class Relationship {
		public ConnoisseurObject dialect1;
		public ConnoisseurObject dialect2;
		public String name;
		public String relationship;
		public String parameter;
		public String comment;
	}

	public List<Relationship> getList() {
		return list;
	}

	@Override
	public void setList(List<Relationship> list) {
		this.list = list;
	}

	@Override
	public Class getContainedClass() {
		return Relationship.class;
	}

	@Override
	public LanguageRelationship clone() {
		LanguageRelationship lr = new LanguageRelationship();
		lr.setDescription(getDescription());
		lr.setEstimate(getEstimate());
		lr.setId(getId());
		lr.setName(getName());
		lr.setReason(getReason());
		return lr;
	}

}
