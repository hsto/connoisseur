package msc.connoisseur.model;

import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.common.DynamicContainer;
import msc.connoisseur.model.dialect.Dialect;
import msc.connoisseur.model.intent.Intent;
import msc.connoisseur.model.language.Language;
import msc.vino.Assessor;

import java.util.ArrayList;
import java.util.List;

public class ConnoisseurProject extends ConnoisseurObject implements DynamicContainer<ConnoisseurObject> {
	//language, dialect, intent, language system, msc.connoisseur.tests.assessment
	List<ConnoisseurObject> list = new ArrayList<>();
	@Override
	public void add(ConnoisseurObject object){
		if(object instanceof LanguageSystem) ((LanguageSystem) object).setParent(this);
		else if(object instanceof Language) ((Language) object).setParent(this);
		else if(object instanceof Intent) ((Intent) object).setParent(this);
		else if(object instanceof Dialect) ((Dialect) object).setParent(this);
		else if(object instanceof Assessment) ((Assessment) object).setParent(this);
		list.add(object);
	}
	@Override
	public List<ConnoisseurObject> getList() {
		return list;
	}

	//@Override
	public void appendList(List<ConnoisseurObject> newList) {
		list.addAll(newList);
	}

	@Override
	public void setList(List<ConnoisseurObject> list) {
		this.list = list;
	}

	@Override
	public Class getContainedClass() {
		return ConnoisseurObject.class;
	}

	@Override
	public ConnoisseurObject getParent() {
		return null;
	}

	@Override
	public double doAssess(Assessment assessment) {
		return Assessor.assess(assessment,(Container)this);
	}

	@Override
	public ConnoisseurProject clone() {
		ConnoisseurProject clone = new ConnoisseurProject();
		clone.setList(cloneList(x -> x.clone()));
		clone.setDescription(getDescription());
		clone.setId(getId());
		clone.setName(getName());
		return clone;
	}

}
