package msc.connoisseur.model.visualvector;

import java.io.Serializable;

import msc.connoisseur.model.bertin.Brightness;
import msc.connoisseur.model.bertin.Color;
import msc.connoisseur.model.bertin.Orientation;
import msc.connoisseur.model.bertin.Position;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.bertin.Size;
import msc.connoisseur.model.bertin.Size.SizeValue;
import msc.connoisseur.model.bertin.Texture;
import msc.connoisseur.model.bertin.UndefinedShape;
import msc.connoisseur.model.bertin.VisualVariable;

public class VisualVector implements Serializable {
	protected Shape shape;
	protected Brightness brightness;
	protected Color color;
	protected Orientation orientation;
	protected Position position;
	protected Size size;
	public VisualVector(){
		setColor(new Color(0xff,0xff,0xff));
		setBrightness(new Brightness(getColor().getBri()));
		setOrientation(new Orientation(0));
		setPosition(new Position());
		setSize(new Size(SizeValue.average,null));
		setShape(new UndefinedShape());
	}
	public Shape getShape(){
		return shape;
	}
	public void setShape(Shape s){
		shape = s;
	}
	public Brightness getBrightness(){
		return brightness;
	}
	public void setBrightness(Brightness b){
		brightness = b;
	}
	public Color getColor(){
		return color;
	}
	public void setColor(Color c){
		color = c;
	}
	public Orientation getOrientation(){
		return orientation;
	}
	public void setOrientation(Orientation o){
		orientation = o;
	}
	public Position getPosition(){
		return position;
	}
	public void setPosition(Position p){
		position = p;
	}
	public Size getSize(){
		return size;
	}
	public void setSize(Size s){
		size = s;
	}
	public Texture getTexture(){
		return shape.getTexture();
	}
	@Override
	public VisualVector clone(){
		VisualVector v = new VisualVector();
		v.setBrightness(getBrightness().clone());
		v.setColor(getColor().clone());
		v.setOrientation(getOrientation().clone());
		v.setPosition(getPosition().clone());
		v.setShape(getShape().clone());
		v.setSize(getSize().clone());
		return v;
	}
	public VisualVariable[] asArray(){
		return new VisualVariable[]{shape,brightness,color,orientation,position,size,shape.getTexture()};
	}
}
