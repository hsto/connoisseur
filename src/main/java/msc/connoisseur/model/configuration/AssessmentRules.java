package msc.connoisseur.model.configuration;

import java.util.ArrayList;
import java.util.function.Function;

import msc.connoisseur.model.configuration.AggregationRules.AggregationRule;
import msc.connoisseur.model.visualvector.VisualVector;

public class AssessmentRules {
	private AggregationRule aggregationRule = AggregationRules.getDefault();

	public Function<ArrayList<VisualVector>,VisualVector> getAggregationFunction(){
		return AggregationRules.getFunction(aggregationRule);
	}
	public AggregationRule getAggregationRule(){
		return this.aggregationRule;
	}
	public void setAggregationRule(AggregationRule aggregationRule) {
		this.aggregationRule = aggregationRule;
	}

	public AggregationRule[] getAggregationRules(){
		return AggregationRules.AggregationRule.values();
	}
	@Override
	public AssessmentRules clone() {
		AssessmentRules r = new AssessmentRules();
		r.setAggregationRule(getAggregationRule());
		return r;
	}
}
