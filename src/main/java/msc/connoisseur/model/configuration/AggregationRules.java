package msc.connoisseur.model.configuration;

import java.util.ArrayList;
import java.util.function.Function;

import msc.connoisseur.model.visualvector.VisualVector;

public class AggregationRules {
	public enum AggregationRule{biggest/*,average*/}
	public static AggregationRule getDefault() {
		return AggregationRule.biggest;
	}
	public static Function<ArrayList<VisualVector>, VisualVector> getFunction(AggregationRule rule){
		switch(rule){
		case biggest:
			return x -> getBiggest(x);
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	private static VisualVector getBiggest(ArrayList<VisualVector> x) {
		ArrayList<VisualVector> list = (ArrayList<VisualVector>) x.clone();
		Function<VisualVector,Integer> vectorSize = vector -> vector.getSize().getSizeValue().ordinal();
		list.sort((first,second)->Integer.compare(vectorSize.apply(first), vectorSize.apply(second)));
		return list.get(0);
	}
}
