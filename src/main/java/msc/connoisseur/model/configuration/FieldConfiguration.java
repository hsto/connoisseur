package msc.connoisseur.model.configuration;

import msc.connoisseur.model.bertin.Brightness;
import msc.connoisseur.model.bertin.Color;
import msc.connoisseur.model.bertin.Orientation;
import msc.connoisseur.model.bertin.Position;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.bertin.Size;
import msc.connoisseur.model.bertin.Texture;
import msc.connoisseur.model.bertin.VisualVariable;

public class FieldConfiguration {
	private boolean brightnessEnabled = true;
	private boolean colorEnabled = true;
	private boolean orientationEnabled = true;
	private boolean positionEnabled = true;
	private boolean shapeEnabled = true;
	private boolean sizeEnabled = true;
	private boolean textureEnabled = true;
	public boolean isBrightnessEnabled() {
		return brightnessEnabled;
	}
	public boolean isColorEnabled() {
		return colorEnabled;
	}
	public boolean isOrientationEnabled() {
		return orientationEnabled;
	}
	public boolean isPositionEnabled() {
		return positionEnabled;
	}
	public boolean isShapeEnabled() {
		return shapeEnabled;
	}
	public boolean isSizeEnabled() {
		return sizeEnabled;
	}
	public boolean isTextureEnabled() {
		return textureEnabled;
	}
	public void setBrightnessEnabled(boolean brightnessEnabled) {
		this.brightnessEnabled = brightnessEnabled;
	}
	public void setColorEnabled(boolean colorEnabled) {
		this.colorEnabled = colorEnabled;
	}
	public void setOrientationEnabled(boolean orientationEnabled) {
		this.orientationEnabled = orientationEnabled;
	}
	public void setPositionEnabled(boolean positionEnabled) {
		this.positionEnabled = positionEnabled;
	}
	public void setShapeEnabled(boolean shapeEnabled) {
		this.shapeEnabled = shapeEnabled;
	}
	public void setSizeEnabled(boolean sizeEnabled) {
		this.sizeEnabled = sizeEnabled;
	}
	public void setTextureEnabled(boolean textureEnabled) {
		this.textureEnabled = textureEnabled;
	}
	@Override
	public FieldConfiguration clone() {
		FieldConfiguration fc = new FieldConfiguration();
		fc.setBrightnessEnabled(isBrightnessEnabled());
		fc.setColorEnabled(isColorEnabled());
		fc.setOrientationEnabled(isOrientationEnabled());
		fc.setPositionEnabled(isPositionEnabled());
		fc.setShapeEnabled(isShapeEnabled());
		fc.setSizeEnabled(isSizeEnabled());
		fc.setTextureEnabled(isTextureEnabled());
		return fc;
	}
	public boolean isEnabled(VisualVariable v) {
		if(v instanceof Brightness)return isBrightnessEnabled();
		if(v instanceof Color)return isColorEnabled();
		if(v instanceof Shape)return isShapeEnabled();
		if(v instanceof Orientation)return isOrientationEnabled();
		if(v instanceof Position)return isPositionEnabled();
		if(v instanceof Size) return isSizeEnabled();
		if(v instanceof Texture) return isTextureEnabled();
		return false;
	}
	public int enabledFields(){
		int counter = 0;
		counter += isBrightnessEnabled()?1:0;
		counter += isColorEnabled()?1:0;
		counter += isShapeEnabled()?1:0;
		counter += isOrientationEnabled()?1:0;
		counter += isPositionEnabled()?1:0;
		counter += isSizeEnabled()?1:0;
		counter += isTextureEnabled()?1:0;
		return counter;
	}
}
