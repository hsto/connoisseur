package msc.connoisseur.model.configuration;

public class Configuration {
	private Weights weights = new Weights();
	private FieldConfiguration fieldConfiguration = new FieldConfiguration();
	private AssessmentRules assessmentRules = new AssessmentRules();
	public Weights getWeights() {
		return weights;
	}
	public FieldConfiguration getFieldConfiguration() {
		return fieldConfiguration;
	}
	public AssessmentRules getAssessmentRules() {
		return assessmentRules;
	}
	public void setWeights(Weights weights) {
		this.weights = weights;
	}
	public void setFieldConfiguration(FieldConfiguration fieldConfiguration) {
		this.fieldConfiguration = fieldConfiguration;
	}
	public void setAssessmentRules(AssessmentRules assessmentRules) {
		this.assessmentRules = assessmentRules;
	}


}
