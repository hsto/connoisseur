package msc.connoisseur.model.configuration;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.bertin.*;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

import static msc.connoisseur.model.configuration.Weights.Variable.*;

public class Weights implements Serializable{
	private HashMap<Class,Integer> weightMap = new HashMap<>();
    private HashMap<Variable,Integer> visualVariableWeightMap = new HashMap<>();

    public Weights(){
        initializeVariableWeights();
    }
    public void setWeight(ConnoisseurObject object, int weight){
        weightMap.put(object.getClass(),weight);
    }
    public void setWeight(Class<? extends VisualVariable> object, int weight){
        visualVariableWeightMap.put(mapping(object),weight);
    }
    public int getWeight(ConnoisseurObject object){
        if(!weightMap.containsKey(object.getClass()))
            weightMap.put(object.getClass(),1);
        return weightMap.get(object.getClass());
    }
    public int getWeight(Class<? extends VisualVariable> object){
        if(!visualVariableWeightMap.containsKey(mapping(object)))
            visualVariableWeightMap.put(mapping(object),1);
        return visualVariableWeightMap.get(mapping(object));
    }

    public int getVVSum() {
        int sum = visualVariableWeightMap.values().stream().collect(Collectors.summingInt(x -> x));
        return sum;
    }

    public int getNumberOfEnabledVVs() {
        return visualVariableWeightMap.values().stream().collect(Collectors.summingInt(x->x>0?1:0));
    }
    enum Variable{shape,position,orientation,texture,size,color,brightness}
    private Variable mapping(Class<? extends VisualVariable> c){
        if(Shape.class.isAssignableFrom(c)) return shape;
        else if(c.isAssignableFrom(Position.class)) return position;
        else if(c.isAssignableFrom(Orientation.class)) return orientation;
        else if(Texture.class.isAssignableFrom(c)) return texture;
        else if(c.isAssignableFrom(Size.class)) return size;
        else if(c.isAssignableFrom(Color.class)) return color;
        else if(c.isAssignableFrom(Brightness.class)) return brightness;
        else return null;
    }
    private void initializeVariableWeights(){
        Arrays.stream(Variable.values()).forEach(x -> visualVariableWeightMap.put(x,1));
    }
}
