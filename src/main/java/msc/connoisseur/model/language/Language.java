package msc.connoisseur.model.language;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.ConnoisseurProject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.language.pragmatics.Pragmatics;
import msc.connoisseur.model.language.semantics.Semantics;
import msc.connoisseur.model.language.syntax.Syntax;
import msc.vino.Assessor;

import java.util.Arrays;
import java.util.List;

public class Language extends ConnoisseurObject implements Container<ConnoisseurObject> {
	private Semantics semantics;
	private Pragmatics pragmatics;
	private Syntax syntax;
	private ConnoisseurProject parent;
	public Language(){
		setSemantics(new Semantics());
		setPragmatics(new Pragmatics());
		setSyntax(new Syntax());
	}

	public Semantics getSemantics() {
		return this.semantics;
	}
	public Pragmatics getPragmatics() {
		return this.pragmatics;
	}
	public Syntax getSyntax() {
		return this.syntax;
	}

	public void setSemantics(Semantics semantics) {
		this.semantics = semantics;
		semantics.setParent(this);
	}
	public void setPragmatics(Pragmatics pragmatics) {
		this.pragmatics = pragmatics;
		pragmatics.setParent(this);
	}
	public void setSyntax(Syntax syntax) {
		this.syntax = syntax;
		syntax.setParent(this);
	}
	@Override
	public Language clone(){
		Language l = new Language();
		l.setDescription(getDescription());
		l.setId(getId());
		l.setName(getName());
		l.setPragmatics(getPragmatics().clone());
		l.setSemantics(getSemantics().clone());
		l.setSyntax(getSyntax().clone());
		return l;
	}
	@Override
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Language){
			this.setPragmatics(((Language) other).getPragmatics().clone());
			this.setSemantics(((Language) other).getSemantics().clone());
			this.setSyntax(((Language) other).getSyntax().clone());
		}
	}

    public void setParent(ConnoisseurProject parent) {
		this.parent = parent;
	}
	@Override
	public ConnoisseurProject getParent(){
		return this.parent;
	}
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,(Container<? extends ConnoisseurObject>) this);
    }

	@Override
	public List<ConnoisseurObject> getList() {
		return Arrays.asList(getSyntax(),getSemantics(),getPragmatics());
	}
}
