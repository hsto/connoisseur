package msc.connoisseur.model.language.pragmatics;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.EstimatableObject;

public class Conventions extends EstimatableObject {
	private Pragmatics parent;
	private String layout;
	private String rules;
	private String labels;
	public Conventions(){
		setName("Conventions");
	}
	@Override
	public Conventions clone(){
		Conventions clone = new Conventions();
		clone.setDescription(getDescription());
		clone.setEstimate(getEstimate());
		clone.setId(getId());
		clone.setName(getName());
		clone.setReason(getReason());
		clone.setLabels(getLabels());
		clone.setLayout(getLayout());
		clone.setRules(getRules());
		return clone;
	}
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Conventions){
			setLayout(((Conventions) other).getLayout());
			setLabels(((Conventions) other).getLabels());
			setRules(((Conventions) other).getRules());
		}
	}

    //region getters/setters
	public void setParent(Pragmatics parent){
		this.parent = parent;
	}
	@Override
	public Pragmatics getParent() {
		return this.parent;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}
	//endregion
}
