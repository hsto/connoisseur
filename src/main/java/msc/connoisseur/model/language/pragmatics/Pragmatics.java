package msc.connoisseur.model.language.pragmatics;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.language.Language;
import msc.vino.Assessor;

import java.util.Arrays;
import java.util.List;

public class Pragmatics extends ConnoisseurObject implements Container<ConnoisseurObject> {
	private Conventions conventions = new Conventions();
	private SupportedUsages supportedUsages = new SupportedUsages();
	private Language parent;
	public Pragmatics(){
		setName("Pragmatics");
	}
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,(Container<? extends ConnoisseurObject>) this);
    }
	@Override
	public Pragmatics clone(){
		Pragmatics clone = new Pragmatics();
		clone.setDescription(getDescription());
		clone.setId(getId());
		clone.setName(getName());
		clone.setConventions(getConventions().clone());
		clone.setSupportedUsages(getSupportedUsages().clone());
		return clone;
	}
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Pragmatics) {
			this.setSupportedUsages(((Pragmatics) other).getSupportedUsages().clone());
			this.setConventions(((Pragmatics) other).getConventions().clone());
		}
	}

    //region getters/setters
	public void setParent(Language parent){
		this.parent = parent;
	}
	@Override
	public Language getParent() {
		return this.parent;
	}
	public Conventions getConventions() {
		return conventions;
	}
	public void setConventions(Conventions conventions) {
		this.conventions = conventions;
		conventions.setParent(this);
	}
	public SupportedUsages getSupportedUsages() {
		return supportedUsages;
	}
	public void setSupportedUsages(SupportedUsages supportedUsages) {
		this.supportedUsages = supportedUsages;
		supportedUsages.setParent(this);
	}

	@Override
	public List<ConnoisseurObject> getList() {
		return Arrays.asList(getConventions(),getSupportedUsages());
	}
	//endregion
}
