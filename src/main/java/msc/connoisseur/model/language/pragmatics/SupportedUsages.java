package msc.connoisseur.model.language.pragmatics;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.EstimatableObject;
import msc.connoisseur.model.common.GroupSize;
import msc.connoisseur.model.common.Interactivity;
import msc.connoisseur.model.common.Persistence;

public class SupportedUsages extends EstimatableObject{
	private Interactivity interactivity;
	private Persistence persistence;
	private GroupSize groupSize;
	private Pragmatics parent;
	public SupportedUsages(){
		setName("Supported Usages");
	}
	@Override
	public SupportedUsages clone(){
		SupportedUsages clone = new SupportedUsages();
		clone.setDescription(getDescription());
		clone.setEstimate(getEstimate());
		clone.setId(getId());
		clone.setName(getName());
		clone.setReason(getReason());
		return clone;
	}
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof SupportedUsages){
			this.setGroupSize(((SupportedUsages) other).getGroupSize());
			this.setInteractivity(((SupportedUsages) other).getInteractivity());
			this.setPersistence(((SupportedUsages) other).getPersistence());
		}
	}

    //region getters/setters
	public void setParent(Pragmatics parent){
		this.parent = parent;
	}
	@Override
	public Pragmatics getParent() {
		return this.parent;
	}

	public Interactivity getInteractivity() {
		return interactivity;
	}

	public void setInteractivity(Interactivity interactivity) {
		this.interactivity = interactivity;
	}

	public Persistence getPersistence() {
		return persistence;
	}

	public void setPersistence(Persistence persistence) {
		this.persistence = persistence;
	}

	public GroupSize getGroupSize() {
		return groupSize;
	}

	public void setGroupSize(GroupSize groupSize) {
		this.groupSize = groupSize;
	}
	//endregion
}
