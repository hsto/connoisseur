package msc.connoisseur.model.language.semantics;

import java.util.ArrayList;
import java.util.List;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.DynamicContainer;

public class Elements extends ConnoisseurObject implements DynamicContainer<ConnoisseurObject> {
	private List<Concept> conceptList = new ArrayList<>();
	private List<Domain> domainList = new ArrayList<>();
	private Semantics parent;
	public Elements(){
		setName("Elements");
	}


	@Override
	public void add(ConnoisseurObject c) {
		if(c instanceof Concept){
			conceptList.add((Concept) c);
			((Concept) c).setParent(this);
		}else if( c instanceof Domain){
			domainList.add((Domain) c);
			((Domain) c).setParent(this);
		}else{
			throw new ClassCastException("Element neither instance of Concept nor Domain");
		}
	}

	@Override
	public void add(int index, ConnoisseurObject c) {
		if(c instanceof Concept){
			conceptList.add(index,(Concept) c);
			((Concept) c).setParent(this);
		}else if( c instanceof Domain){
			domainList.add(index,(Domain) c);
			((Domain) c).setParent(this);
		}else{
			throw new ClassCastException("Element neither instance of Concept nor Domain");
		}
	}
	@Deprecated
	@Override
	public void setList(List<ConnoisseurObject> list) {
		conceptList.clear();
		domainList.clear();
		for(ConnoisseurObject c : list){
			add(c);
		}
	}

	@Override
	public Class getContainedClass() {
		return Concept.class;
	}


	@Override
	public Elements clone(){
		Elements c = new Elements();
		c.setDescription(getDescription());
		c.setId(getId());
		c.setName(getName());
		c.setList(this.cloneList(x -> x.clone()));
		return c;
	}

    public void cloneInto(Elements cs){
		this.setDescription(cs.getDescription());
		this.setId(cs.getId());
		this.setName(cs.getName());
		this.setList(cs.cloneList(x -> x.clone()));
	}
	public void setParent(Semantics parent){
		this.parent = parent;
	}
	@Override
	public Semantics getParent() {
		return this.parent;
	}

    @Override
	public double doAssess(Assessment assessment) {
		return 1d*assessment.getWeights().getWeight(this);
	}
    @Deprecated
	@Override
	public List<ConnoisseurObject> getList() {
		List<ConnoisseurObject> list = new ArrayList<>();
		list.addAll(conceptList);
		list.addAll(domainList);
		return list;
	}
    public List<Domain> getDomainList(){
		return domainList;
	}
	public List<Concept> getConceptList(){
		return conceptList;
	}
}
