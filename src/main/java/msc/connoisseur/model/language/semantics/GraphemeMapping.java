package msc.connoisseur.model.language.semantics;

import java.util.ArrayList;
import java.util.List;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.MappingContainer;
import msc.connoisseur.model.common.Pair;
import msc.connoisseur.model.language.syntax.Grapheme;
import msc.vino.Assessor;

public class GraphemeMapping extends ConnoisseurObject implements MappingContainer<Grapheme,Concept>{

	private List<Pair<Grapheme,Concept>> map = new ArrayList<>();
	private Semantics parent;

	public GraphemeMapping(){
		setName("GraphemeMapping");
	}


	public List<Pair<Grapheme,Concept>> getList(){
		return map;
	}
	public void setList(List<Pair<Grapheme,Concept>> map){
		this.map = map;
	}
	public void setParent(Semantics parent){
		this.parent = parent;
	}
	@Override
	public Semantics getParent() {
		return this.parent;
	}
	@Override
	public GraphemeMapping clone() {
		GraphemeMapping clone = new GraphemeMapping();
		clone.setName(getName());
		clone.setDescription(getDescription());
		clone.setId(getId());
		clone.setList(cloneList(x->new Pair<>(x.a,x.b)));
		return clone;
	}

    @Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,this);
    }
}
