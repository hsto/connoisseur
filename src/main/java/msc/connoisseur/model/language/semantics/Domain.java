package msc.connoisseur.model.language.semantics;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.vino.Assessor;

/**
 * Created by jeppe on 28-09-2016.
 */
public class Domain extends ConnoisseurObject {
    private Elements parent;
    private String reference;
    @Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,this);
    }

    @Override
    public Domain clone() {
        Domain d = new Domain();
        d.setName(this.getName());
        d.setDescription(this.getDescription());
        d.setId(this.getId());
        d.setReference(this.getReference());
        return d;
    }

    @Override
    public Elements getParent() {
        return this.parent;
    }
    public void setParent(Elements parent) {
        this.parent = parent;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
