package msc.connoisseur.model.language.semantics;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.vino.Assessor;


public class Concept extends ConnoisseurObject {
	private Elements parent;
	private String reference;
	public Concept(){
		setName("Concept");
	}
	public Concept clone(){
		Concept c = new Concept();
		c.setDescription(getDescription());
		c.setId(getId());
		c.setName(getName());
		c.setReference(this.getReference());
		return c;
	}

    public void setParent(Elements parent){
		this.parent = parent;
	}
	@Override
	public Elements getParent() {
		return this.parent;
	}
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,this);
    }

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
}
