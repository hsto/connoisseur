package msc.connoisseur.model.language.semantics;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.language.Language;
import msc.vino.Assessor;

import java.util.Arrays;
import java.util.List;

public class Semantics extends ConnoisseurObject implements Container<ConnoisseurObject> {
	private Elements elements;
	private GraphemeMapping graphemeMapping;
	private DomainMapping domainMapping;
	private Language parent;
	public Semantics(){
		setElements(new Elements());
		setGraphemeMapping(new GraphemeMapping());
		setDomainMapping(new DomainMapping());
		setName("Semantics");
	}
	public Elements getElements() {
		return elements;
	}
	public GraphemeMapping getGraphemeMapping() {
		return graphemeMapping;
	}
	public void setElements(Elements elements) {
		this.elements = elements;
		elements.setParent(this);
	}
	public void setGraphemeMapping(GraphemeMapping graphemeMapping) {
		this.graphemeMapping = graphemeMapping;
		graphemeMapping.setParent(this);
	}
	@Override
	public Semantics clone(){
		Semantics s = new Semantics();
		s.setDescription(getDescription());
		s.setId(getId());
		s.setName(getName());
		s.setElements(getElements().clone());
		//the mappings cannot be cloned since we don't know whether the corresponding elements are available at the destination
		return s;
	}
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Semantics){
			this.setElements(((Semantics) other).getElements().clone());
		}
	}

    public void setParent(Language parent){
		this.parent = parent;
	}
	@Override
	public Language getParent() {
		return this.parent;
	}
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,(Container<? extends ConnoisseurObject>) this);
    }

	public DomainMapping getDomainMapping() {
		return domainMapping;
	}

	public void setDomainMapping(DomainMapping domainMapping) {
		this.domainMapping = domainMapping;
		this.domainMapping.setParent(this);
	}

	@Override
	public List<ConnoisseurObject> getList() {
		return Arrays.asList(getElements(),getGraphemeMapping(),getDomainMapping());
	}
}
