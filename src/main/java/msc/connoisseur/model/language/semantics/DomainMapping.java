package msc.connoisseur.model.language.semantics;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.MappingContainer;
import msc.connoisseur.model.common.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jeppe on 28-09-2016.
 */
public class DomainMapping extends ConnoisseurObject implements MappingContainer<Domain, Concept> {
    List<Pair<Domain,Concept>> list = new ArrayList<>();
    private Semantics parent;

    @Override
    public double doAssess(Assessment assessment) {
        return 1d*assessment.getWeights().getWeight(this);
    }

    @Override
    public DomainMapping clone() {
        return null;
    }

    @Override
    public List<Pair<Domain, Concept>> getList() {
        return list;
    }

    @Override
    public void setList(List<Pair<Domain, Concept>> list) {
        this.list = list;
    }

    @Override
    public Semantics getParent() {
        return this.parent;
    }
    public void setParent(Semantics parent) {
        this.parent = parent;
    }
}
