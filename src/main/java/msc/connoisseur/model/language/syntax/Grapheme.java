package msc.connoisseur.model.language.syntax;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.bertin.*;
import msc.connoisseur.model.configuration.Weights;
import msc.connoisseur.model.visualvector.VisualVector;
import msc.vino.Assessor;

public abstract class Grapheme extends ConnoisseurObject {
	private ConnoisseurObject parent;
	public Grapheme(){
		super();
	}
	public double similarity(Assessment assessment, Grapheme g2){
		Weights w = assessment.getWeights();
		VisualVector other = g2 instanceof CompoundGrapheme
				?assessment.aggregate((CompoundGrapheme) g2)
				:g2.getVisualVector();
		VisualVector vector = getVisualVector();
		return comparison(w, vector, other);
	}

	protected double comparison(Weights w, VisualVector vector, VisualVector other) {
		double sum = 0;
		sum += vector.getBrightness().similarity( other.getBrightness())*
				w.getWeight(Brightness.class);
		sum += vector.getColor().similarity( other.getColor())*
                w.getWeight(Color.class);
		sum += vector.getOrientation().similarity(other.getOrientation())*
                w.getWeight(Orientation.class);
		sum += vector.getPosition().similarity(other.getPosition())*
                w.getWeight(Position.class);
		sum += vector.getShape().similarity(other.getShape())*
                w.getWeight(Shape.class);
		sum += vector.getSize().similarity(other.getSize())*
                w.getWeight(Size.class);
		sum += vector.getTexture().similarity(other.getTexture())*
                w.getWeight(Texture.class);
		return sum/w.getVVSum();
	}

	public void setParent(ConnoisseurObject parent){
		this.parent = parent;
	}
	public ConnoisseurObject getParent(){
		return this.parent;
	}
	public abstract VisualVector getVisualVector();
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,this);
    }
}
