package msc.connoisseur.model.language.syntax;

import msc.connoisseur.model.EstimatableObject;

public class Diagrams extends EstimatableObject{
	private Syntax parent;
	public Diagrams(){
		setName("Diagrams");
	}
	@Override
	public Diagrams clone(){
		Diagrams clone = new Diagrams();
		clone.setDescription(getDescription());
		clone.setEstimate(getEstimate());
		clone.setId(getId());
		clone.setName(getName());
		clone.setReason(getReason());
		return clone;
	}

    @Override
	public Syntax getParent() {
		return this.parent;
	}
	public void setParent(Syntax parent){
		this.parent = parent;
	}
}
