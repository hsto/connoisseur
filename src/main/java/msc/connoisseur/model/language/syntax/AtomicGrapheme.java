package msc.connoisseur.model.language.syntax;

import javafx.util.Pair;
import msc.connoisseur.model.bertin.Position;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.bertin.Size.SizeValue;
import msc.connoisseur.model.visualvector.VisualVector;

import java.util.List;

public class AtomicGrapheme extends Grapheme {
	VisualVector vv;
	public AtomicGrapheme(){
		super();
		vv = new VisualVector();
	}
	public AtomicGrapheme(VisualVector vv) {
		super();
		this.vv = vv;
	}
	@Override
	public VisualVector getVisualVector(){
		return vv;
	}
	public void setVisualVector(VisualVector vv){
		this.vv = vv;
	}
	public void setBrightness(Float brightness) {
		vv.getBrightness().setValue(brightness);
	}
	public void setColor(double hue, double sat) {
		vv.getColor().setHue((float)hue);
		vv.getColor().setSat((float)sat);
	}
	public void setOrientation(int degrees) {
		vv.getOrientation().setValue(degrees);

	}
	public void setTexture(String textureString) {
		vv.getTexture().setValue(textureString);

	}
	public void setPosition(List<Position.CardinalDirection> position, Grapheme reference) {
		vv.getPosition().setValue(new Pair<>(position,reference));

	}
	public void setSize(SizeValue value, Grapheme reference) {
		vv.getSize().setSizeValue(value);
		vv.getSize().setRelation(reference);

	}
	public void setShape(Shape shape){
		vv.setShape(shape);
	}
	@Override
	public AtomicGrapheme clone(){
		AtomicGrapheme g = new AtomicGrapheme();
		g.setDescription(getDescription());
		g.setId(getId());
		g.setName(getName());
		g.setVisualVector(getVisualVector().clone());
		return g;
	}

    public void clone(AtomicGrapheme g){
		this.setVisualVector(g.getVisualVector().clone());
		this.setDescription(g.getDescription());
		this.setId(g.getId());
		this.setName(g.getName());
	}
}
