package msc.connoisseur.model.language.syntax;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.language.Language;
import msc.vino.Assessor;

import java.util.Arrays;
import java.util.List;

public class Syntax extends ConnoisseurObject implements Container<ConnoisseurObject> {
	private Graphemes graphemes;
	private Rules rules;
	private Diagrams diagrams;
	private Language parent;
	public Syntax(){
		setName("Syntax");
		setGraphemes(new Graphemes());
		setRules(new Rules());
		setDiagrams(new Diagrams());
	}
	public Rules getRules() {
		return rules;
	}
	public void setRules(Rules rules) {
		this.rules = rules;
		rules.setParent(this);
	}
	public Diagrams getDiagrams() {
		return diagrams;
	}
	public void setDiagrams(Diagrams diagrams) {
		this.diagrams = diagrams;
		diagrams.setParent(this);
	}
	public Graphemes getGraphemes() {
		return graphemes;
	}
	public void setGraphemes(Graphemes graphemes) {
		this.graphemes = graphemes;
		graphemes.setParent(this);
	}
	@Override
	public Syntax clone(){
		Syntax s = new Syntax();
		s.setDescription(getDescription());
		s.setId(getId());
		s.setName(getName());
		s.setDiagrams(getDiagrams().clone());
		s.setGraphemes(getGraphemes().clone());
		s.setRules(getRules().clone());
		return s;
	}
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Syntax){
			this.setDiagrams(((Syntax) other).getDiagrams().clone());
			this.setGraphemes(((Syntax) other).getGraphemes().clone());
			this.setRules(((Syntax) other).getRules().clone());
		}
	}

    public void setParent(Language parent){
		this.parent = parent;
	}
	@Override
	public Language getParent(){
		return this.parent;
	}
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,(Container<? extends ConnoisseurObject>) this);
    }

	public List<ConnoisseurObject> getList() {
		return Arrays.asList(getGraphemes(),getRules(),getDiagrams());
	}
}
