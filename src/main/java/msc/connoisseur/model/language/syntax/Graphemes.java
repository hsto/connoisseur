package msc.connoisseur.model.language.syntax;

import java.util.ArrayList;
import java.util.List;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.DynamicContainer;
import msc.vino.Assessor;

public class Graphemes extends ConnoisseurObject implements DynamicContainer<Grapheme> {
	private List<Grapheme> graphemeList = new ArrayList<>();
	private Syntax parent;
	public void add(Grapheme g){
		graphemeList.add(g);
		g.setParent(this);
	}

	public List<Grapheme> getList(){
		return graphemeList;
	}
	public void setList(List<Grapheme> graphemes){
		graphemes.forEach(x -> {x.setParent(this);});
		this.graphemeList = graphemes;
	}

	@Override
	public Class getContainedClass() {
		return Grapheme.class;
	}

	@Override
	public void add(int index, Grapheme g) {
		this.graphemeList.add(index,g);
		g.setParent(this);
	}
	@Override
	public Graphemes clone(){
		Graphemes gs = new Graphemes();
		ArrayList<Grapheme> list = new ArrayList<>();
		for(Grapheme g : getList())
			list.add((Grapheme)g.clone());
		gs.setDescription(getDescription());
		gs.setId(getId());
		gs.setName(getName());
		gs.setList(list);
		return gs;
	}
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Graphemes){
			ArrayList<Grapheme> list = new ArrayList<>();
			for(Grapheme g : ((Graphemes) other).getList())
				list.add((Grapheme) g.clone());
			this.setList(list);
		}
	}

    public void setParent(Syntax parent){
		this.parent = parent;
	}
	@Override
	public Syntax getParent(){
		return this.parent;
	}
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,(ConnoisseurObject)this);
    }
}
