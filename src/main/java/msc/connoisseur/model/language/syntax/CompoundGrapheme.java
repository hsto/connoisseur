package msc.connoisseur.model.language.syntax;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.DynamicContainer;
import msc.connoisseur.model.visualvector.VisualVector;

public class CompoundGrapheme extends Grapheme implements DynamicContainer<Grapheme> {
	private boolean top;
	private boolean option;
	private Function<ArrayList<VisualVector>, VisualVector> aggregationRule;
	private List<Grapheme> graphemeList = new ArrayList<>();
	public CompoundGrapheme(){
		super();
		this.option = false;
	}
	public CompoundGrapheme(boolean top, boolean option) {
		super();
		this.top = top;
		this.option = option;
	}
	public boolean isTop(){
		return top;
	}
	public boolean isOption(){
		return option;
	}
	public void add(Grapheme g){
		graphemeList.add(g);
		g.setParent(this);
	}
	public void add(int index, Grapheme g){
		graphemeList.add(index,g);
		g.setParent(this);
	}
	@Override
	public double similarity(Assessment assessment, Grapheme other){
		return comparison(assessment.getWeights(),
				assessment.aggregate(this),
				other instanceof CompoundGrapheme
						?assessment.aggregate((CompoundGrapheme) other)
						:other.getVisualVector());
	}


	@Override
	public VisualVector getVisualVector() {
		ArrayList<VisualVector> lst = new ArrayList<>();
		for(Grapheme child: graphemeList){
			lst.add(child.getVisualVector());
		}
		if(aggregationRule == null){
			return lst.get(0);
		}else{
			return aggregationRule.apply(lst);
		}
	}
	public List<Grapheme> getList() {
		return graphemeList;
	}
	public void setList(List<Grapheme> children){
		children.forEach(x -> {x.setParent(this);});
		this.graphemeList = children;
	}

	@Override
	public Class getContainedClass() {
		return Grapheme.class;
	}

	@Override
	public String toString(){
		return (option?"option - ":"")+super.toString();
	}
	@Override
	public CompoundGrapheme clone(){
		CompoundGrapheme c = new CompoundGrapheme();
		c.setDescription(getDescription());
		c.setId(getId());
		c.setName(getName());
		for(Grapheme g: getList())
			c.add((Grapheme)g.clone());
		return c;
	}
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof CompoundGrapheme){
			ArrayList<Grapheme> list = new ArrayList<>();
			for(Grapheme child: ((CompoundGrapheme) other).getList())
				list.add((Grapheme)child.clone());
			this.setList(list);
		}
	}

}
