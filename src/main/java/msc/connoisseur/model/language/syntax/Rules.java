package msc.connoisseur.model.language.syntax;

import msc.connoisseur.model.EstimatableObject;

public class Rules extends EstimatableObject{
	private Syntax parent;
	public Rules(){
		setName("Rules");
	}
	@Override
	public Rules clone(){
		Rules clone = new Rules();
		clone.setDescription(getDescription());
		clone.setEstimate(getEstimate());
		clone.setId(getId());
		clone.setName(getName());
		clone.setReason(getReason());
		return clone;
	}

    public void setParent(Syntax parent){
		this.parent = parent;
	}
	@Override
	public Syntax getParent(){
		return this.parent;
	}
}
