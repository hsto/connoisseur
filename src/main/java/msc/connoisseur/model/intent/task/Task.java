package msc.connoisseur.model.intent.task;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.intent.Intent;
import msc.vino.Assessor;

public class Task extends ConnoisseurObject {
	private int understanding,documentation,communication,presentation;
	private Intent parent;

	@Override
	public double doAssess(Assessment assessment) {
		return Assessor.assess(assessment,this);
	}

	@Override
	public Task clone() {
		Task t = new Task();
		t.setDescription(getDescription());
		t.setName(getName());
		t.setId(getId());
		t.setCommunication(getCommunication());
		t.setDocumentation(getDocumentation());
		t.setUnderstanding(getUnderstanding());
		t.setPresentation(getPresentation());
		return t;
	}

	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Task){
			this.setCommunication(((Task) other).getCommunication());
			this.setDocumentation(((Task) other).getDocumentation());
			this.setUnderstanding(((Task) other).getUnderstanding());
			this.setPresentation(((Task) other).getPresentation());
		}
	}

    //region getters/setters
	@Override
	public Intent getParent() {
		return this.parent;
	}

	public void setParent(Intent parent){
		this.parent = parent;
	}

	public int getPresentation() {
		return presentation;
	}

	public void setPresentation(int presentation) {
		this.presentation = presentation;
	}

	public int getCommunication() {
		return communication;
	}

	public void setCommunication(int communication) {
		this.communication = communication;
	}

	public int getDocumentation() {
		return documentation;
	}

	public void setDocumentation(int documentation) {
		this.documentation = documentation;
	}

	public int getUnderstanding() {
		return understanding;
	}

	public void setUnderstanding(int understanding) {
		this.understanding = understanding;
	}
	//endregion
}
