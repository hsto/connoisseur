package msc.connoisseur.model.intent;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.ConnoisseurProject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.intent.modelnature.ModelNature;
import msc.connoisseur.model.intent.task.Task;
import msc.connoisseur.model.intent.tooldemands.ToolDemands;
import msc.connoisseur.model.intent.user.User;
import msc.vino.Assessor;

import java.util.Arrays;
import java.util.List;

public class Intent extends ConnoisseurObject implements Container<ConnoisseurObject> {
	private User user = new User();
	private ModelNature modelNature = new ModelNature();
	private Task task = new Task();
	private ToolDemands toolDemand = new ToolDemands();

	private ConnoisseurProject parent;
	public Intent(){
		user = new User();
	}
	public void setParent(ConnoisseurProject parent){
		this.parent = parent;
	}
	@Override
	public ConnoisseurProject getParent() {
		return parent;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ModelNature getModelNature() {
		return modelNature;
	}

	public void setModelNature(ModelNature modelNature) {
		this.modelNature = modelNature;
		this.modelNature.setParent(this);
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
		this.task.setParent(this);
	}

	public ToolDemands getToolDemand() {
		return toolDemand;
	}

	public void setToolDemand(ToolDemands toolDemand) {
		this.toolDemand = toolDemand;
		this.toolDemand.setParent(this);
	}

	@Override
	public double doAssess(Assessment assessment) {
		return Assessor.assess(assessment,(Container<? extends ConnoisseurObject>) this);
	}

	@Override
	public Intent clone() {
		Intent i = new Intent();
		i.setDescription(getDescription());
		i.setName(getName());
		i.setId(getId());
		i.setModelNature(getModelNature().clone());
		i.setTask(getTask().clone());
		i.setToolDemand(getToolDemand().clone());
		i.setUser(getUser().clone());
		return i;
	}

	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Intent){
			setUser(((Intent) other).getUser());
			setToolDemand(((Intent) other).getToolDemand());
			setTask(((Intent) other).getTask());
			setModelNature(((Intent) other).getModelNature());
		}
	}

    @Override
	public List<ConnoisseurObject> getList() {
		return Arrays.asList(getUser(),getTask(),getModelNature(),getToolDemand());
	}
}
