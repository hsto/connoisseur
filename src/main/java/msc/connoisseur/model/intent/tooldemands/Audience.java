package msc.connoisseur.model.intent.tooldemands;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.GroupSize;
import msc.connoisseur.model.common.Interactivity;
import msc.connoisseur.model.common.Persistence;
import msc.vino.Assessor;

public class Audience extends ConnoisseurObject {
	private Interactivity interactivity;
	private Persistence persistence;
	private GroupSize groupSize;
	private ToolDemands parent;

	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,this);
    }

	@Override
	public Audience clone() {
		Audience a = new Audience();
		a.setDescription(getDescription());
		a.setId(getId());
		a.setName(getName());
		a.setGroupSize(getGroupSize());
		a.setInteractivity(getInteractivity());
		a.setPersistence(getPersistence());
		return a;
	}

	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Audience){
			setGroupSize(((Audience) other).getGroupSize());
			setPersistence(((Audience) other).getPersistence());
			setInteractivity(((Audience) other).getInteractivity());
		}
	}

    //region getters/setters
	@Override
	public ToolDemands getParent() {
		return this.parent;
	}
	public void setParent(ToolDemands parent){
		this.parent = parent;
	}

	public GroupSize getGroupSize() {
		return groupSize;
	}

	public void setGroupSize(GroupSize groupSize) {
		this.groupSize = groupSize;
	}

	public Persistence getPersistence() {
		return persistence;
	}

	public void setPersistence(Persistence persistence) {
		this.persistence = persistence;
	}

	public Interactivity getInteractivity() {
		return interactivity;
	}

	public void setInteractivity(Interactivity interactivity) {
		this.interactivity = interactivity;
	}
	//endregion
}
