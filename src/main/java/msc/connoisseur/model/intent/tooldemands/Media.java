package msc.connoisseur.model.intent.tooldemands;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.vino.Assessor;

public class Media extends ConnoisseurObject {
	private int screen,paper,poster,whiteboard;
	private ToolDemands parent;
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,this);
    }

	@Override
	public Media clone() {
		Media m = new Media();
		m.setDescription(this.getDescription());
		m.setId(this.getId());
		m.setName(this.getName());
		m.setPaper(getPaper());
		m.setPoster(getPoster());
		m.setScreen(getScreen());
		m.setWhiteboard(getWhiteboard());
		return m;
	}
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Media){
			setPaper(((Media) other).getPaper());
			setPoster(((Media) other).getPoster());
			setScreen(((Media) other).getScreen());
			setWhiteboard(((Media) other).getWhiteboard());
		}
	}

    @Override
	public ToolDemands getParent() {
		return parent;
	}
	public void setParent(ToolDemands parent){
		this.parent = parent;
	}

	public int getWhiteboard() {
		return whiteboard;
	}

	public void setWhiteboard(int whiteboard) {
		this.whiteboard = whiteboard;
	}

	public int getPoster() {
		return poster;
	}

	public void setPoster(int poster) {
		this.poster = poster;
	}

	public int getPaper() {
		return paper;
	}

	public void setPaper(int paper) {
		this.paper = paper;
	}

	public int getScreen() {
		return screen;
	}

	public void setScreen(int screen) {
		this.screen = screen;
	}
}
