package msc.connoisseur.model.intent.tooldemands;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.intent.Intent;
import msc.vino.Assessor;

import java.util.Arrays;
import java.util.List;

public class ToolDemands extends ConnoisseurObject implements Container<ConnoisseurObject> {
	Audience audience = new Audience();
	Media media = new Media();
	private Intent parent;

	@Override
	public double doAssess(Assessment assessment) {
		return Assessor.assess(assessment,(Container<? extends ConnoisseurObject>) this);
	}

	@Override
	public ToolDemands clone() {
		ToolDemands td = new ToolDemands();
		td.setName(getName());
		td.setDescription(getDescription());
		td.setId(getId());
		td.setMedia(getMedia().clone());
		td.setAudience(getAudience().clone());
		return td;
	}

	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof ToolDemands){
			setAudience(((ToolDemands) other).getAudience().clone());
			setMedia(((ToolDemands) other).getMedia().clone());
		}
	}

    //region getters/setters
	@Override
	public Intent getParent() {
		return parent;
	}
	public void setParent(Intent parent){
		this.parent = parent;
	}
	public Audience getAudience() {
		return audience;
	}

	public Media getMedia() {
		return media;
	}

	public void setAudience(Audience audience) {
		this.audience = audience;
		this.audience.setParent(this);
	}

	public void setMedia(Media media) {
		this.media = media;
		this.media.setParent(this);
	}

	@Override
	public List<ConnoisseurObject> getList() {
		return Arrays.asList(getAudience(),getMedia());
	}
	//endregion
}
