package msc.connoisseur.model.intent.modelnature;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.vino.Assessor;

public class Size extends ConnoisseurObject {
	private int xxs,xs,s,m,l,xl,xxl;

	private ModelNature parent;

	@Override
	public double doAssess(Assessment assessment) {
		return Assessor.assess(assessment,this);
	}

	@Override
	public Size clone() {
		Size s = new Size();
		s.setDescription(getDescription());
		s.setName(getName());
		s.setId(getId());
		s.setXxs(getXxs());
		s.setXs(getXs());
		s.setS(getS());
		s.setM(getM());
		s.setL(getL());
		s.setXl(getXl());
		s.setXxl(getXxl());
		return s;
	}

	public void loadInto(ConnoisseurObject other) {
		super.loadInto(other);
		if(other instanceof Size){
			setXxs(((Size) other).getXxs());
			setXs(((Size) other).getXs());
			setS(((Size) other).getS());
			setM(((Size) other).getM());
			setL(((Size) other).getL());
			setXl(((Size) other).getXl());
			setXxl(((Size) other).getXxl());
		}
	}

    public void setParent(ModelNature parent) {
		this.parent = parent;
	}
	@Override
	public ModelNature getParent() {
		return this.parent;
	}

	public int getXxs() {
		return xxs;
	}

	public void setXxs(int xxs) {
		this.xxs = xxs;
	}

	public int getXs() {
		return xs;
	}

	public void setXs(int xs) {
		this.xs = xs;
	}

	public int getS() {
		return s;
	}

	public void setS(int s) {
		this.s = s;
	}

	public int getM() {
		return m;
	}

	public void setM(int m) {
		this.m = m;
	}

	public int getL() {
		return l;
	}

	public void setL(int l) {
		this.l = l;
	}

	public int getXl() {
		return xl;
	}

	public void setXl(int xl) {
		this.xl = xl;
	}

	public int getXxl() {
		return xxl;
	}

	public void setXxl(int xxl) {
		this.xxl = xxl;
	}

}
