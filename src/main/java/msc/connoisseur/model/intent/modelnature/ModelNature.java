package msc.connoisseur.model.intent.modelnature;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.intent.Intent;
import msc.vino.Assessor;

import java.util.Arrays;
import java.util.List;

public class ModelNature extends ConnoisseurObject implements Container<ConnoisseurObject> {
    private Formality formality = new Formality();
    private Size size = new Size();
    private Intent parent;

    public Formality getFormality() {
        return formality;
    }

    public void setFormality(Formality formality) {
        this.formality = formality;
        this.formality.setParent(this);
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
        this.size.setParent(this);
    }

    public Intent getParent() {
        return parent;
    }

    public void setParent(Intent parent) {
        this.parent = parent;
    }

    @Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,(Container)this);
    }

    @Override
    public ModelNature clone() {
        ModelNature m = new ModelNature();
        m.setDescription(getDescription());
        m.setName(getName());
        m.setId(getId());
        m.setFormality(getFormality().clone());
        m.setSize(getSize().clone());
        return m;
    }

    @Override
    public List<ConnoisseurObject> getList() {
        return Arrays.asList(getFormality(),getSize());
    }
}
