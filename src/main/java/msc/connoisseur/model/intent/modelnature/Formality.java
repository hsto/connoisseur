package msc.connoisseur.model.intent.modelnature;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.vino.Assessor;

public class Formality extends ConnoisseurObject {
	private int informal,semiformal,fullyformal;
	private ModelNature parent;

	@Override
	public double doAssess(Assessment assessment) {
		return Assessor.assess(assessment,this);
	}

	@Override
	public Formality clone() {
		Formality f = new Formality();
		f.setDescription(getDescription());
		f.setName(getName());
		f.setId(getId());
		f.setInformal(getInformal());
		f.setSemiformal(getSemiformal());
		f.setFullyformal(getFullyformal());
		return f;
	}

	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof Formality){
			setInformal(((Formality) other).getInformal());
			setSemiformal(((Formality) other).getSemiformal());
			setFullyformal(((Formality) other).getFullyformal());
		}
	}

    @Override
	public ModelNature getParent() {
		return this.parent;
	}
	public void setParent(ModelNature parent){
		this.parent = parent;
	}

	public int getFullyformal() {
		return fullyformal;
	}

	public void setFullyformal(int fullyformal) {
		this.fullyformal = fullyformal;
	}

	public int getSemiformal() {
		return semiformal;
	}

	public void setSemiformal(int semiformal) {
		this.semiformal = semiformal;
	}

	public int getInformal() {
		return informal;
	}

	public void setInformal(int informal) {
		this.informal = informal;
	}
}
