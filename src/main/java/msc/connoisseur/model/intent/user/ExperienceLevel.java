package msc.connoisseur.model.intent.user;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.vino.Assessor;

public class ExperienceLevel extends ConnoisseurObject {
	private User parent;
	private int novice,intermediate,expert;
	@Override
	public double doAssess(Assessment assessment) {
		return Assessor.assess(assessment,this);
	}

	@Override
	public ConnoisseurObject clone() {
		ExperienceLevel e = new ExperienceLevel();
		e.setDescription(getDescription());
		e.setId(getId());
		e.setName(getName());
		return e;
	}

	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof ExperienceLevel){
			setNovice(((ExperienceLevel) other).getNovice());
			setExpert(((ExperienceLevel) other).getExpert());
			setIntermediate(((ExperienceLevel) other).getIntermediate());
		}
	}

    //region getters/setters
	@Override
	public User getParent() {
		return this.parent;
	}

	public void setParent(User parent) {
		this.parent = parent;
	}

	public int getExpert() {
		return expert;
	}

	public void setExpert(int expert) {
		this.expert = expert;
	}

	public int getIntermediate() {
		return intermediate;
	}

	public void setIntermediate(int intermediate) {
		this.intermediate = intermediate;
	}

	public int getNovice() {
		return novice;
	}

	public void setNovice(int novice) {
		this.novice = novice;
	}
	//endregion
}
