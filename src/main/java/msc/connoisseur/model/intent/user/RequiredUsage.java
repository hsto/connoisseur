package msc.connoisseur.model.intent.user;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.GroupSize;
import msc.connoisseur.model.common.Interactivity;
import msc.connoisseur.model.common.Persistence;
import msc.vino.Assessor;

public class RequiredUsage extends ConnoisseurObject {
	private Interactivity interactivity;
	private Persistence persistence;
	private GroupSize groupSize;
	private User parent;
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,this);
    }

	@Override
	public ConnoisseurObject clone() {
		RequiredUsage r = new RequiredUsage();
		r.setDescription(getDescription());
		r.setName(getName());
		r.setId(getId());
		r.setGroupSize(getGroupSize());
		r.setInteractivity(getInteractivity());
		r.setPersistence(getPersistence());
		return r;
	}

	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof RequiredUsage){
			setInteractivity(((RequiredUsage) other).getInteractivity());
			setPersistence(((RequiredUsage) other).getPersistence());
			setGroupSize(((RequiredUsage) other).getGroupSize());
		}
	}

    //region getters/setters
	@Override
	public User getParent() {
		return this.parent;
	}

	public void setParent(User parent) {
		this.parent = parent;
	}

	public Interactivity getInteractivity() {
		return interactivity;
	}

	public void setInteractivity(Interactivity interactivity) {
		this.interactivity = interactivity;
	}

	public Persistence getPersistence() {
		return persistence;
	}

	public void setPersistence(Persistence persistence) {
		this.persistence = persistence;
	}

	public GroupSize getGroupSize() {
		return groupSize;
	}

	public void setGroupSize(GroupSize groupSize) {
		this.groupSize = groupSize;
	}
	//endregion
}
