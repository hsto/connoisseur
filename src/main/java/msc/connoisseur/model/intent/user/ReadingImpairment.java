package msc.connoisseur.model.intent.user;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.vino.Assessor;

public class ReadingImpairment extends ConnoisseurObject {
	private int none,dyslexia,analphabetic;
	private User parent;
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,this);
    }

	@Override
	public ReadingImpairment clone() {
		ReadingImpairment r = new ReadingImpairment();
		r.setNone(getNone());
		r.setDescription(getDescription());
		r.setAnalphabetic(getAnalphabetic());
		r.setDyslexia(getDyslexia());
		r.setId(getId());
		r.setName(getName());
		return r;
	}

	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof ReadingImpairment){
			setAnalphabetic(((ReadingImpairment) other).getAnalphabetic());
			setDyslexia(((ReadingImpairment) other).getDyslexia());
			setNone(((ReadingImpairment) other).getNone());
		}
	}

    //region getters/setters
	@Override
	public User getParent() {
		return this.parent;
	}

	public void setParent(User parent) {
		this.parent = parent;
	}

	public int getAnalphabetic() {
		return analphabetic;
	}

	public void setAnalphabetic(int analphabetic) {
		this.analphabetic = analphabetic;
	}

	public int getDyslexia() {
		return dyslexia;
	}

	public void setDyslexia(int dyslexia) {
		this.dyslexia = dyslexia;
	}

	public int getNone() {
		return none;
	}

	public void setNone(int none) {
		this.none = none;
	}
	//endregion
}
