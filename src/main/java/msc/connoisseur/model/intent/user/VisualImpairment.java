package msc.connoisseur.model.intent.user;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.vino.Assessor;

public class VisualImpairment extends ConnoisseurObject {
	private int none,colorDiscriminationImpairment,impairedEyesight,severelyImpairedEyesight;
	private User parent;

	@Override
	public double doAssess(Assessment assessment) {
		return Assessor.assess(assessment,this);
	}

	@Override
	public ConnoisseurObject clone() {
		VisualImpairment r = new VisualImpairment();
		r.setDescription(getDescription());
		r.setName(getName());
		r.setId(getId());
		r.setNone(getNone());
		r.setColorDiscriminationImpairment(getColorDiscriminationImpairment());
		r.setImpairedEyesight(getImpairedEyesight());
		r.setSeverelyImpairedEyesight(getSeverelyImpairedEyesight());
		return r;
	}

	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof VisualImpairment){
			setNone(((VisualImpairment) other).getNone());
			setSeverelyImpairedEyesight(((VisualImpairment) other).getSeverelyImpairedEyesight());
			setImpairedEyesight(((VisualImpairment) other).getImpairedEyesight());
			setColorDiscriminationImpairment(((VisualImpairment) other).getColorDiscriminationImpairment());
		}
	}

    //region getters/setters
	@Override
	public User getParent() {
		return this.parent;
	}
	public void setParent(User parent) {
		this.parent = parent;
	}

	public int getSeverelyImpairedEyesight() {
		return severelyImpairedEyesight;
	}

	public void setSeverelyImpairedEyesight(int severelyImpairedEyesight) {
		this.severelyImpairedEyesight = severelyImpairedEyesight;
	}

	public int getImpairedEyesight() {
		return impairedEyesight;
	}

	public void setImpairedEyesight(int impairedEyesight) {
		this.impairedEyesight = impairedEyesight;
	}

	public int getColorDiscriminationImpairment() {
		return colorDiscriminationImpairment;
	}

	public void setColorDiscriminationImpairment(int colorDiscriminationImpairment) {
		this.colorDiscriminationImpairment = colorDiscriminationImpairment;
	}

	public int getNone() {
		return none;
	}

	public void setNone(int none) {
		this.none = none;
	}
	//endregion
}
