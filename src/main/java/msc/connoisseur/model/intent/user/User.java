package msc.connoisseur.model.intent.user;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.intent.Intent;
import msc.vino.Assessor;

public class User extends ConnoisseurObject {
	private Intent parent;
	ExperienceLevel experienceLevel = new ExperienceLevel();
	VisualImpairment visualImpairment = new VisualImpairment();
	RequiredUsage requiredUsage = new RequiredUsage();
	ReadingImpairment readingImpairment = new ReadingImpairment();


	@Override
	public User clone() {
		User u = new User();
		u.setExperienceLevel(getExperienceLevel());
		u.setVisualImpairment(getVisualImpairment());
		u.setRequiredUsage(getRequiredUsage());
		u.setReadingImpairment(getReadingImpairment());
		u.setDescription(getDescription());
		u.setName(getName());
		u.setId(getId());
		return u;
	}

	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof User){
			setRequiredUsage(((User) other).getRequiredUsage());
			setReadingImpairment(((User) other).getReadingImpairment());
			setVisualImpairment(((User) other).getVisualImpairment());
			setExperienceLevel(((User) other).getExperienceLevel());
		}
	}

    //endregion
	@Override
	public double doAssess(Assessment assessment) {
		return Assessor.assess(assessment,this);
	}

	//region getters/setters
	public void setParent(Intent parent) {
		this.parent = parent;
	}

	@Override
	public Intent getParent() {
		return this.parent;
	}

	public ExperienceLevel getExperienceLevel() {
		return experienceLevel;
	}

	public VisualImpairment getVisualImpairment() {
		return visualImpairment;
	}

	public RequiredUsage getRequiredUsage() {
		return requiredUsage;
	}

	public ReadingImpairment getReadingImpairment() {
		return readingImpairment;
	}

	public void setExperienceLevel(ExperienceLevel experienceLevel) {
		this.experienceLevel = experienceLevel;
		this.experienceLevel.setParent(this);
	}

	public void setVisualImpairment(VisualImpairment visualImpairment) {

		this.visualImpairment = visualImpairment;
		this.visualImpairment.setParent(this);
	}

	public void setRequiredUsage(RequiredUsage requiredUsage) {
		this.requiredUsage = requiredUsage;
		this.requiredUsage.setParent(this);
	}
	public void setReadingImpairment(ReadingImpairment readingImpairment) {
		this.readingImpairment = readingImpairment;
		this.readingImpairment.setParent(this);
	}
	//endregion
}
