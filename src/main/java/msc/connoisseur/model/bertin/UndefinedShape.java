package msc.connoisseur.model.bertin;

public class UndefinedShape extends Shape<Void> {
	Void dummy;
	Texture texture;
	public UndefinedShape(){
		this.shapeClass = ShapeClass.undefined;
		texture = new RegionShape.RegionTexture();
	}
	@Override
	public double similarity(Shape<Void> other) {
		return 0;
	}

	@Override
	public Shape<Void> clone() {
		return new UndefinedShape();
	}

	@Override
	public void setValue(Void value) {
		dummy = value;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	@Override

	public Void getValue() {return dummy;}
	@Override
	public boolean equals(Object other) {
		return other instanceof UndefinedShape;
	}
	@Override
	public Texture getTexture() {
		return texture;
	}

}
