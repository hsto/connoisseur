package msc.connoisseur.model.bertin;


public class LabelShape extends Shape<String> {
	private String grammar = "";
	private LabelTexture texture;
	public LabelShape(){
		this.shapeClass = ShapeClass.label;
		texture = new LabelTexture();
	}
	public String getValue() {
		return grammar;
	}

	public void setValue(String grammar) {
		this.grammar = grammar;
	}

	public double similarity(Shape<String> other) {
			if (other instanceof LabelShape) {
				if (getValue().isEmpty() || ((LabelShape) other).getValue().isEmpty())
					return getValue()==((LabelShape) other).getValue()?1:0.5;
				else
					return 0.5 + (levenshteinDistance(getValue(), ((LabelShape) other).getValue())
							/ Math.max(getValue().length(), ((LabelShape) other).getValue().length())) * 0.5;
			} else
				return 0;
	}
	//https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Java
	public int levenshteinDistance (CharSequence lhs, CharSequence rhs) {
		int len0 = lhs.length() + 1;
		int len1 = rhs.length() + 1;

		// the array of distances
		int[] cost = new int[len0];
		int[] newcost = new int[len0];

		// initial cost of skipping prefix in String s0
		for (int i = 0; i < len0; i++) cost[i] = i;

		// dynamically computing the array of distances

		// transformation cost for each letter in s1
		for (int j = 1; j < len1; j++) {
			// initial cost of skipping prefix in String s1
			newcost[0] = j;

			// transformation cost for each letter in s0
			for(int i = 1; i < len0; i++) {
				// matching current letters in both strings
				int match = (lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1;

				// computing cost for each transformation
				int cost_replace = cost[i - 1] + match;
				int cost_insert  = cost[i] + 1;
				int cost_delete  = newcost[i - 1] + 1;

				// keep minimum cost
				newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
			}

			// swap cost/newcost arrays
			int[] swap = cost; cost = newcost; newcost = swap;
		}

		// the distance is the cost for transforming all letters in both strings
		return cost[len0 - 1];
	}
	@Override
	public LabelShape clone() {
		LabelShape s = new LabelShape();
		s.setValue(getValue());
		return s;
	}
	@Override
	public boolean equals(Object other) {
		return other instanceof LabelShape ? similarity((LabelShape) other)==1 : false;
	}

	public void setTexture(LabelTexture texture) {
		this.texture = texture;
	}

	@Override
	public LabelTexture getTexture() {
		return texture;
	}
	public static class LabelTexture extends Texture{
		@Override
		public String[] getTextureValues() {
			return new String[]{"Regular","Bold","Italic","Underlined","Bold Italic", "Bold Underlined", "Italic Underlined" ,"Bold Italic Underlined"};
		}
	}
}
