package msc.connoisseur.model.bertin;

public class Color extends VisualVariable<Float[]> {
	float hue, sat, bri;
	public Color(int r,int g,int b){
		setColor(r,g,b);
	}
	public Color(float h, float s, float b){
		hue = h;
		sat = s;
		bri = b;
	}
	public javafx.scene.paint.Color getFXColor() {
		return javafx.scene.paint.Color.hsb(hue*360, sat, bri);
	}
	public void setBri(float value){
		bri = value;
	}
	public float getBri(){
		return bri;
	}
	private String toFormattedHexString(int integer){
		String hs = Integer.toHexString(integer);
		while(hs.length()<2)hs = "0"+hs;
		return hs;
	}
	public String getRGBHexString(){
		java.awt.Color color = java.awt.Color.getHSBColor(hue, sat, bri);
		String r = toFormattedHexString(color.getRed());
		String g = toFormattedHexString(color.getGreen());
		String b = toFormattedHexString(color.getBlue());
		return "#"+r+g+b;
	}
	public Float[] getValue(){
		return new Float[]{hue,sat,bri};
	}
	public void setValue(Float[] hsb){
		setHue(hsb[0]);
		setSat(hsb[1]);
		setBri(hsb[2]);
	}
	public void setColor(int red, int green, int blue) {
		float[] hsb = java.awt.Color.RGBtoHSB(red, green, blue, null);
		setHue(hsb[0]);
		setSat(hsb[1]);
		setBri(hsb[2]);
	}

	public void setHue(float value) {
		hue = value;
	}
	public void setSat(float value) {
		sat = value;
	}
	public double similarity(Color other){
		double epsilon = 0.0001;
		return Math.abs(getValue()[0]-other.getValue()[0])<epsilon
				&&Math.abs(getValue()[1]-other.getValue()[1])<epsilon?1:0;
	}
	@Override
	public Color clone(){
		return new Color(hue,sat,bri);
	}
	@Override
	public double similarity(VisualVariable vv) {
		return vv instanceof VisualVariable?similarity((Color)vv):0;
	}
	@Override
	public boolean equals(Object other) {
		return other instanceof Color ? similarity((Color) other)==1 : false;
	}
}
