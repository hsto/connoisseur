package msc.connoisseur.model.bertin;

import javafx.util.Pair;
import msc.connoisseur.model.bertin.RegionShape.CornerFeatures;
import msc.connoisseur.model.bertin.RegionShape.Kind;

public class RegionShape extends Shape<Pair<Kind,CornerFeatures>> {
	public enum Kind{circle,oval,ellipsis,square,rectangle,triangle,polygon,optional,undefined}
	public enum CornerFeatures{sharp,rounded,optional,undefined}
	private Kind kind = Kind.undefined;
	private CornerFeatures cf = CornerFeatures.undefined;
	private RegionTexture texture;
	public RegionShape(){
		this.shapeClass = ShapeClass.region;
		texture = new RegionTexture();
	}
	public CornerFeatures getCornerFeatures(){
		return this.cf;
	}
	public void setCornerFeatures(CornerFeatures cf){
		this.cf = cf;
	}
	public Kind getKind() {
		return kind;
	}
	public void setKind(Kind kind) {
		this.kind = kind;
	}
	public double similarity(Shape<Pair<Kind,CornerFeatures>> other){
		if(other instanceof RegionShape){
			double result = 0.75;
			if(getKind()==((RegionShape) other).getKind())
				result += 0.20;
			if(getCornerFeatures()==((RegionShape) other).getCornerFeatures())
				result += 0.05;
			return result;
		}else
			return 0;
	}

	public void setTexture(RegionTexture texture) {
		this.texture = texture;
	}

	@Override
	public RegionShape clone(){
		RegionShape r = new RegionShape();
		r.setValue(getValue());
		return r;
	}
	public void setValue(Pair<Kind,CornerFeatures> value){
		setKind(value.getKey());
		setCornerFeatures(value.getValue());
	}
	@Override
	public Pair<Kind, CornerFeatures> getValue() {
		return new Pair(getKind(),getCornerFeatures());
	}
	@Override
	public boolean equals(Object other) {
		return other instanceof RegionShape ? similarity((RegionShape) other)==1 : false;
	}
	@Override
	public Texture getTexture() {
		return texture;
	}
	public static class RegionTexture extends Texture{
		@Override
		public String[] getTextureValues() {
			return new String[]{"Solid","Striped","Dotted","Checkered","Outlined","Undefined"};
		}
	}
}
