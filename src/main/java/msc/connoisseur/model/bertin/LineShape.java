package msc.connoisseur.model.bertin;

public class LineShape extends Shape<LineShape.Kind> {
	public enum Kind {
		curved, edged, straight, optional, undefined
	}

	private Kind kind = Kind.undefined;
	private LineTexture texture;
	public LineShape(){
		this.shapeClass = ShapeClass.line;
		texture = new LineTexture();
	}
	public Kind getValue() {
		return kind;
	}

	public void setValue(Kind kind) {
		this.kind = kind;
	}

	public void setTexture(LineTexture texture) {
		this.texture = texture;
	}

	public double similarity(Shape<LineShape.Kind> other) {
		if (other instanceof LineShape) {
			double result = 0.5;
			if (getValue() == ((LineShape) other).getValue())
				result += 0.5;
			return result;
		}else
			return 0;

	}

	@Override
	public LineShape clone() {
		LineShape s = new LineShape();
		s.setValue(getValue());
		return s;
	}
	@Override
	public boolean equals(Object other) {
		return other instanceof LineShape ? similarity((Shape<LineShape.Kind>) other)==1 : false;
	}

	@Override
	public LineTexture getTexture() {
		return texture;
	}
	public static class LineTexture extends Texture{
		@Override
		public String[] getTextureValues() {
			return new String[]{"Solid","Dashed","Dotted","Undefined"};
		}
	}

}
