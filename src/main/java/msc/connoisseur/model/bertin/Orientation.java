package msc.connoisseur.model.bertin;

public class Orientation extends VisualVariable<Integer> {
	int rotation;
	public Orientation(int rotation){
		setValue(rotation);
	}
	public Integer getValue() {
		return rotation%360;
	}

	public void setValue(Integer rotation) {
		this.rotation = rotation;
	}
	public double similarity(Orientation other){
		return getValue()==other.getValue()?1:0;
	}
	@Override
	public Orientation clone(){
		Orientation o = new Orientation(getValue());
		return o;
	}
	@Override
	public double similarity(VisualVariable vv) {
		return vv instanceof Orientation?similarity((Orientation)vv):0;
	}
	@Override
	public boolean equals(Object other) {
		return other instanceof Orientation ? similarity((Orientation) other)==1 : false;
	}
}
