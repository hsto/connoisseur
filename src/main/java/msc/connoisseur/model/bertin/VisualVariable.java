package msc.connoisseur.model.bertin;

import java.io.Serializable;

public abstract class VisualVariable<T> implements Serializable{
	public abstract double similarity(VisualVariable<T> vv);
	public abstract void setValue(T value);
	public abstract T getValue();
	public abstract boolean equals(Object other);
}
