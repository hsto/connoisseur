package msc.connoisseur.model.bertin;


public abstract class Texture extends VisualVariable<String> {
	String pattern;
	public Texture(){
		setValue("Undefined");
	}
	public void setValue(String pattern){
		this.pattern = pattern;
	}
	public String getValue(){
		return pattern;
	}
	public abstract String[] getTextureValues();
	@Override
	public double similarity(VisualVariable<String> vv) {
		return getValue()==vv.getValue()?1:0;
	}
	@Override
	public boolean equals(Object other) {
		return other instanceof Texture ? similarity((Texture) other)==1 : false;
	}
}
