package msc.connoisseur.model.bertin;

import javafx.util.Pair;
import msc.connoisseur.model.language.syntax.Grapheme;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static msc.connoisseur.model.bertin.Position.CardinalDirection.East;
import static msc.connoisseur.model.bertin.Position.CardinalDirection.West;

public class Position extends VisualVariable<Pair<List<Position.CardinalDirection>,Grapheme>> {
	public enum CardinalDirection{North,South,East,West;};
	private List<CardinalDirection> position = new ArrayList<>();
	Grapheme reference;

	public List<CardinalDirection> getPosition() {
		return position;
	}

	public void setPosition(List<CardinalDirection> position) {
		this.position = position;
	}


	public Grapheme getReference(){
		return reference;
	}
	public void setReference(Grapheme reference){
		this.reference = reference;
	}
	public double similarity(Position other){
		double result = 1;
		if(getReference() == other.getReference()){
			int x1 = 0, x2 = 0, y1 = 0, y2 = 0;
			for(CardinalDirection dir : getValue().getKey()){
				if(dir == West || dir == East)
					x1 += simSub(dir);
				else
					y1 += simSub(dir);
			}
			for(CardinalDirection dir : other.getValue().getKey()){
				if(dir == West || dir == East)
					x2 += simSub(dir);
				else
					y2 += simSub(dir);
			}
			if(x1 == x2 && y1 == y2)
				return 1d;
			else
				result = 1d - (new Triangle(new Point(x1,y1),new Point(x2,y2))).getAngleC()/180d;
		}else{
			result = 0;
		}
		return result;
	}
	private int simSub(CardinalDirection dir){
		switch(dir){
			case East:
			case North:return 1;
			case South:
			case West:return -1;
			default: return 0;
		}
	}
	@Override
	public Position clone(){
		Position p = new Position();
		p.setValue(getValue());
		return p;
	}

	@Override
	public double similarity(VisualVariable vv) {
		return vv instanceof Position?similarity((Position)vv):0;
	}


	@Override
	public void setValue(Pair<List<CardinalDirection>, Grapheme> value) {
		this.position = value.getKey();
		this.setReference(value.getValue());
	}

	@Override
	public Pair<List<CardinalDirection>, Grapheme> getValue() {
		return new Pair<List<CardinalDirection>,Grapheme>(
				this.position
				, getReference());
	}
	@Override
	public boolean equals(Object other) {
		return other instanceof Position ? similarity((Position) other)==1 : false;
	}
	private class Triangle{
		private Point a,b,c;
		public Triangle(Point a, Point b){
			this.a = a;
			this.b = b;
			this.c = new Point(0,0);
		}
		public double getSideA(){
			return distance(b,c);
		}
		public double getSideB(){
			return distance(a,c);
		}
		public double getSideC(){
			return distance(a,b);
		}
		public double getAngleC(){
			return Math.acos((Math.pow(getSideA(),2)+Math.pow(getSideB(),2)-Math.pow(getSideC(),2))/(2*getSideA()*getSideB()));
		}
		private double distance(Point p1, Point p2){
			return Math.abs(Math.sqrt(Math.pow(p1.getX()-p2.getX(),2)+Math.pow(p1.getY()-p2.getY(),2)));
		}
	}
}
