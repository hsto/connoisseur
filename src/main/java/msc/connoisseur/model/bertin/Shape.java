package msc.connoisseur.model.bertin;

public abstract class Shape<T> extends VisualVariable<T> {
	public enum ShapeClass{ region, line, icon, label, undefined}
	protected ShapeClass shapeClass;
	public ShapeClass getShapeClass(){
		return shapeClass;
	}
	public abstract Texture getTexture();
	public abstract void setValue(T value);
	public abstract T getValue();
	public abstract double similarity(Shape<T> other);
	public double similarity(VisualVariable vv){
		if(vv instanceof Shape){
			Shape s = (Shape) vv;
			return similarity(s);
		}else{
			return 0;
		}
	}
	public abstract Shape<T> clone();
}
