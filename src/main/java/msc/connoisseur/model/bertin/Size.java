package msc.connoisseur.model.bertin;

import javafx.util.Pair;
import msc.connoisseur.model.bertin.Size.SizeValue;
import msc.connoisseur.model.language.syntax.Grapheme;

public class Size extends VisualVariable<Pair<SizeValue,Grapheme>> {
	public enum SizeValue{inconsequential,tiny,small,average,large,huge,humongous, none}
	SizeValue value;
	Grapheme relation;

	public Size(SizeValue value, Grapheme relation) {
		setSizeValue(value);
		this.relation = relation;
	}
	public SizeValue getSizeValue() {
		return value;
	}
	public void setSizeValue(SizeValue value) {
		this.value = value;
	}
	public Grapheme getRelation() {
		return relation;
	}
	public void setRelation(Grapheme reference) {
		this.relation = reference;

	}
	public double similarity(Size other) {
		return getSizeValue()==other.getSizeValue()?1:0;
	}
	@Override
	public Size clone(){
		Size s = new Size(getSizeValue(),null);
		return s;
	}
	@Override
	public double similarity(VisualVariable vv) {
		return vv instanceof Size?similarity((Size)vv):0;
	}
	public Pair<SizeValue,Grapheme> getValue(){
		return new Pair<SizeValue,Grapheme>(getSizeValue(),getRelation());
	}
	public void setValue(Pair<SizeValue,Grapheme> value){
		setSizeValue(value.getKey());
		setRelation(value.getValue());
	}
	@Override
	public boolean equals(Object other) {
		return other instanceof Size ? similarity((Size) other)==1 : false;
	}
}
