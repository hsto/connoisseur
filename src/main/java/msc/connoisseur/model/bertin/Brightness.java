package msc.connoisseur.model.bertin;

public class Brightness extends VisualVariable<Float> {
	float brightness;
	public Brightness(float value){
		brightness = value;
	}
	public Brightness(Color base){
		brightness = base.getBri();
	}
	public void setValue(Float value){
		brightness = value;
	}
	public Float getValue(){
		return brightness;
	}
	public double similarity(Brightness other){

		return 1d-(Math.max(getValue(), other.getValue())-Math.min(getValue(), other.getValue()));
	}
	@Override
	public Brightness clone(){
		return new Brightness(getValue());
	}
	@Override
	public double similarity(VisualVariable vv) {
		return vv instanceof Brightness?similarity((Brightness)vv):0;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Brightness ? similarity((Brightness) other)==1 : false;
	}
}
