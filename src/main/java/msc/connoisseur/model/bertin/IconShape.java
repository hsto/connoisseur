package msc.connoisseur.model.bertin;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;

import javax.imageio.ImageIO;

public class IconShape extends Shape<Image> {
	private Image bitmap;
	private IconTexture texture;
	public IconShape(){
		this.shapeClass = ShapeClass.icon;
		texture = new IconTexture();
	}
	public Image getValue() {
		return bitmap;
	}

	public void setValue(Image bitmap) {
		this.bitmap = bitmap;
	}

	public double similarity(Shape<Image> other) {
		if (other instanceof IconShape) {
			double result = 0;
			Image i1 = getValue();
			Image i2 = ((IconShape) other).getValue();
			if (i1 == null || i2 == null)
				return 1;
			PixelReader pr1 = i1.getPixelReader();
			PixelReader pr2 = i2.getPixelReader();
			for (int x = 0; x < Math.min(i1.getWidth(), i2.getWidth()); x++) {
				for (int y = 0; y < Math.min(i2.getHeight(), i1.getHeight()); y++) {
					result += pr1.getArgb(x, y) == pr2.getArgb(x, y) ? 1 : 0;
				}
			}
			return 0.5 + (result / (Math.max(i1.getWidth(), i2.getWidth()) * Math.max(i2.getHeight(), i1.getHeight())))
					* 0.5;
		} else
			return 0;
	}

	@Override
	public IconTexture getTexture() {
		return texture;
	}

	@Override
	public IconShape clone() {
		IconShape s = new IconShape();
		s.setValue(getValue());
		return s;
	}
	@Override
	public boolean equals(Object other) {
		return other instanceof IconShape ? similarity((Shape<Image>) other)==1 : false;
	}
	public static class IconTexture extends Texture{
		@Override
		public String[] getTextureValues() {
			return new String[]{"Solid","Outlined","Other","Undefined"};
		}
	}


}
