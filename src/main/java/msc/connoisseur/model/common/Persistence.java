package msc.connoisseur.model.common;

/**
 * Created by jeppe on 29-09-2016.
 */
public enum Persistence {
    ephemeral,
    elongated,
    eternal
}
