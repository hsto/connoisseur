package msc.connoisseur.model.common;

import msc.connoisseur.model.common.Pair;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jeppe on 28-09-2016.
 */
public interface MappingContainer<k,v> extends DynamicContainer<Pair<k,v>> {
    default List<Pair<k,v>> removeKey(k g){
        List<Pair<k,v>> output = getList().stream()
                .filter(pair -> pair.a==g)
                .collect(Collectors.toList());
        getList().removeAll(output);
        return output;
    }
    default List<Pair<k,v>> removeValue(v c){
        List<Pair<k,v>> output = getList().stream()
                .filter(pair -> pair.b==c)
                .collect(Collectors.toList());
        getList().removeAll(output);
        return output;
    }
    default Set<v> getMappedValues(){
        return getList().stream()
                .map(pair -> pair.b)
                .collect(Collectors.toSet());
    }
    default Set<v> getMappedValues(k g){
        return getList().stream()
                .filter(pair -> pair.a==g)
                .map(pair -> pair.b)
                .collect(Collectors.toSet());
    }
    default Set<k> getMappedKeys(){
        return getList().stream()
                .map(pair -> pair.a)
                .collect(Collectors.toSet());
    }
    default Set<k> getMappedKeys(v c){
        return getList().stream()
                .filter(pair -> pair.b==c)
                .map(pair -> pair.a)
                .collect(Collectors.toSet());
    }
    default Set<k> getUnmappedks(List<k> availableks){
        Set<k> mapped = getMappedKeys();
        return availableks.stream().filter(g -> !mapped.contains(g)).collect(Collectors.toSet());
    }
    default Set<v> getUnmappedvs(List<v> availablevs){
        Set<v> mapped = getMappedValues();
        return availablevs.stream().filter(c -> !mapped.contains(c)).collect(Collectors.toSet());
    }
    default Class getContainedClass(){
        return Pair.class;
    }
}
