package msc.connoisseur.model.common;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public interface DynamicContainer<T> extends Container<T> {
	default void add(T element) {
		getList().add(element);
	}

	default void add(int index, T element) {
		getList().add(index, element);
	}

	default boolean remove(T element){
		return getList().remove(element);
	}

	default T remove(int index){
		return (T) getList().remove(index);
	}

	default int indexOf(T element){
		return getList().indexOf(element);
	}

	default T get(int index){
		return (T) getList().get(index);
	}

	default List<T> cloneList(Function<T, T> cloner) {
		return getList().stream().collect(Collectors.mapping(cloner,Collectors.toList()));
	}

	void setList(List<T> list);

	Class getContainedClass();

}
