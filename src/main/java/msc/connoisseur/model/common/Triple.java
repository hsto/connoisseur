package msc.connoisseur.model.common;

import java.io.Serializable;

/**
 * Created by Jeppe on 02-11-2016.
 */
public class Triple<S,T,U> implements Serializable {
    public S a;
    public T b;
    public U c;
    public Triple(){

    }
    public Triple(S a, T b, U c){
        this.a = a;
        this.b = b;
        this.c = c;
    }
}
