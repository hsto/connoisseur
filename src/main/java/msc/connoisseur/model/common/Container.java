package msc.connoisseur.model.common;

import java.util.List;

/**
 * Created by jeppe on 02-10-2016.
 */
public interface Container<T> {
    List<T> getList();
}
