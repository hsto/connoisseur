package msc.connoisseur.model.common;

/**
 * Created by jeppe on 29-09-2016.
 */
public enum GroupSize {
    g1,
    g2_5,
    g5_10,
    g10_50,
    ggt50
}
