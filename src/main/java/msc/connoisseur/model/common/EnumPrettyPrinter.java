package msc.connoisseur.model.common;

/**
 * Created by jeppe on 29-09-2016.
 */
public class EnumPrettyPrinter {
    public static String groupSizeString(GroupSize groupSizeType){
        return groupSizeType.name().substring(1).replaceAll("_"," ").replaceAll("gt",">");
    }
    public static String persistenceString(Persistence persistenceType) {
        String name = persistenceType.name();
        return name.substring(0,1).toUpperCase()+name.substring(1).toLowerCase();
    }
    public static String interactivityString(Interactivity interactivityType) {
        String name = interactivityType.name().replaceAll("_"," ");
        return name.substring(0,1).toUpperCase()+name.substring(1).toLowerCase();
    }
}
