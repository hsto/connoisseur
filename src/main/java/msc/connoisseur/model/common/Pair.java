package msc.connoisseur.model.common;

import java.io.Serializable;

/**
 * Created by Jeppe on 02-11-2016.
 */
public class Pair<T,U> implements Serializable{
    public Pair(){

    }
    public Pair(T a, U b){
        this.a = a;
        this.b = b;
    }
    public T a;
    public U b;
}
