package msc.connoisseur.model.assessment;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.ConnoisseurProject;
import msc.connoisseur.model.bertin.RegionShape;
import msc.connoisseur.model.configuration.Weights;
import msc.connoisseur.model.intent.Intent;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.language.syntax.CompoundGrapheme;
import msc.connoisseur.model.language.syntax.Grapheme;
import msc.connoisseur.model.visualvector.VisualVector;
import msc.vino.Assessor;

import java.util.function.Function;

public class Assessment extends ConnoisseurObject {
	ConnoisseurProject parent;
	ConnoisseurObject scope;
	Intent assumptions;
    Weights weights = new Weights();
    AggregationMethods.Method aggregationMethod = AggregationMethods.Method.base;

    public void setAggregationMethod(AggregationMethods.Method aggregationMethod) {
        this.aggregationMethod = aggregationMethod;
    }
    public AggregationMethods.Method getAggregationMethod() {
        return aggregationMethod;
    }
    public VisualVector aggregate(CompoundGrapheme compoundGrapheme){
        return AggregationMethods.getMethod(getAggregationMethod()).apply(compoundGrapheme);
    }


	public ConnoisseurObject getScope() {
		return scope;
	}

	public void setScope(ConnoisseurObject scope) {
        if(this.scope == scope) return;
        this.scope = scope;
	}


    public Intent getAssumptions() {
		return assumptions;
	}

	public void setAssumptions(Intent assumptions) {
		this.assumptions = assumptions;
	}

	public Weights getWeights() {
		return weights;
	}




	@Override
	public ConnoisseurProject getParent() {
		return this.parent;
	}

	public void setParent(ConnoisseurProject parent) {
		this.parent = parent;
	}

	public double doAssess(Assessment assessment) {
        return Assessor.assess(this);
    }

    @Override
    public Assessment clone() {
        Assessment a = new Assessment();
        a.setDescription(getDescription());
        a.setId(getId());
        a.setName(getName());
        a.setScope(getScope());
        a.setAssumptions(getAssumptions());
        a.setScope(getScope());
        a.setWeights(getWeights());
        a.setAggregationMethod(getAggregationMethod());
        return a;
    }

	public void loadInto(ConnoisseurObject other) {
		super.loadInto(other);
		if (other instanceof Assessment) {
			setScope(((Assessment) other).getScope());
			setAssumptions(((Assessment) other).getAssumptions());
			setWeights(((Assessment) other).getWeights());
            setAggregationMethod(((Assessment)other).getAggregationMethod());
		}
	}

    public void setWeights(Weights weights) {
        this.weights = weights;
    }


    public static class AggregationMethods{
        public enum Method{base,biggest,smallest,biggestregion}
        //Base
        private static Function<CompoundGrapheme,VisualVector> baseMethod = x ->
            x.getList().isEmpty()?null:gToVec(x.getList().get(0),getMethod(Method.base));

        //Biggest
        private static Function<CompoundGrapheme,VisualVector> biggestMethod = x ->
            x.getList().stream()
                    .map(grapheme -> gToVec(grapheme,getMethod(Method.biggest)))
                    .max((a,b) -> a.getSize().getSizeValue().ordinal()-b.getSize().getSizeValue().ordinal()).orElse(baseMethod.apply(x));
        //Smallest
        private static Function<CompoundGrapheme,VisualVector> smallestMethod = x ->
            x.getList().stream()
                    .map(grapheme -> gToVec(grapheme,getMethod(Method.smallest)))
                    .min((a,b) -> a.getSize().getSizeValue().ordinal()-b.getSize().getSizeValue().ordinal()).orElse(baseMethod.apply(x));
        //Biggest Region
        private static Function<CompoundGrapheme,VisualVector> biggestRegionMethod = x ->
                x.getList().stream()
                        .map(grapheme -> gToVec(grapheme,getMethod(Method.biggestregion)))
                        .filter(vector -> vector.getShape() instanceof RegionShape)
                        .max((a,b) -> a.getSize().getSizeValue().ordinal()-b.getSize().getSizeValue().ordinal()).orElse(biggestMethod.apply(x));
        public static Function<CompoundGrapheme,VisualVector> getMethod(Method method){
            switch (method){
                case base: return baseMethod;
                case biggest: return biggestMethod;
                case smallest: return smallestMethod;
                case biggestregion: return biggestRegionMethod;
                default: return null;
            }
        }
        private static VisualVector gToVec(Grapheme g, Function<CompoundGrapheme,VisualVector> aggregationMethod){
            if(g instanceof AtomicGrapheme) return g.getVisualVector();
            else if (g instanceof CompoundGrapheme) return aggregationMethod.apply((CompoundGrapheme) g);
            else return null;
        }
    }
}

