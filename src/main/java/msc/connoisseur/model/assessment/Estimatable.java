package msc.connoisseur.model.assessment;

public interface Estimatable {
	public void setEstimate(double estimate);
	public double getEstimate();
	public void setReason(String text);
	public String getReason();
}
