package msc.connoisseur.model;

import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.assessment.Estimatable;
import msc.vino.Assessor;

public abstract class EstimatableObject extends ConnoisseurObject implements Estimatable {
	private double estimate;
	private String reason;
	@Override
	public double getEstimate() {
		return this.estimate;
	}
	@Override
	public String getReason() {
		return this.reason;
	}
	@Override
	public void setEstimate(double estimate) {
		this.estimate = estimate;

	}
	@Override
	public void setReason(String text) {
		this.reason = text;
	}
	@Override
	public void loadInto(ConnoisseurObject other){
		super.loadInto(other);
		if(other instanceof EstimatableObject){
			this.setReason(((EstimatableObject) other).getReason());
			this.setEstimate(((EstimatableObject) other).getEstimate());
		}
	}
	@Override
    public double doAssess(Assessment assessment) {
        return Assessor.assess(assessment,this);
    }
}
