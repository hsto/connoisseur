package msc.connoisseur.view.properties.panel;

import javafx.scene.control.ScrollPane;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.common.Pair;
import msc.connoisseur.model.language.Language;
import msc.connoisseur.model.language.semantics.Concept;
import msc.connoisseur.model.language.semantics.Domain;
import msc.connoisseur.view.properties.panel.components.table.MappingTable;
import msc.connoisseur.view.properties.panel.components.table.TableRow;

import java.util.List;

/**
 * Created by jeppe on 10-10-2016.
 */
public class DomainMappingPanel extends PropertiesPanel{
    private MappingTable<Domain,Concept> table;
    public DomainMappingPanel(Controller controller,Language language){
        super(controller);
        table = new MappingTable<Domain,Concept>() {

            @Override
            protected void onNewBlankRow(TableRow tr) {

            }

            @Override
            protected List<? extends Pair<Domain, Concept>> getCurrentMappings() {
                return language.getSemantics().getDomainMapping().getList();
            }

            @Override
            protected String getSecondColumnName() {
                return "Concepts";
            }

            @Override
            protected String getFirstColumnName() {
                return "Domains";
            }

            @Override
            protected void addMapping(Pair newpair) {
                getController().add(language.getSemantics().getDomainMapping(),newpair);
            }

            @Override
            protected void removeMapping(Pair oldpair) {
                getController().remove(language.getSemantics().getDomainMapping(),oldpair);
            }

            @Override
            protected List<Domain> getFirstColumnChoices() {
                return language.getSemantics().getElements().getDomainList();
            }

            @Override
            protected List<Concept> getSecondColumnChoices() {
                return language.getSemantics().getElements().getConceptList();
            }
        };
        table.setOnChangedAction(()->table.apply());
        ScrollPane sc = new ScrollPane(table);
        sc.setPrefWidth(770);
        sc.setPrefHeight(400);
        sc.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        add(sc);
    }
    @Override
    public void reset() {
        table.reset();
    }
}
