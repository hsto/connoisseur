package msc.connoisseur.view.properties.panel.components.table;

import javafx.scene.Node;

public abstract class TableCell<T> {
	private TableColumn<T> column;
	private TableRow row;
	private T value;
	public TableCell(TableRow row,TableColumn<T> column){
		setRow(row);
		setColumn(column);
	}
	public TableColumn<T> getColumn() {
		return column;
	}
	public void setColumn(TableColumn<T> column) {
		this.column = column;
	}
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}
	public TableRow getRow() {
		return row;
	}
	public void setRow(TableRow row) {
		this.row = row;
	}
	protected abstract void onChanged(T oldvalue,T newvalue);
	public abstract Node getNode();
	public void setVisible(boolean visible) {
		getNode().setVisible(visible);
	}
}
