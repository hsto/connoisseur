package msc.connoisseur.view.properties.panel;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.ConnoisseurProject;
import msc.connoisseur.model.LanguageSystem;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.dialect.Dialect;
import msc.connoisseur.model.intent.Intent;
import msc.connoisseur.model.language.Language;
import msc.connoisseur.view.properties.panel.components.WeightPanel;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by jeppe on 10-10-2016.
 */
public class AssessmentPanel extends PropertiesPanel {
    Assessment model;
    ChoiceBox<ConnoisseurObject> scopeChooser = new ChoiceBox<>();
    ChoiceBox<ConnoisseurObject> assumptions = new ChoiceBox<>();
    ChoiceBox<Assessment.AggregationMethods.Method> aggregationMethods = new ChoiceBox<>();
    WeightPanel weightPanel;
    public AssessmentPanel(Controller controller, Assessment model) {
        super(controller);
        this.model = model;

        weightPanel = new WeightPanel(controller, model);
        initialize();
    }

    private void initialize() {
        populateChoiceBox(scopeChooser,validScopeFilter());
        populateChoiceBox(assumptions,x -> x instanceof Intent);
        aggregationMethods.setItems(FXCollections.observableArrayList(Assessment.AggregationMethods.Method.values()));
        VBox components = new VBox(new HBox(simpleFormField("Scope",scopeChooser),simpleFormField("Assumption",assumptions)),simpleFormField("Variable Aggregation Method",aggregationMethods),titledContainer("Weights",new Node[][]{{weightPanel}}));
        add(components);
        aggregationMethods.valueProperty().addListener(methodListener());
        scopeChooser.valueProperty().addListener(scopeListener());
        assumptions.valueProperty().addListener(assumptionListener());
    }

    private ChangeListener<? super Assessment.AggregationMethods.Method> methodListener() {
        return (owner,oldValue,newValue) -> {
            getController().setAggregationMethod(model,newValue);
        };
    }

    private ChangeListener<? super ConnoisseurObject> assumptionListener() {
        return (owner,oldValue,newValue) -> {
            getController().setAssumptions(model,(Intent)newValue);
        };
    }

    private ChangeListener<? super ConnoisseurObject> scopeListener() {
        return (owner,oldValue,newValue) -> {
            getController().setScope(model,newValue);
            weightPanel.setScope(newValue);
        };
    }

    private void populateChoiceBox(ChoiceBox<ConnoisseurObject> chooser, Predicate<? super ConnoisseurObject> filter) {
        chooser.setPrefWidth(200);
        ConnoisseurProject project = model.getParent();
        List<ConnoisseurObject> list = project.getList().stream().filter(filter).collect(Collectors.toList());
        chooser.setItems(FXCollections.observableList(list));
    }

    private Predicate<? super ConnoisseurObject> validScopeFilter() {
        return x -> x instanceof LanguageSystem || x instanceof Language || x instanceof Dialect;
    }

    @Override
    public void reset() {
        scopeChooser.setValue(model.getScope());
        assumptions.setValue(model.getAssumptions());
        aggregationMethods.setValue(model.getAggregationMethod());
        weightPanel.reset();
    }
}
