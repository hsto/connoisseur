package msc.connoisseur.view.properties.panel.grapheme;

import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.bertin.Size.SizeValue;
import msc.connoisseur.model.common.DynamicContainer;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.language.syntax.CompoundGrapheme;
import msc.connoisseur.model.language.syntax.Grapheme;
import msc.connoisseur.model.language.syntax.Graphemes;
import msc.connoisseur.model.visualvector.VisualVector;
import msc.connoisseur.view.properties.panel.PropertiesPanel;
import msc.connoisseur.view.properties.panel.components.visualvariable.OrientationDial;
import msc.connoisseur.view.properties.panel.components.visualvariable.PositionControl;

import java.util.ArrayList;

public class VisualPropertiesPanel extends PropertiesPanel {
	private AtomicGrapheme grapheme;
	private ColorPicker color;
	private ChoiceBox<Grapheme> positionReference;
	private ChoiceBox<SizeValue> size;
	private ChoiceBox<Grapheme> sizeReference;
	private OrientationDial orientation;
	private PositionControl position;
	private Slider slider;

	public VisualPropertiesPanel(Controller controller, AtomicGrapheme grapheme){
		super(controller);
		this.grapheme = grapheme;
		initializeForm();
		addChangeBehavior();
	}
	private void addChangeBehavior() {
		VisualVector vv = grapheme.getVisualVector();
		position.addListener((x,y,z)->{getController().setValue(vv.getPosition(),new Pair<>(position.getValue(),positionReference.getValue()));});
		positionReference.valueProperty().addListener((x,y,newValue)->{new Pair<>(position.getValue(),newValue);});
		color.valueProperty().addListener((x,y,z) ->{
			getController().setValue(vv.getColor(), new Float[]{(float)(z.getHue()/360),(float)z.getSaturation(),(float)z.getBrightness()}); //, color.getValue().getHue()/360, color.getValue().getSaturation());
			getController().setValue(vv.getBrightness(), (float)(color.getValue().getBrightness()));});
		size.valueProperty().addListener((x,y,z)->
				getController().setValue(vv.getSize(), new Pair(size.getValue(), sizeReference.getValue())));
		sizeReference.valueProperty().addListener((x,y,newValue)->
			{if(y!=newValue)getController().setValue(vv.getSize(), new Pair(size.getValue(), newValue));});

	}

	private void initializeForm() {
		initOrientation();
		initPosition();
		initSize();
		initColor();

		reset();
		addChangeBehavior();
	}
	private void initColor() {
		color = new ColorPicker();
		GridPane cGrid = titledContainer("Color",new Node[][]{{simpleFormField("Color",color)}});
		add(cGrid);
	}
	private void initSize() {
		size = new ChoiceBox<SizeValue>();
		size.setItems(FXCollections.observableArrayList(SizeValue.values()));
		sizeReference = new ChoiceBox<Grapheme>();
		populateReferenceBox(sizeReference);
		GridPane siGrid = titledContainer("Size",new Node[][]{{simpleFormField("Size",size),simpleFormField("Relation",sizeReference)}});
		add(siGrid);
	}
	private void initPosition() {

		positionReference = new ChoiceBox<>();
		position = new PositionControl(positionReference);
		populateReferenceBox(positionReference);
		GridPane pGrid = titledContainer("Position",new Node[][]{{simpleFormField("Reference",positionReference),simpleFormField("Position",position)}});
		add(pGrid);
	}
	private void initOrientation() {
		VisualVector v = grapheme.getVisualVector();
		orientation = new OrientationDial(v.getOrientation().getValue());
		slider = new Slider();
		slider.setMax(359);
		slider.setMin(0);
		slider.setShowTickMarks(true);
		slider.setShowTickLabels(true);
		slider.setMajorTickUnit(45);
		slider.setMinorTickCount(2);
		slider.setSnapToTicks(true);
		slider.prefWidthProperty().bind(orientation.widthProperty());
		slider.setValue(orientation.getValue());
		orientation.rotateProperty().bind(slider.valueProperty().negate().add(360.0));
		slider.valueProperty().addListener((x,y,newValue) -> {getController().setValue(v.getOrientation(),newValue.intValue());});
		GridPane oGrid = titledContainer("Orientation", new Node[][]{{orientation, simpleFormField("Counter-clockwise rotation in degrees", slider)}});
		add(oGrid);
	}
	private void populateReferenceBox(ChoiceBox<Grapheme> cbox){
		DynamicContainer<Grapheme> parent = (DynamicContainer<Grapheme>) grapheme.getParent();
		ArrayList<Grapheme> list = new ArrayList<>();
		while(true){
			list.addAll(parent.getList());
			if(parent instanceof Graphemes){
				break;
			}else{
				parent = (DynamicContainer<Grapheme>) ((CompoundGrapheme) parent).getParent();
			}
		}
		cbox.setItems(FXCollections.observableArrayList(list));
	}
	@Override
	public void reset() {
		VisualVector v = grapheme.getVisualVector();
		positionReference.setValue(v.getPosition().getReference());
		position.setValue(v.getPosition().getValue().getKey());
		color.setValue(v.getColor().getFXColor());
		slider.setValue(v.getOrientation().getValue());
		size.setValue(v.getSize().getSizeValue());
		sizeReference.setValue(v.getSize().getRelation());
	}

}
