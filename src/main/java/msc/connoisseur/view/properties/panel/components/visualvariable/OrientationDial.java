package msc.connoisseur.view.properties.panel.components.visualvariable;

import javafx.beans.value.ChangeListener;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import java.util.*;
import java.util.List;

/**
 * Created by hartmjep on 11-10-2016.
 */
public class OrientationDial extends StackPane {
    private static final Image backgroundImage;
    Canvas foreground = new Canvas();
    ImageView background = new ImageView();
    int value = 0;
    static{
        backgroundImage = new Image("/img/wine.png");
    }

    private List<ChangeListener<Integer>> listeners = new ArrayList<>();

    public OrientationDial(int value){
        setPrefWidth(300);
        setPrefHeight(300);
        initialize();
        drawForeground();
        setValue(value);
    }
    private void initialize(){
        background.setImage(backgroundImage);
        background.fitHeightProperty().bind(this.heightProperty().divide(2));
        background.fitWidthProperty().bind(this.widthProperty().divide(2));
        this.getChildren().addAll(background,foreground);

    }
    private void drawForeground(){
        foreground.setHeight(300);
        foreground.setWidth(300);
        GraphicsContext gc = foreground.getGraphicsContext2D();
        gc.setStroke(javafx.scene.paint.Color.BLUE);
        gc.setFill(Color.BLUE);
        gc.setLineWidth(1);
        gc.strokeLine(foreground.getWidth()/2,foreground.getHeight()/2,foreground.getWidth(),foreground.getHeight()/2);
        gc.fillRect(foreground.getWidth()-5,foreground.getHeight()/2-2.5,5,5);
    }
    public int getValue(){
        return this.value;
    }
    public void setValue(int value){
        this.value = value;
        this.setRotate(value);
    }


}
