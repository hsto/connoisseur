package msc.connoisseur.view.properties.panel.grapheme.shape;

import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.GridPane;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.bertin.LineShape;
import msc.connoisseur.model.bertin.LineShape.Kind;
import msc.connoisseur.view.properties.panel.PropertiesPanel;

public class LineShapePanel extends PropertiesPanel {
	private LineShape model;
	private ChoiceBox<Kind> kind;
	private ChoiceBox<String> texture;

	public LineShapePanel(Controller controller,LineShape model){
		super(controller);
		this.model = model;
		initialize();
	}
	private void initialize() {
		kind = new ChoiceBox<>();
		kind.setItems(FXCollections.observableArrayList(Kind.values()));
		texture = new ChoiceBox<>(FXCollections.observableArrayList(model.getTexture().getTextureValues()));
		reset();
		kind.valueProperty().addListener((x,y,value)->{getController().setValue(model, value);});
		texture.valueProperty().addListener((x,y,value)->{getController().setValue(model.getTexture(), value);});
		Node[][] layout = {{simpleFormField("Kind",kind),simpleFormField("Texture",texture)}};
		GridPane grid = titledContainer("Line",layout);
		add(grid);
	}

	@Override
	public void reset() {
		texture.setValue(model.getTexture().getValue());
		kind.setValue(model.getValue());
	}

}
