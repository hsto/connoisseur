package msc.connoisseur.view.properties.panel.components.table;

import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.language.semantics.Domain;
import msc.connoisseur.model.language.semantics.Elements;

import java.util.HashMap;
import java.util.UUID;

public class DomainTable extends Table {
    private final Controller controller;
    private Elements elements;
    private HashMap<TableRow,Domain> mapping = new HashMap<>();
    public DomainTable(Controller controller, Elements elements){
        this.controller = controller;
        this.elements = elements;
        initialize();
    }
    private void initialize() {
        addTextColumn("ID",".*");
        addTextColumn("Name",".*");
        addTextColumn("Description",".*");
        addTextColumn("Reference",".*");
        for(Domain domain: elements.getDomainList()){
            TableCell<?>[] cells = getCellArray();
            ((TextCell)cells[0]).setValue(domain.getId());
            ((TextCell)cells[1]).setValue(domain.getName());
            ((TextCell)cells[2]).setValue(domain.getDescription());
            ((TextCell)cells[3]).setValue(domain.getReference());
            mapping.put(addTableRow(cells), domain);
        }
        ColumnConstraints iDColumnConstraints = new ColumnConstraints();
        ColumnConstraints nameColumnConstraints = new ColumnConstraints();
        ColumnConstraints descriptionColumnConstraints = new ColumnConstraints();
        ColumnConstraints referenceColumnConstraints = new ColumnConstraints();
        iDColumnConstraints.setPrefWidth(100);
        nameColumnConstraints.setPrefWidth(200);
        descriptionColumnConstraints.setPrefWidth(400);
        referenceColumnConstraints.setPrefWidth(200);
        this.getColumnConstraints().addAll(iDColumnConstraints,nameColumnConstraints,descriptionColumnConstraints,referenceColumnConstraints);
    }

    @Override
    protected void onNewBlankRow(TableRow tr) {
        ((TextField)tr.getCells()[0].getNode()).setText("D_"+ UUID.randomUUID().toString().substring(0,6));
    }

    @Override
    protected void updateRow(TableRow tr) {
        Domain c = mapping.get(tr);
        controller.changeId(c,((TextCell)tr.getCells()[0]).getValue());
        controller.changeName(c,((TextCell)tr.getCells()[1]).getValue());
        controller.changeDescription(c, ((TextCell)tr.getCells()[2]).getValue());
        controller.changeReference(c, ((TextCell)tr.getCells()[2]).getValue());
    }

    @Override
    protected void applyRow(TableRow tr) {
        Domain domain = new Domain();
        domain.setId(((TextCell)tr.getCells()[0]).getValue());
        domain.setName(((TextCell)tr.getCells()[1]).getValue());
        domain.setDescription(((TextCell)tr.getCells()[2]).getValue());
        domain.setReference(((TextCell)tr.getCells()[3]).getValue());
        mapping.put(tr, domain);
        controller.add(elements, domain);
    }

    @Override
    protected void removeRow(TableRow tr) {
        controller.remove(elements, mapping.remove(tr));
    }
}
