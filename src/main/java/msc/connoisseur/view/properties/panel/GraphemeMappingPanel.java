package msc.connoisseur.view.properties.panel;

import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.common.Pair;
import msc.connoisseur.model.language.Language;
import msc.connoisseur.model.language.semantics.Concept;
import msc.connoisseur.model.language.syntax.Grapheme;
import msc.connoisseur.view.properties.panel.components.table.MappingTable;
import msc.connoisseur.view.properties.panel.components.table.TableRow;

import java.util.List;

public class GraphemeMappingPanel extends PropertiesPanel {
	private MappingTable<Grapheme,Concept> table;
	public GraphemeMappingPanel(Controller controller,Language language){
		super(controller);
		table = new MappingTable<Grapheme, Concept>() {
			@Override
			protected void onNewBlankRow(TableRow tr) {

			}

			@Override
			protected List<? extends Pair<Grapheme, Concept>> getCurrentMappings() {
				return language.getSemantics().getGraphemeMapping().getList();
			}

			@Override
			protected List<Concept> getSecondColumnChoices() {
				return language.getSemantics().getElements().getConceptList();
			}

			@Override
			protected List<Grapheme> getFirstColumnChoices() {
				return language.getSyntax().getGraphemes().getList();
			}

			@Override
			protected String getSecondColumnName() {
				return "Concepts";
			}

			@Override
			protected String getFirstColumnName() {
				return "Graphemes";
			}

			@Override
			protected void addMapping(Pair<Grapheme, Concept> newpair) {
				getController().add(language.getSemantics().getGraphemeMapping(),newpair);
			}

			@Override
			protected void removeMapping(Pair<Grapheme, Concept> oldpair) {
				getController().remove(language.getSemantics().getGraphemeMapping(),oldpair);
			}
		};
		table.setOnChangedAction(()->table.apply());
		ScrollPane sc = new ScrollPane(table);
		sc.setPrefWidth(770);
		sc.setPrefHeight(400);
		sc.setHbarPolicy(ScrollBarPolicy.NEVER);
		add(sc);
	}
	@Override
	public void reset() {
		table.reset();
	}

}
