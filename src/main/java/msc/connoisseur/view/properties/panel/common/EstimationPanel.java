package msc.connoisseur.view.properties.panel.common;

import javafx.scene.Node;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.assessment.Estimatable;
import msc.connoisseur.view.properties.panel.PropertiesPanel;
import msc.connoisseur.view.utility.NumberTextField;
public class EstimationPanel extends PropertiesPanel {
	Estimatable model;
	Slider estimationSlider;
	NumberTextField estimationField;
	TextArea reason;
	public EstimationPanel(Controller controller,Estimatable model){
		super(controller);
		this.model = model;
		initialize();
	}
	private void initialize() {
		estimationSlider = new Slider();
		estimationSlider.setMax(10);
		estimationSlider.setMin(0);
		estimationSlider.setMajorTickUnit(5);
		estimationSlider.setMinorTickCount(4);
		estimationField = new NumberTextField();
		estimationField.setPrefWidth(60);
		estimationField.setEditable(false);
		estimationField.textProperty().bind(estimationSlider.valueProperty().asString("%.0f"));
		reason = new TextArea();
		reason.setPromptText("Reason for the estimate");
		reset();
		reason.focusedProperty().addListener((x,y,focused)->{if(!focused)getController().changeReason(model, reason.getText());});
		estimationSlider.setShowTickMarks(true);
		estimationSlider.setSnapToTicks(true);
		estimationSlider.valueProperty().addListener((x,y,n)->{if(!estimationSlider.isValueChanging()){getController().changeEstimate(model,estimationSlider.getValue()/estimationSlider.getMax());}});
		GridPane reasonField = simpleFormField("Reason",reason);
		Node[][] layout = {{estimationSlider,reasonField},{simpleFormField("Estimate",estimationField)}};
		GridPane.setColumnSpan(reasonField, 2);
		GridPane grid = titledContainer("Estimation",layout);
		add(grid);
	}


	@Override
	public void reset() {
		estimationSlider.setValue(model.getEstimate()*estimationSlider.getMax());
		reason.setText(model.getReason());
	}

}
