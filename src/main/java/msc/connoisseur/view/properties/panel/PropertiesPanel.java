package msc.connoisseur.view.properties.panel;

import javafx.geometry.Insets;
import javafx.scene.layout.ColumnConstraints;
import msc.connoisseur.control.Controller;
import msc.connoisseur.view.ConnoiseurPanel;

public abstract class PropertiesPanel extends ConnoiseurPanel {
	public PropertiesPanel(Controller controller){
		super(controller);
		getStyleClass().add("msc.connoisseur.tests.properties-panel");
		this.setPadding(new Insets(10));
		this.setHgap(10);
		this.setVgap(10);
	}



	public abstract void reset();
	@Override
	protected ColumnConstraints[] subColumnConstraints(){
		return new ColumnConstraints[]{};
	}
}
