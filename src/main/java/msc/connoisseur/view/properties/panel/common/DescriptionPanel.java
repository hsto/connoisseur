package msc.connoisseur.view.properties.panel.common;

import javafx.scene.control.TextArea;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.view.properties.panel.PropertiesPanel;

public class DescriptionPanel extends PropertiesPanel {
	TextArea descriptionArea = new TextArea();
	private ConnoisseurObject model;
	public DescriptionPanel(Controller controller, ConnoisseurObject model){
		super(controller);
		this.model = model;
		init();
	}
	private void init() {
		add(simpleFormField("Description",descriptionArea));
		descriptionArea.setPromptText("Description");
		reset();
		descriptionArea.focusedProperty().addListener((x,y,focused)->{
			if(!focused)getController().changeDescription(model, descriptionArea.getText());
		});
	}
	@Override
	public void reset() {
		descriptionArea.setText(model.getDescription());
	}

}
