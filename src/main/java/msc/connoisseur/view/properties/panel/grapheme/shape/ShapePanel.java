package msc.connoisseur.view.properties.panel.grapheme.shape;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.bertin.IconShape;
import msc.connoisseur.model.bertin.LabelShape;
import msc.connoisseur.model.bertin.LineShape;
import msc.connoisseur.model.bertin.RegionShape;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.bertin.UndefinedShape;
import msc.connoisseur.model.bertin.Shape.ShapeClass;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.view.properties.panel.PropertiesPanel;

public class ShapePanel extends PropertiesPanel {
	ChoiceBox<ShapeClass> shapeChooser = new ChoiceBox<>();
	PropertiesPanel subShapePanel = null;
	GridPane grid;
	AtomicGrapheme model;
	public ShapePanel(Controller controller,AtomicGrapheme model){
		super(controller);
		this.model = model;
		initialize();
	}
	private void initialize() {
		Label shapeLabel = new Label("Shape");
		shapeLabel.getStyleClass().add("variable-header");
		grid = titledContainer("Shape");
		grid.add(shapeChooser,0,1);
		add(grid);
		shapeChooser.getItems().addAll(ShapeClass.values());
		reset();
		shapeChooser.valueProperty().addListener((x,y,newValue)->{
			Shape s = model.getVisualVector().getShape();
			if(s.getValue()!=newValue){
				switch(newValue){
				case region:
					s = new RegionShape();
					break;
				case label:
					s = new LabelShape();
					break;
				case line:
					s = new LineShape();
					break;
				case icon:
					s = new IconShape();
					break;
				default:
					s = new UndefinedShape();
					break;
				}
				getController().changeShape(model,s);
				resetSubShapePanel();
			}
		});

	}

	@Override
	public void reset() {
		shapeChooser.setValue(model.getVisualVector().getShape().getShapeClass());
		resetSubShapePanel();
		if(subShapePanel!=null)subShapePanel.reset();
	}
	private void resetSubShapePanel() {
		subShapePanel = getShapePanel(model.getVisualVector().getShape().getShapeClass());
		if(subShapePanel!=null){
			grid.add(subShapePanel,0,2,GridPane.REMAINING,GridPane.REMAINING);
		}else
			getChildren().remove(subShapePanel);
	}

	private PropertiesPanel getShapePanel(ShapeClass shapeType) {
		Shape shape = model.getVisualVector().getShape();
		switch(shapeType){
		case region:
			return new RegionShapePanel(getController(),(RegionShape) shape);
		case label:
			return new LabelShapePanel(getController(),(LabelShape) shape);
		case line:
			return new LineShapePanel(getController(),(LineShape) shape);
		case icon:
			return new IconShapePanel(getController(),(IconShape) shape);
		default:
			return null;
		}
	}
}
