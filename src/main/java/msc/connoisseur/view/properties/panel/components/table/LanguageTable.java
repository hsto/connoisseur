package msc.connoisseur.view.properties.panel.components.table;

import javafx.scene.layout.ColumnConstraints;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.ConnoisseurProject;
import msc.connoisseur.model.LanguageSystem;
import msc.connoisseur.model.language.Language;

import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Created by jeppe on 14-11-2016.
 */
public class LanguageTable extends Table {
    private final LanguageSystem languageSystem;
    private final Controller controller;
    private HashMap<TableRow,Language> mapping = new HashMap<>();

    public LanguageTable(LanguageSystem languageSystem, Controller controller){
        super();
        this.controller = controller;
        this.languageSystem = languageSystem;
        initialize();
    }

    private void initialize() {
        addChoiceColumn("Language",x -> {
            ConnoisseurProject project = languageSystem.getParent();
            return project.getList().stream()
                    .filter(y -> y instanceof Language)
                    .map(y -> (Language) y)
                    .filter(y -> !mapping.containsValue(y))
                    .collect(Collectors.toList());
        });
        for(Language l : languageSystem.getLanguages()){
            TableCell<?>[] cells = getCellArray();
            ((ChoiceCell<Language>)cells[0]).setValue(l);
            mapping.put(addTableRow(cells), l);
        }
        ColumnConstraints c0 = new ColumnConstraints();
        c0.setPrefWidth(400);
        this.getColumnConstraints().addAll(c0);
    }

    @Override
    protected void onNewBlankRow(TableRow tr) {

    }

    @Override
    protected void updateRow(TableRow tr) {
        Language l = mapping.remove(tr);
        Language l2 = (Language) tr.getCells()[0].getValue();
        controller.replace(languageSystem,l,l2);
        mapping.put(tr,l2);
    }

    @Override
    protected void applyRow(TableRow tr) {
        Language l = (Language)tr.getCells()[0].getValue();
        controller.add(languageSystem, l);
        mapping.put(tr,l);
    }

    @Override
    protected void removeRow(TableRow tr) {
        controller.remove(languageSystem,(Language)tr.getCells()[0].getValue());
        mapping.remove(tr);
    }
}
