package msc.connoisseur.view.properties.panel.grapheme.shape;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.bertin.IconShape;
import msc.connoisseur.view.properties.panel.PropertiesPanel;

public class IconShapePanel extends PropertiesPanel {
	private IconShape model;
	private ImageView icon;
	private Button setImageButton;
	private ChoiceBox<String> texture;
	public IconShapePanel(Controller controller, IconShape model){
		super(controller);
		this.model = model;
		initialize();
	}
	private void initialize() {
		icon = new ImageView();
		icon.setFitHeight(200);
		icon.setPreserveRatio(true);
		icon.setSmooth(true);
		icon.setCache(true);
		icon.setImage(getImage());
		setImageButton = new Button("Browse");
		texture = new ChoiceBox<>(FXCollections.observableArrayList(model.getTexture().getTextureValues()));
		reset();
		setImageButton.setOnAction(x -> imageButtonAction());
		texture.valueProperty().addListener((x,y,value)->getController().setValue(model.getTexture(),value));
		Node[][] layout = {{icon,setImageButton},{simpleFormField("Texture",texture)}};
		GridPane grid = titledContainer("Icon",layout);
		add(grid);
	}
	private void imageButtonAction() {
		FileChooser fc = new FileChooser();//".jpg",".jpeg",".JPG",".JPEG",".png",".PNG",".bmp",".BMP",".gif",".GIF"
		fc.getExtensionFilters().add(new ExtensionFilter("Bitmap File","*.jpg","*.jpeg","*.JPG","*.JPEG","*.png","*.PNG","*.bmp","*.BMP","*.gif","*.GIF"));
		try {
			InputStream is = new FileInputStream(fc.showOpenDialog(this.getScene().getWindow()).getAbsolutePath());
			setImage(is);
			getController().setValue(model, icon.getImage());
			is.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void setImage(InputStream is){
		this.icon.setImage(new Image(is));
	}
	private Image getImage() {
		Image img = model.getValue();
		if(img==null)
			img = new Image(this.getClass().getResourceAsStream("/img/phImg.png"));
		return img;
	}
	@Override
	public void reset() {
		texture.setValue(model.getTexture().getValue());
		this.icon.setImage(getImage());
	}

}
