package msc.connoisseur.view.properties.panel.grapheme.shape;

import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.bertin.LabelShape;
import msc.connoisseur.view.properties.panel.PropertiesPanel;

public class LabelShapePanel extends PropertiesPanel {
	private TextField grammar;
	private LabelShape model;
	private ComboBox<String> texture;

	public LabelShapePanel(Controller controller,LabelShape model){
		super(controller);
		this.model = model;
		initialize();
	}
	private void initialize() {
		grammar = new TextField(model.getValue());
		grammar.focusedProperty().addListener((x,y,focused)->{if(!focused)getController().setValue(model, grammar.getText());});
		grammar.setPromptText("Grammar e.g. regular expression");
		grammar.setPrefWidth(500);
		initializeTextureBox();
		GridPane field = simpleFormField("Grammar",grammar);
		Node[][] layout = {{field},{simpleFormField("Texture",texture)}};
		GridPane grid = titledContainer("Label", layout);
		add(grid);
	}

	private void initializeTextureBox() {
		texture = new ComboBox<>(FXCollections.observableArrayList(model.getTexture().getTextureValues()));
		texture.valueProperty().addListener((x,y,value) -> getController().setValue(model.getTexture(),value));
		texture.setEditable(false);
		texture.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
			@Override
			public ListCell<String> call(ListView<String> param) {
				return new ListCell<String>(){
					@Override
					protected void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (item != null) {
							setText(item);
							boolean bold = item.toLowerCase().contains("bold");
							boolean italic = item.toLowerCase().contains("italic");
							Font f = Font
									.font(null,
											bold? FontWeight.BOLD : FontWeight.MEDIUM,
											italic? FontPosture.ITALIC : FontPosture.REGULAR, 16);
							if(item.toLowerCase().contains("underlined"))
								this.setUnderline(true);
							setFont(f);
						}
					}
				};
			}
		});

	}


	@Override
	public void reset() {
		texture.setValue(model.getTexture().getValue());
		grammar.setText(model.getValue());
	}

}
