package msc.connoisseur.view.properties.panel;

import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.language.semantics.Elements;
import msc.connoisseur.view.properties.panel.components.table.DomainTable;

/**
 * Created by jeppe on 09-10-2016.
 */
public class DomainsPanel extends PropertiesPanel{
    private DomainTable table;
    public DomainsPanel(Controller controller, Elements elements) {
        super(controller);
        table = new DomainTable(controller, elements);
        table.setOnChangedAction(()->table.apply());
        ScrollPane sc = new ScrollPane(table);
        sc.setPrefWidth(970);
        sc.setPrefHeight(400);
        sc.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        add(titledContainer("Domains",new Node[][]{{sc}}));
    }
    @Override
    public void reset() {
        table.reset();
    }
}
