package msc.connoisseur.view.properties.panel.components.visualvariable;

import javafx.beans.value.ChangeListener;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import msc.connoisseur.model.bertin.LabelShape;
import msc.connoisseur.model.bertin.LineShape;
import msc.connoisseur.model.bertin.Position;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.language.syntax.Grapheme;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hartmjep on 12-10-2016.
 */
public class PositionControl extends StackPane {
    private List<Position.CardinalDirection> value;
    private RadioButton[][] buttons;
    private GridPane buttonPane;
    private HashMap<RadioButton,List<Position.CardinalDirection>> map = new HashMap<>();
    ToggleGroup group = new ToggleGroup();
    public PositionControl(ChoiceBox<Grapheme> references){
        references.valueProperty().addListener(referenceListener());
        initialize(references.getValue());
    }

    private ChangeListener<? super Grapheme> referenceListener() {
        return(property,oldvalue,newvalue) -> {
            initialize(newvalue);
        };
    }

    private void initialize(Grapheme newvalue) {
        getChildren().clear();
        buttonPane = new GridPane();
        map.clear();
        if(newvalue==null) {
            initializeBox();
            return;
        }else {
            Shape s = newvalue.getVisualVector().getShape();
            if (s instanceof LineShape || s instanceof LabelShape)
                initializeLine();
            else
                initializeBox();
        }
    }

    private void initializeLine() {
        buttons = new RadioButton[3][7];
        for(int i = 0; i < buttons.length; i++){
            for( int j = 0; j < buttons[i].length; j++){

                RadioButton radioButton = new RadioButton();
                setMapping(radioButton,i-1,j-3);
                buttons[i][j] = radioButton;
                radioButton.setToggleGroup(group);
                radioButton.getStyleClass().add("position-button");
                if(i == 1 && j > 0 && j < 6){
                    radioButton.getStyleClass().add("position-border-button");
                }
                if(i == 1 && j == 3){
                    radioButton.getStyleClass().add("position-center-button");
                }
                buttonPane.add(radioButton,j,i);
            }
        }
        Label border = new Label("On border");
        border.setGraphic(new Rectangle(15,15, Color.AQUA));
        buttonPane.add(border,1,8,GridPane.REMAINING,1);
        Label centered = new Label("Centered");
        centered.setGraphic(new Rectangle(15,15, Color.rgb(0xAD,0xFF,0x2F)));
        buttonPane.add(centered,1,9,GridPane.REMAINING,1);
        buttonPane.setVgap(5);
        buttonPane.setBackground(Background.EMPTY);
        this.getChildren().addAll(buttonPane);
    }

    private void initializeBox(){
        buttons = new RadioButton[7][7];
        for(int i = 0; i < buttons.length; i++){
            for( int j = 0; j < buttons[i].length; j++){

                RadioButton radioButton = new RadioButton();
                setMapping(radioButton,i-3,j-3);
                buttons[i][j] = radioButton;
                radioButton.setToggleGroup(group);
                radioButton.getStyleClass().add("position-button");
                if(((i == 1 || i == 5) && j > 0 && j < 6)||((j == 1 || j == 5) && i > 0 && i < 6)){
                    radioButton.getStyleClass().add("position-border-button");
                }
                if(i == 3 && j == 3){
                    radioButton.getStyleClass().add("position-center-button");
                }
                buttonPane.add(radioButton,j,i);
            }
        }
        Label border = new Label("On border");
        border.setGraphic(new Rectangle(15,15, Color.AQUA));
        buttonPane.add(border,1,8,GridPane.REMAINING,1);
        Label centered = new Label("Centered");
        centered.setGraphic(new Rectangle(15,15, Color.rgb(0xAD,0xFF,0x2F)));
        buttonPane.add(centered,1,9,GridPane.REMAINING,1);
        buttonPane.setVgap(5);
        buttonPane.setBackground(Background.EMPTY);
        this.getChildren().addAll(buttonPane);

    }
    public void addListener(ChangeListener<? super Toggle> listener){
        group.selectedToggleProperty().addListener(listener);
    }
    private void setMapping(RadioButton radioButton, int y, int x) {
        List<Position.CardinalDirection> output = new ArrayList<>();
        for(int i = y; i > 0; i--)
            output.add(Position.CardinalDirection.South);
        for(int i = y; i < 0; i++)
            output.add(Position.CardinalDirection.North);
        for(int i = x; i < 0; i++)
            output.add(Position.CardinalDirection.West);
        for(int i = x; i > 0; i--)
            output.add(Position.CardinalDirection.East);
        map.put(radioButton,output);
    }

    public List<Position.CardinalDirection> getValue(){
        RadioButton rb = (RadioButton) group.getSelectedToggle();
        return map.get(rb);
    }
    public void setValue(List<Position.CardinalDirection> value){
        int y = (buttons.length-1)/2, x = (buttons[y].length-1)/2;
        for(Position.CardinalDirection cd : value){
            switch (cd){
                case East:
                    x++;
                    break;
                case West:
                    x--;
                    break;
                case North:
                    y--;
                    break;
                case South:
                    y++;
                    break;
            }
        }
        buttons[y][x].setSelected(true);
    }
}
