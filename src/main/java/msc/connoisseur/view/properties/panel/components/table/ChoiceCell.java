package msc.connoisseur.view.properties.panel.components.table;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;

public abstract class ChoiceCell<T> extends TableCell<T> {
	private ChoiceBox<T> node;
	public ChoiceCell(TableRow row,ChoiceColumn<T> column) {
		super(row, column);
		initNode();
	}

	private void initNode() {
		this.node = new ChoiceBox<T>();
		this.node.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		this.node.setItems(FXCollections.observableArrayList(getColumn().getOptions().apply(null)));
		this.node.managedProperty().bind(this.node.visibleProperty());
		this.node.getSelectionModel().select(this.getValue());
		this.node.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<T>(){
			@Override
			public void changed(ObservableValue<? extends T> observable, T oldValue, T newValue) {
				onChanged(oldValue,newValue);
			}
		});
	}
	@Override
	public T getValue(){
		return node.getSelectionModel().getSelectedItem();
	}
	@Override
	public void setValue(T value){
		node.getSelectionModel().select(value);
	}
	@Override
	public ChoiceColumn<T> getColumn(){
		return (ChoiceColumn<T>) super.getColumn();
	}

	@Override
	public ChoiceBox<T> getNode() {
		return node;
	}

}
