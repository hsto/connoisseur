package msc.connoisseur.view.properties.panel.components.table;

import java.util.List;
import java.util.function.Function;

public class ChoiceColumn<T> extends TableColumn<T> {
	private Function<Void, List<T>> options;

	public ChoiceColumn(String title, Function<Void, List<T>> options) {
		super(title);
		this.setOptions(options);
	}

	public Function<Void, List<T>> getOptions() {
		return options;
	}

	public void setOptions(Function<Void, List<T>> choices) {
		this.options = choices;
	}

	@Override
	public ChoiceCell<T> createCell(TableRow row,Function<TableCell<?>,Void> onCellChangedAction) {
		ChoiceCell<T> cell = new ChoiceCell<T>(row,this){
			@Override
			protected void onChanged(T oldvalue, T newvalue) {
				onCellChangedAction.apply(this);
			}
		};
		return cell;
	}
}
