package msc.connoisseur.view.properties.panel.components.table;

import java.util.function.Function;

public abstract class TableColumn<T> {
	private String title;
	public TableColumn(String title) {
		this.title = title;
	}

	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public abstract TableCell<T> createCell(TableRow row, Function<TableCell<?>, Void> onCellChangedAction);



}
