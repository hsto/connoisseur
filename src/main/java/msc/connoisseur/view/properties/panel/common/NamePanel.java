package msc.connoisseur.view.properties.panel.common;

import javafx.scene.control.TextField;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.view.properties.panel.PropertiesPanel;

public class NamePanel extends PropertiesPanel {
	public final int NAME = 0;
	private ConnoisseurObject model;
	protected TextField nameField = new TextField();
	protected TextField idField = new TextField();
	public NamePanel(Controller controller,ConnoisseurObject co){
		super(controller);
		model = co;
		init();
	}
	private void init() {
		add(simpleFormField("Name",nameField));
		add(simpleFormField("Id",idField));
		idField.setPromptText("ID");
		nameField.setPromptText("Name");
		reset();
		idField.focusedProperty().addListener((x,y,focused)->{if(!focused)getController().changeId(model, idField.getText());});
		nameField.focusedProperty().addListener((x,y,focused)->{if(!focused)getController().changeName(model, nameField.getText());});
	}
	@Override
	public void reset() {
		nameField.setText(model.getName());
		idField.setText(model.getId());
	}
}
