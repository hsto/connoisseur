package msc.connoisseur.view.properties.panel.components.table;

import java.util.HashMap;
import java.util.List;

import javafx.scene.layout.ColumnConstraints;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.common.Pair;

public abstract class MappingTable<X,Y> extends Table {
    private HashMap<TableRow,Pair<X,Y>> mapping = new HashMap<>();
    public MappingTable(){
        initialize();
    }
    @SuppressWarnings("unchecked")
    private void initialize() {
        addChoiceColumn(getFirstColumnName(),(Void v)->getFirstColumnChoices());
        addChoiceColumn(getSecondColumnName(),(Void v)->getSecondColumnChoices());
        for(Pair<X,Y> pair : getCurrentMappings()){
        	TableCell<?>[] cells = getCellArray();
            ((ChoiceCell<X>)cells[0]).setValue(pair.a);
            ((ChoiceCell<Y>)cells[1]).setValue(pair.b);
            mapping.put(addTableRow(cells), pair);
        }
        ColumnConstraints c0 = new ColumnConstraints();
        ColumnConstraints c1 = new ColumnConstraints();
        c0.setPrefWidth(350);
        c1.setPrefWidth(350);
        this.getColumnConstraints().addAll(c0,c1);
    }

    protected abstract List<? extends Pair<X,Y>> getCurrentMappings();

    protected abstract <Y extends ConnoisseurObject> List<Y> getSecondColumnChoices();

    protected abstract <X extends ConnoisseurObject> List<X> getFirstColumnChoices();

    protected abstract String getSecondColumnName();

    protected abstract String getFirstColumnName();

    @Override
    protected void updateRow(TableRow tr) {
    	if(tr.getCells()[0].getValue() == null
    			|| tr.getCells()[1].getValue() == null)return;
        Pair<X,Y> oldpair = mapping.remove(tr);
        if(oldpair.b!=null&&oldpair.a!=null)
        	removeMapping(oldpair);//Controller.remove(language.getSemantics().getGraphemeMapping(),oldpair);
        Pair<X,Y> newpair = mappingFromRow(tr);
        addMapping(newpair);//Controller.add(language.getSemantics().getGraphemeMapping(),newpair);
        mapping.put(tr, newpair);
    }

    protected abstract void addMapping(Pair<X, Y> newpair);

    protected abstract void removeMapping(Pair<X, Y> oldpair);

    @Override
    protected void applyRow(TableRow tr) {
    	if(tr.getCells()[0].getValue() == null
    			&& tr.getCells()[1].getValue() == null)return;
    	Pair<X,Y> pair = mappingFromRow(tr);

        mapping.put(tr, pair);
        if(validMapping(tr))
            addMapping(pair);
    }

    private boolean validMapping(TableRow tr) {
        return tr.getCells()[0].getValue() != null
                && tr.getCells()[1].getValue() != null;
    }

    @Override
    protected void removeRow(TableRow tr) {
    	Pair<X,Y> pair = mappingFromRow(tr);
        removeMapping(pair);
        mapping.remove(tr, pair);

    }

    private Pair<X,Y> mappingFromRow(TableRow tr){
        return new Pair<>(
            (X) tr.getCells()[0].getValue(),
            (Y) tr.getCells()[1].getValue());
    }

}
