package msc.connoisseur.view.properties.panel.components.table;

import java.util.function.Function;

public class TextColumn extends TableColumn<String> {

	private String regex;

	public TextColumn(String name, String regex) {
		super(name);
		this.setRegex(regex);
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	@Override
	public TableCell<String> createCell(TableRow row,Function<TableCell<?>,Void> onCellChangedAction) {
		TextCell tc = new TextCell(row,this){
			@Override
			protected void onChanged(String oldvalue, String newvalue) {
				onCellChangedAction.apply(this);
			}
		};
		return tc;
	}

}
