package msc.connoisseur.view.properties.panel.grapheme;

import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.language.syntax.CompoundGrapheme;
import msc.connoisseur.model.language.syntax.Grapheme;
import msc.connoisseur.view.properties.panel.PropertiesPanel;

public class CompoundGraphemePanel extends PropertiesPanel {
	CompoundGrapheme model;
	ChoiceBox<Grapheme> base;
	public CompoundGraphemePanel(Controller controller, CompoundGrapheme model){
		super(controller);
		this.model = model;
		initializeComponents();
	}

	private void initializeComponents() {
		base = new ChoiceBox<>();
		base.setItems(FXCollections.observableArrayList(model.getList()));
		base.valueProperty().addListener((x,old,newval)-> getController().setBaseGrapheme(model,newval));
		add(simpleFormField("Base Grapheme",base));
	}

	@Override
	public void reset() {
		try{
			base.setValue(model.get(0));
		}catch(IndexOutOfBoundsException e){
			base.setValue(null);
		}
	}

}
