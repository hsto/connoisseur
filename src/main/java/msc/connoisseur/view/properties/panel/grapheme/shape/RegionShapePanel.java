package msc.connoisseur.view.properties.panel.grapheme.shape;

import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.bertin.RegionShape;
import msc.connoisseur.model.bertin.RegionShape.CornerFeatures;
import msc.connoisseur.model.bertin.RegionShape.Kind;
import msc.connoisseur.view.properties.panel.PropertiesPanel;

public class RegionShapePanel extends PropertiesPanel {
	private RegionShape model;
	private ChoiceBox<Kind> kind;
	private ChoiceBox<CornerFeatures> corners;
	private ChoiceBox<String> texture;

	public RegionShapePanel(Controller controller,RegionShape model){
		super(controller);
		this.model = model;
		initialize();
	}
	private void initialize() {
		kind = new ChoiceBox<>(FXCollections.observableArrayList(Kind.values()));
		corners = new ChoiceBox<>(FXCollections.observableArrayList(CornerFeatures.values()));
		texture = new ChoiceBox<>(FXCollections.observableArrayList(model.getTexture().getTextureValues()));
		reset();
		kind.valueProperty().addListener((x,y,value)->getController().setValue(model,new Pair(value,model.getCornerFeatures())));
		corners.valueProperty().addListener((x,y,value)->getController().setValue(model, new Pair(model.getKind(),value)));
		texture.valueProperty().addListener((x,y,value)->getController().setValue(model.getTexture(),value));
		Node[][] layout = {{simpleFormField("Kind",kind),simpleFormField("Corners",corners)},{simpleFormField("Texture",texture)}};
		GridPane grid = titledContainer("Region",layout);
		add(grid);
	}

	@Override
	public void reset() {
		kind.setValue(model.getKind());
		corners.setValue(model.getCornerFeatures());
		texture.setValue(model.getTexture().getValue());
	}


}
