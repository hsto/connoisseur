package msc.connoisseur.view.properties.panel;

import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.language.semantics.Elements;
import msc.connoisseur.view.properties.panel.components.table.ConceptTable;

public class ConceptsPanel extends PropertiesPanel {
	private ConceptTable table;
	public ConceptsPanel(Controller controller, Elements elements){
		super(controller);
		table = new ConceptTable(controller,elements);
		table.setOnChangedAction(()->table.apply());
		ScrollPane sc = new ScrollPane(table);
		sc.setPrefWidth(970);
		sc.setPrefHeight(400);
		sc.setHbarPolicy(ScrollBarPolicy.NEVER);
		add(titledContainer("Concepts",new Node[][]{{sc}}));
	}
	@Override
	public void reset() {
		table.reset();
	}


}
