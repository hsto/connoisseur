package msc.connoisseur.view.properties.panel.components.table;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

public abstract class TextCell extends TableCell<String> {
	private boolean matchesRegex = false;
	private TextField node;
	public TextCell(TableRow row,TextColumn column) {
		super(row,column);
		this.node = new TextField("");
		this.node.managedProperty().bind(this.node.visibleProperty());
		this.node.textProperty().addListener(new ChangeListener<String>(){
			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				matchesRegex = arg2.matches(getColumn().getRegex());
				onChanged(arg1,arg2);
			}
		});
	}
	@Override
	public String getValue(){
		return node.getText();
	}
	@Override
	public void setValue(String value){
		node.setText(value);
	}
	public boolean matchesRegex(){
		return this.matchesRegex;
	}
	@Override
	public TextColumn getColumn(){
		return (TextColumn) super.getColumn();
	}
	@Override
	public TextField getNode() {
		return this.node;
	}

}
