package msc.connoisseur.view.properties.panel.components.table;

import java.util.HashMap;
import java.util.UUID;

import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.language.semantics.Concept;
import msc.connoisseur.model.language.semantics.Elements;

public class ConceptTable extends Table {
	private final Controller controller;
	private Elements elements;
	private HashMap<TableRow,Concept> mapping = new HashMap<>();
	public ConceptTable(Controller controller, Elements elements){
		this.controller = controller;
		this.elements = elements;
		initialize();
	}
	private void initialize() {
		addTextColumn("ID",".*");
		addTextColumn("Name",".*");
		addTextColumn("Description",".*");
		addTextColumn("Reference",".*");
		for(Concept concept: elements.getConceptList()){
			TableCell<?>[] cells = getCellArray();
			((TextCell)cells[0]).setValue(concept.getId());
			((TextCell)cells[1]).setValue(concept.getName());
			((TextCell)cells[2]).setValue(concept.getDescription());
			((TextCell)cells[3]).setValue(concept.getReference());
			mapping.put(addTableRow(cells), concept);
		}
		ColumnConstraints iDColumnConstraints = new ColumnConstraints();
		ColumnConstraints nameColumnConstraints = new ColumnConstraints();
		ColumnConstraints descriptionColumnConstraints = new ColumnConstraints();
		ColumnConstraints referenceColumnConstraints = new ColumnConstraints();
		iDColumnConstraints.setPrefWidth(100);
		nameColumnConstraints.setPrefWidth(200);
		descriptionColumnConstraints.setPrefWidth(400);
		referenceColumnConstraints.setPrefWidth(200);
		this.getColumnConstraints().addAll(iDColumnConstraints,nameColumnConstraints,descriptionColumnConstraints,referenceColumnConstraints);
	}

	@Override
	protected void onNewBlankRow(TableRow tr) {
		((TextField)tr.getCells()[0].getNode()).setText("C_"+UUID.randomUUID().toString().substring(0,6));
	}

	@Override
	protected void updateRow(TableRow tr) {
		Concept concept = mapping.get(tr);
		controller.changeId(concept,((TextCell)tr.getCells()[0]).getValue());
		controller.changeName(concept,((TextCell)tr.getCells()[1]).getValue());
		controller.changeDescription(concept, ((TextCell)tr.getCells()[2]).getValue());
		controller.changeReference(concept,((TextCell)tr.getCells()[3]).getValue() );
	}

	@Override
	protected void applyRow(TableRow tr) {
		Concept concept = new Concept();

		concept.setId(((TextCell)tr.getCells()[0]).getValue());
		concept.setName(((TextCell)tr.getCells()[1]).getValue());
		concept.setDescription(((TextCell)tr.getCells()[2]).getValue());
		concept.setReference(((TextCell)tr.getCells()[3]).getValue());
		mapping.put(tr, concept);
		controller.add(elements, concept);
	}

	@Override
	protected void removeRow(TableRow tr) {
		controller.remove(elements, mapping.remove(tr));
	}

}
