package msc.connoisseur.view.properties.panel.components.table;

import java.util.ArrayList;
import java.util.function.Function;

import javafx.geometry.Insets;
import javafx.scene.control.Button;

public class TableRow {
	private boolean visible;
	private Button deleteButton;
	private TableCell<?>[] cells;
	public TableRow(ArrayList<TableColumn<?>> columns, Function<TableCell<?>, Void> onCellChangedAction) {
		deleteButton = new Button("del");
		deleteButton.setPadding(Insets.EMPTY);
		deleteButton.getStyleClass().add("connoisseur-table-delete-button");
		deleteButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		deleteButton.managedProperty().bind(deleteButton.visibleProperty());
		this.cells = new TableCell[columns.size()];
		for(int i = 0; i < cells.length; i++)
			cells[i] = columns.get(i).createCell(this,onCellChangedAction);
		setVisible(true);
	}
	public void enterAll(TableCell<?>[] cells) {
		for(int i = 0; i < cells.length; i++)
			if(this.cells[i].getColumn() == cells[i].getColumn()){
				this.cells[i] = cells[i];
				this.cells[i].setRow(this);
			}
		setVisible(true);
	}
	public TableCell<?>[] getCells() {
		return cells;
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
		deleteButton.setVisible(visible);
		for(TableCell<?> cell: cells)
			cell.setVisible(visible);
	}
	public Button getDeleteButton() {
		return deleteButton;
	}

}
