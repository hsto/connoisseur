package msc.connoisseur.view.properties.panel.components;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Line;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.bertin.*;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.common.DynamicContainer;
import msc.connoisseur.view.properties.panel.PropertiesPanel;
import msc.connoisseur.view.utility.NumberTextField;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by hartmjep on 13-10-2016.
 */
public class WeightPanel extends PropertiesPanel {
    private final Assessment owner;
    private ObjectProperty<ConnoisseurObject> scope;
    private List<Consumer<Assessment>> resetActions = new ArrayList<>();

    public WeightPanel(Controller controller, Assessment owner){
        super(controller);
        this.owner = owner;
        this.scope = new SimpleObjectProperty<>();
        this.scope.addListener(initialize());
    }

    private ChangeListener<? super ConnoisseurObject> initialize() {
        return (property,oldValue,newValue) -> {
            resetActions.clear();
            if(newValue == null) return;
            initializeWeightFields();
        };
    }

    private void initializeWeightFields() {
        getChildren().clear();
        ConnoisseurObject root = scopeProperty().getValue();
        GridPane pane = generateVVWeightFields();
        add(pane);
        add(generateField(root));
    }

    private GridPane generateVVWeightFields() {
        Node[][] layout = {{new FlowPane(
                generateVVWeightField("Color",Color.class),
                generateVVWeightField("Brightness",Brightness.class),
                generateVVWeightField("Position",Position.class),
                generateVVWeightField("Orientation",Orientation.class),
                generateVVWeightField("Size",Size.class),
                generateVVWeightField("Shape",Shape.class),
                generateVVWeightField("Texture",Texture.class))}};
        return titledContainer("Visual Variables Weights",layout);
    }

    private GridPane generateVVWeightField(String title,Class<? extends VisualVariable> vvClass) {
        Slider slider = createWeightSlider();
        slider.setValue(owner.getWeights().getWeight(vvClass));
        TextField field = createNumberField();
        field.textProperty().bind(slider.valueProperty().asString("%.0f"));
        //slider.valueProperty().addListener(getNumberChangeListener(vvClass, slider));
        field.textProperty().addListener(getTextChangeListener(vvClass, slider));
        Consumer<Assessment> resetAction = x ->{
            slider.setValue(x.getWeights().getWeight(vvClass));
        };
        resetActions.add(resetAction);
        return simpleFormField(title,new HBox(slider,field));
    }

    private GridPane generateField(ConnoisseurObject object) {
        Slider slider = createWeightSlider();
        slider.setValue(owner.getWeights().getWeight(object));
        TextField field = createNumberField();
        field.textProperty().bind(slider.valueProperty().asString("%.0f"));
        //slider.valueProperty().addListener(getNumberChangeListener(object, slider));
        field.textProperty().addListener(getTextChangeListener(object, slider));
        HBox weightChooser = new HBox(slider,field);
        FlowPane content = new FlowPane();
        Consumer<Assessment> resetAction = x ->{
            slider.setValue(x.getWeights().getWeight(object));
        };
        resetActions.add(resetAction);
        boolean hasChildren = object instanceof Container && !(object instanceof DynamicContainer);
        if(hasChildren){
            Container<ConnoisseurObject> parent = (Container) object;
            for(ConnoisseurObject child : parent.getList())
                content.getChildren().add(generateField(child));
            Line line = new Line();
            line.getStyleClass().add("content-separator");
            line.endXProperty().bind(weightChooser.widthProperty().negate());
            content.setPadding(new Insets(10));
            content.setVgap(10);
            content.setHgap(15);
            content.setPrefWidth(800);
            return titledContainer(object.toString(),new Node[][]{{weightChooser,line,content}});
        }else{
            return simpleFormField(object.toString(),weightChooser);
        }

    }

    private ChangeListener<String> getTextChangeListener(ConnoisseurObject object, Slider slider) {
        return (x,y,n)->{
            if(!y.equalsIgnoreCase(n)){
                getController().setWeight(owner,object,Integer.parseInt(n));
            }
        };
    }
    private ChangeListener<String> getTextChangeListener(Class<? extends VisualVariable> object, Slider slider) {
        return (x,y,n)->{
            if(!y.equalsIgnoreCase(n)){
                getController().setWeight(owner,object,Integer.parseInt(n));
            }
        };
    }

    private Slider createWeightSlider() {
        Slider estimationSlider = new Slider();
        estimationSlider.setMax(10);
        estimationSlider.setMin(0);
        estimationSlider.setMajorTickUnit(5);
        estimationSlider.setMinorTickCount(4);
        estimationSlider.setShowTickMarks(true);
        estimationSlider.setSnapToTicks(true);
        return estimationSlider;
    }

    private TextField createNumberField() {
        TextField estimationField = new NumberTextField();
        estimationField.setPrefWidth(60);
        estimationField.setEditable(false);
        return estimationField;
    }

    public void setScope(ConnoisseurObject scope){
        this.scopeProperty().setValue(scope);
    }
    public ObjectProperty<ConnoisseurObject> scopeProperty(){
        return scope;
    }

    @Override
    public void reset() {
        resetActions.stream().forEach(x -> x.accept(owner));
    }
}
