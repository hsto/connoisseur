package msc.connoisseur.view.properties.panel.grapheme;

import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.LanguageSystem;
import msc.connoisseur.view.properties.panel.PropertiesPanel;
import msc.connoisseur.view.properties.panel.components.table.LanguageTable;
import msc.connoisseur.view.properties.panel.components.table.Table;

/**
 * Created by jeppe on 14-11-2016.
 */
public class LanguageSystemPanel extends PropertiesPanel {
    private Table languageTable;
    private LanguageSystem languageSystem;
    private Controller controller;

    public LanguageSystemPanel(Controller controller, LanguageSystem languageSystem) {
        super(controller);
        this.languageSystem = languageSystem;
        this.controller = controller;
        initialize();
    }

    private void initialize() {
        languageTable = new LanguageTable(languageSystem,controller);
        languageTable.setOnChangedAction(()->languageTable.apply());
        ScrollPane sc = new ScrollPane(languageTable);
        sc.setPrefWidth(500);
        sc.setPrefHeight(400);
        sc.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        add(titledContainer("Languages",new Node[][]{{sc}}));
    }

    @Override
    public void reset() {
        languageTable.reset();
    }
}
