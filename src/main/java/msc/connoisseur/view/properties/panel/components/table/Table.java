package msc.connoisseur.view.properties.panel.components.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import msc.connoisseur.model.ConnoisseurObject;

public abstract class Table extends GridPane {
	private Button addRowButton = new Button("add");
	private HashMap<TableCell<?>,Node> nodeMapping = new HashMap<>();
	private ArrayList<TableRow> removedRows = new ArrayList<>();
	private ArrayList<TableRow> newRows = new ArrayList<>();
	private ArrayList<TableRow> changedRows = new ArrayList<>();
	private ArrayList<TableRow> rows = new ArrayList<>();
	private ArrayList<TableColumn<?>> columns = new ArrayList<>();
	private Runnable onChangedAction;
	public Table(){

		getStyleClass().add("connoisseur-table");
		addRowButton.setOnAction(x -> addBlankRow());
		//addRowButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/msc/connoisseur/resource/plus.png"))));
		addRowButton.setId("AddButton");
		setFillWidth(addRowButton,true);
		setFillHeight(addRowButton,true);
	}
	private void addBlankRow() {
		TableRow tr = new TableRow(columns,(TableCell<?> x) -> {changed(x);return null;});
		tr.getDeleteButton().setOnAction(x -> {tr.setVisible(false);removedRows.add(tr);onChangedAction();});
		newRows.add(tr);
		draw(tr,rows.size()+newRows.size());
		onNewBlankRow(tr);
	}
	protected void addTextColumn(String name,String regex){
		TextColumn tc = new TextColumn(name,regex);
		addColumn(tc);
	}
	protected <T extends ConnoisseurObject> void addChoiceColumn(String name, Function<Void,List<T>> choices){
		ChoiceColumn<T> tc = new ChoiceColumn<T>(name,choices);
		addColumn(tc);
	}
	private void addColumn(TableColumn<?> tc){

		columns.add(tc);
		addColumnTitle(tc);
	}
	private void addColumnTitle(TableColumn<?> tc) {
		Label heading =new Label(tc.getTitle());
		heading.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		setFillWidth(heading,true);
		setFillHeight(heading,true);
		heading.getStyleClass().add("connoisseur-table-heading");
		add(heading,columns.indexOf(tc),0);
	}
	private void addTitleRow(){
		for(TableColumn<?> col : columns)
			addColumnTitle(col);
	}
	protected TableRow addTableRow(){
		TableRow tr = new TableRow(columns,(TableCell<?> x) -> {changed(x);return null;});
		tr.getDeleteButton().setOnAction(x -> {tr.setVisible(false);removedRows.add(tr);onChangedAction();});
		rows.add(tr);
		return tr;
	}
	protected TableRow addTableRow(TableCell<?>...cells){
		TableRow tr = addTableRow();
		tr.enterAll(cells);
		return tr;
	}
	protected TableCell<?>[] getCellArray(){
		TableCell<?>[] cells = new TableCell<?>[columns.size()];
		for(int i = 0; i < cells.length; i++){
			cells[i] = columns.get(i).createCell(null, (TableCell<?> x) -> {changed(x);return null;});
		}
		return cells;
	}
	private void redraw(){
		this.getChildren().clear();
		addTitleRow();
		for(TableRow row : rows){
			draw(row,rows.indexOf(row)+1);
		}
		add(addRowButton,columns.size(),0);
	}
	private void draw(TableRow row, int index) {
		TableCell<?>[] cells = row.getCells();
		for(int i = 0; i < cells.length; i++)
			draw(cells[i],index,i);
		add(row.getDeleteButton(),cells.length,index);
	}
	private void draw(TableCell<?> c, int row, int col) {
		Node node = c.getNode();
		setFillWidth(node,true);
		setFillHeight(node,true);
		node.getStyleClass().add("connoisseur-table-cell");
		nodeMapping.put(c, node);
		add(node,col,row);
	}
	private void changed(TableCell<?> x) {
		if(!changedRows.contains(x.getRow()) && !newRows.contains(x.getRow()))
			changedRows.add(x.getRow());
		onChangedAction();
	}
	private void onChangedAction() {
		if(onChangedAction!=null)
			this.onChangedAction.run();
	}
	public void reset(){
		newRows.clear();
		changedRows.clear();
		removedRows.clear();
		redraw();
	}
	public void apply(){
		for(int i = 0; i < removedRows.size(); i++){
			TableRow row = removedRows.get(i);
			removeRow(row);
			newRows.remove(row);
			changedRows.remove(row);
			rows.remove(row);
		}
		for(int i = 0; i < changedRows.size(); i++){
			TableRow row = changedRows.get(i);
			updateRow(row);
		}
		for(int i = 0; i < newRows.size(); i++){
			TableRow row = newRows.get(i);
			applyRow(row);
			rows.add(row);
		}
		reset();
	}
	protected abstract void onNewBlankRow(TableRow tr);
	protected abstract void updateRow(TableRow tr);
	protected abstract void applyRow(TableRow tr);
	protected abstract void removeRow(TableRow tr);
	public void setOnChangedAction(Runnable onChangedAction) {
		this.onChangedAction = onChangedAction;
	}

}
