package msc.connoisseur.view.properties;
import java.util.ArrayList;

import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import msc.connoisseur.view.EditorPanel;
import msc.connoisseur.view.properties.panel.PropertiesPanel;

public class PropertiesView extends AnchorPane {
	ArrayList<PropertiesPanel> panels = new ArrayList<PropertiesPanel>();
	VBox panelBox;
    EditorPanel parent;
    Label titleLabel;

    public PropertiesView(EditorPanel parent){
    	this.getStyleClass().add("GPView");
    	this.parent = parent;
    	this.panelBox = new VBox();
        initializeVisualRepresentation();
    }
    private void initializeVisualRepresentation(){
        addTitleBox();
        addPanelBox();
    }
	private void addTitleBox() {
		titleLabel = new Label("");
        titleLabel.setId("TitleLabel");
        setTopAnchor(titleLabel,0.0);
        setLeftAnchor(titleLabel,0.0);
        getChildren().add(titleLabel);
	}
	private void addPanelBox() {

		ScrollPane sp = new ScrollPane();
		sp.setContent(panelBox);
        sp.setFitToWidth(true);

    	getChildren().add(sp);
    	setTopAnchor(sp,50.0);
    	setLeftAnchor(sp,10.0);
    	setRightAnchor(sp,10.0);
    	setBottomAnchor(sp,50.0);
	}
	public void clear(){
    	for(PropertiesPanel panel: panels)
    		panelBox.getChildren().remove(panel);
    	panels.clear();
    }
    public void setTitle(String str){
    	titleLabel.setText(str);
    }
    public void reset(){
    	for(PropertiesPanel panel: panels)
    		panel.reset();
    }
    public void addPropertiesPanel(PropertiesPanel pp){
    	panelBox.getChildren().add(pp);
    	panels.add(pp);
    	pp.reset();
    }

}
