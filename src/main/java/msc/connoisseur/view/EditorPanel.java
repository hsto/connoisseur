package msc.connoisseur.view;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableObjectValue;
import javafx.scene.layout.BorderPane;
import msc.connoisseur.control.Controller;
import msc.connoisseur.control.CopyPasteController;
import msc.connoisseur.control.UndoRedoController;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.ConnoisseurProject;
import msc.connoisseur.model.EstimatableObject;
import msc.connoisseur.model.LanguageSystem;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.assessment.Estimatable;
import msc.connoisseur.model.dialect.Dialect;
import msc.connoisseur.model.intent.modelnature.ModelNature;
import msc.connoisseur.model.intent.task.Task;
import msc.connoisseur.model.intent.tooldemands.ToolDemands;
import msc.connoisseur.model.intent.user.User;
import msc.connoisseur.model.language.Language;
import msc.connoisseur.model.language.pragmatics.Conventions;
import msc.connoisseur.model.language.pragmatics.SupportedUsages;
import msc.connoisseur.model.language.semantics.DomainMapping;
import msc.connoisseur.model.language.semantics.Elements;
import msc.connoisseur.model.language.semantics.GraphemeMapping;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.language.syntax.CompoundGrapheme;
import msc.connoisseur.model.language.syntax.Diagrams;
import msc.connoisseur.model.language.syntax.Rules;
import msc.connoisseur.view.assessment.AssessorView;
import msc.connoisseur.view.hierarchy.HierarchyView;
import msc.connoisseur.view.properties.PropertiesView;
import msc.connoisseur.view.properties.panel.*;
import msc.connoisseur.view.properties.panel.common.DescriptionPanel;
import msc.connoisseur.view.properties.panel.common.EstimationPanel;
import msc.connoisseur.view.properties.panel.common.NamePanel;
import msc.connoisseur.view.properties.panel.grapheme.CompoundGraphemePanel;
import msc.connoisseur.view.properties.panel.grapheme.LanguageSystemPanel;
import msc.connoisseur.view.properties.panel.grapheme.VisualPropertiesPanel;
import msc.connoisseur.view.properties.panel.grapheme.shape.ShapePanel;
import msc.connoisseur.view.utility.DragDropController;
import msc.connoisseur.view.utility.HorizontalResizer;

import java.util.ArrayList;
import java.util.List;

public class EditorPanel extends BorderPane {
	public EditorTopBar etb;
	HierarchyView hv;
	PropertiesView pv;
	AssessorView av;

	private ObservableProjectWrapper project;
	private ObservableConnoiseurWrapper currentObject;
	private Controller controller;
	private UndoRedoController undoRedoController;
	private DragDropController dragDropController;
	private CopyPasteController copyPasteController;
	//private Preferences preferences;

	public EditorPanel(Controller controller, UndoRedoController undoRedoController, DragDropController dragDropController, CopyPasteController copyPasteController){
		this.controller = controller;
		this.undoRedoController = undoRedoController;
		this.dragDropController = dragDropController;
		this.copyPasteController = copyPasteController;
		initializeObservables();
		loadPanels();
		setStyle("-fx-background: #EEEEEE;");
	}

	private void initializeObservables() {
		project = new ObservableProjectWrapper();
		currentObject = new ObservableConnoiseurWrapper();
	}

	private void loadPanels() {
		etb = new EditorTopBar(controller, undoRedoController,this);
		pv = new PropertiesView(this);
		hv = new HierarchyView(dragDropController, copyPasteController, controller, this);
		av = new AssessorView(this);

		HorizontalResizer.makeResizable(pv,hv);
		setTop(etb);
		setLeft(hv);
		setCenter(pv);
		setRight(av);
	}
	public void reset() {
		pv.reset();
		hv.reset();
	}
	public void updateHierarchyLabel(ConnoisseurObject co){
		hv.updateLabel(co);
	}
	private Language getLanguageOf(ConnoisseurObject co){
		while(!(co instanceof Language) && co != null)
			co = co.getParent();
		return (Language) co;
	}

	public void select(ConnoisseurObject co) {
		pv.clear();
		pv.addPropertiesPanel(new NamePanel(controller,co));

		if (co instanceof AtomicGrapheme) {
			VisualPropertiesPanel vvp = new VisualPropertiesPanel(controller,(AtomicGrapheme) co);
			ShapePanel isp = new ShapePanel(controller,(AtomicGrapheme) co);
			pv.addPropertiesPanel(vvp);
			pv.addPropertiesPanel(isp);
		} else if (co instanceof CompoundGrapheme){
			CompoundGraphemePanel cgp = new CompoundGraphemePanel(controller,(CompoundGrapheme) co);
			pv.addPropertiesPanel(cgp);
		} else if (co instanceof LanguageSystem) {
			LanguageSystemPanel lsp = new LanguageSystemPanel(controller,(LanguageSystem)co);
			pv.addPropertiesPanel(lsp);
		} else if (co instanceof Elements) {
			ConceptsPanel cp = new ConceptsPanel(controller,(Elements) co);
			DomainsPanel dp = new DomainsPanel(controller,(Elements) co);
			pv.addPropertiesPanel(cp);
			pv.addPropertiesPanel(dp);
		} else if (co instanceof GraphemeMapping) {
			GraphemeMappingPanel mp = new GraphemeMappingPanel(controller,getLanguageOf(co));
			pv.addPropertiesPanel(mp);
		} else if (co instanceof DomainMapping) {
			DomainMappingPanel dmp = new DomainMappingPanel(controller,getLanguageOf(co));
			pv.addPropertiesPanel(dmp);
		}else if(co instanceof Conventions){
			ConventionsPanel cp = new ConventionsPanel(controller,(Conventions)co);
			pv.addPropertiesPanel(cp);
		}else if(co instanceof SupportedUsages){
			SupportedUsagesPanel sup = new SupportedUsagesPanel(controller,(SupportedUsages)co);
			pv.addPropertiesPanel(sup);
		}else if(co instanceof Diagrams){
			DiagramsPanel dp = new DiagramsPanel(controller,(Diagrams)co);
			pv.addPropertiesPanel(dp);
		}else if(co instanceof Rules){
			RulesPanel rp = new RulesPanel(controller,(Rules)co);
			pv.addPropertiesPanel(rp);
		}else if(co instanceof ModelNature){
			ModelNaturePanel mnp = new ModelNaturePanel(controller,(ModelNature)co);
			pv.addPropertiesPanel(mnp);
		}else if(co instanceof User){
			UserPanel up = new UserPanel(controller,(User)co);
			pv.addPropertiesPanel(up);
		}else if(co instanceof ToolDemands){
			ToolDemandsPanel tdp = new ToolDemandsPanel(controller,(ToolDemands)co);
			pv.addPropertiesPanel(tdp);
		}else if(co instanceof Task){
			TaskPanel tp = new TaskPanel(controller,(Task)co);
			pv.addPropertiesPanel(tp);
		}else if(co instanceof Dialect){
			DialectPanel dp = new DialectPanel(controller,(Dialect)co);
			pv.addPropertiesPanel(dp);
		}else if(co instanceof Assessment){
			AssessmentPanel ap = new AssessmentPanel(controller,(Assessment)co);
			pv.addPropertiesPanel(ap);
		}
		if(co instanceof EstimatableObject){
			EstimationPanel ep = new EstimationPanel(controller,(Estimatable) co);
			pv.addPropertiesPanel(ep);
		}

 		pv.addPropertiesPanel(new DescriptionPanel(controller,co));
		pv.setTitle(co.toString());
		currentObject.set(co);
	}



	public ConnoisseurObject getCurrentElement() {
		return this.currentObject.get();
	}
	public void resetPropertiesView() {
		pv.reset();
	}
	public void setProject(ConnoisseurProject connoisseurProject) {
		hv.clear();
		this.project.set(connoisseurProject);
		hv.setProject(connoisseurProject);
	}

	public ConnoisseurProject getProject() {
		return this.project.get();
	}

	public ObservableConnoiseurWrapper getElementWrapper() {
		return this.currentObject;
	}

	private class ObservableProjectWrapper implements ObservableObjectValue<ConnoisseurProject> {
		List<ChangeListener<? super ConnoisseurProject>> changeListeners = new ArrayList<>();
		List<InvalidationListener> invalidationListeners = new ArrayList<>();
		ConnoisseurProject project;
		public void notifyChangeListeners(){
			for(ChangeListener changeListener: changeListeners)
				changeListener.changed(this,project,project);
		}
		@Override
		public ConnoisseurProject get() {
			return project;
		}
		public void set(ConnoisseurProject model){
			this.project = model;
		}
		@Override
		public void addListener(ChangeListener<? super ConnoisseurProject> listener) {
			changeListeners.add(listener);
		}

		@Override
		public void removeListener(ChangeListener<? super ConnoisseurProject> listener) {
			changeListeners.remove(listener);
		}

		@Override
		public ConnoisseurProject getValue() {
			return project;
		}

		@Override
		public void addListener(InvalidationListener listener) {
			invalidationListeners.add(listener);
		}

		@Override
		public void removeListener(InvalidationListener listener) {
			invalidationListeners.remove(listener);
		}
	}

	private class ObservableConnoiseurWrapper implements ObservableObjectValue<ConnoisseurObject> {
		List<ChangeListener<? super ConnoisseurObject>> changeListeners = new ArrayList<>();
		List<InvalidationListener> invalidationListeners = new ArrayList<>();
		ConnoisseurObject model;
		public void set(ConnoisseurObject model){
			for(ChangeListener<? super ConnoisseurObject> listener : changeListeners){
				listener.changed(this,this.model,model);
			}
			this.model = model;
		}
		@Override
		public ConnoisseurObject get() {
			return this.model;
		}

		@Override
		public void addListener(ChangeListener<? super ConnoisseurObject> listener) {
			changeListeners.add(listener);
		}

		@Override
		public void removeListener(ChangeListener<? super ConnoisseurObject> listener) {
			changeListeners.remove(listener);
		}

		@Override
		public ConnoisseurObject getValue() {
			return this.model;
		}

		@Override
		public void addListener(InvalidationListener listener) {
			invalidationListeners.add(listener);
		}

		@Override
		public void removeListener(InvalidationListener listener) {
			invalidationListeners.remove(listener);
		}
	}
}
