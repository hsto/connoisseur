package msc.connoisseur.view.hierarchy;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.bertin.IconShape;
import msc.connoisseur.model.bertin.LabelShape;
import msc.connoisseur.model.bertin.LineShape;
import msc.connoisseur.model.bertin.RegionShape;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.language.syntax.CompoundGrapheme;
import msc.connoisseur.model.language.syntax.Grapheme;

public class HierarchyElement extends HBox {
	private boolean collapsible = false;
	private boolean collapsed = false;
	private boolean hidden = false;
	private Button elementButton = new Button();
	private Button expandButton = new Button();
	private static Image expandedImage;
	private static Image collapsedImage;
	private static Image iconImage;
	private static Image regionImage;
	private static Image labelImage;
	private static Image lineImage;
	private ImageView expandedGraphic = new ImageView(expandedImage);
	private ImageView collapsedGraphic = new ImageView(collapsedImage);
	private ImageView icon = new ImageView();
	private ConnoisseurObject model;
	static {
		expandedImage = new Image(HierarchyElement.class.getResourceAsStream("/img/minus.png"));
		collapsedImage = new Image(HierarchyElement.class.getResourceAsStream("/img/plus.png"));
		iconImage = new Image(HierarchyElement.class.getResourceAsStream("/img/icon.png"));
		regionImage = new Image(HierarchyElement.class.getResourceAsStream("/img/region.png"));
		labelImage = new Image(HierarchyElement.class.getResourceAsStream("/img/label.png"));
		lineImage = new Image(HierarchyElement.class.getResourceAsStream("/img/line.png"));
	}
	protected ObservableList<HierarchyElement> subelements = FXCollections.observableArrayList();
	private int depth;
	public HierarchyElement(ConnoisseurObject model){
		this.model = model;
		elementButton.setPadding(Insets.EMPTY);
		expandButton.setPadding(Insets.EMPTY);
		//ensures area is used by other nodes when setVisible(false) is invoked
		expandButton.managedProperty().bind(visibleProperty());

		managedProperty().bind(visibleProperty());
		setCollapsible(false);
		setStyleClasses();
		getChildren().add(expandButton);
		getChildren().add(elementButton);
		updateLabel();
		updateIcon();
		initializeIcons();
	}
	public void updateLabel(){
		elementButton.setText(model.toString());
		updateIcon();
	}
	private void initializeIcons() {
		expandedGraphic.setPreserveRatio(true);
		expandedGraphic.setFitHeight(10);
		expandedGraphic.setFitWidth(10);
		collapsedGraphic.setPreserveRatio(true);
		collapsedGraphic.setFitHeight(10);
		collapsedGraphic.setFitWidth(10);
		icon.getStyleClass().add("hIcon");
		expandButton.setGraphic(expandedGraphic);
		expandButton.setOnAction(expandButtonAction());
		elementButton.setGraphic(icon);
	}
	private EventHandler<ActionEvent> expandButtonAction() {
		HierarchyElement node = this;
		return new EventHandler<ActionEvent>(){
			HierarchyElement he = node;
			@Override
			public void handle(ActionEvent event) {
				if(he.isCollapsed())
					he.expand();
				else
					he.collapse();
			}
		};
	}
	public void addSubElement(HierarchyElement he){
		subelements.add(he);
		if(this.isCollapsed())
			he.hide();
	}
	public void setCollapsible(boolean b){
		collapsible = b;
		expandButton.setVisible(collapsible);
	}
	public boolean isCollapsed(){
		return collapsed;
	}
	private void hide(){
		this.setVisible(false);
		subelements.forEach(x -> x.hide());
	}
	public void collapse(){
		if(!collapsible)return;
		collapsed = true;
		subelements.forEach(x -> x.hide());
		expandButton.setGraphic(collapsedGraphic);
	}
	public void expand(){
		if(!collapsible)return;
		collapsed = false;
		subelements.forEach(x -> x.show());
		expandButton.setGraphic(expandedGraphic);
	}
	private void show(){
		this.setVisible(true);
		if(!collapsed)
			subelements.forEach(x -> x.show());
	}
	private void setStyleClasses(){
		this.getStyleClass().add("hButtonArea");
		elementButton.getStyleClass().add("hButton");
		expandButton.getStyleClass().add("hButton");
	}
	public ObservableList<HierarchyElement> getSubElements() {
		return subelements;
	}
	public void setOnAction(EventHandler<ActionEvent> event) {
		elementButton.setOnAction(event);
	}
	public void setContextMenu(ContextMenu cm) {
		elementButton.setContextMenu(cm);
	}
	public Button getElementButton(){
		return elementButton;
	}
	public void updateIcon(){
		if(model instanceof AtomicGrapheme){
			Shape s = ((Grapheme) model).getVisualVector().getShape();

			if(s instanceof RegionShape){
				setIcon(regionImage);
			}else if(s instanceof LabelShape){
				setIcon(labelImage);
			}else if(s instanceof LineShape){
				setIcon(lineImage);
			}else if(s instanceof IconShape){
				setIcon(iconImage);
			}else{
				setIcon(null);
			}
		}else if(model instanceof CompoundGrapheme){
			setIcon(null);
		}else{
			setIcon(null);
		}
	}
	private void setIcon(Image img){
		if(img == null){
			elementButton.setGraphicTextGap(0);
			icon.setImage(null);
			icon.setFitHeight(0);
			icon.setFitWidth(0);
		}else{
			elementButton.setGraphicTextGap(5);
			icon.setImage(img);
			icon.setFitHeight(10);
			icon.setFitWidth(10);
		}
	}
	public void mimicState(HierarchyElement other){
		if(other.collapsed) collapse();
		else expand();
		setVisible(other.isVisible());
	}
	public ConnoisseurObject getModel() {
		return model;
	}
	public void setDepth(int depth) {
		this.depth = depth;
		this.setPadding(new Insets(0,0,0,15*depth));
	}
	public int getDepth(){
		return this.depth;
	}


}
