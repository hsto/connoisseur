package msc.connoisseur.view.hierarchy;

import msc.connoisseur.control.Controller;
import msc.connoisseur.control.CopyPasteController;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.common.MappingContainer;
import msc.connoisseur.model.dialect.Dialect;
import msc.connoisseur.model.language.semantics.Concept;
import msc.connoisseur.model.language.semantics.Domain;
import msc.connoisseur.model.language.semantics.Elements;
import msc.connoisseur.view.utility.DragDropController;

import java.util.List;

public class ElementFactory {
    private DragDropController ddcon;
    HierarchyView owner;
    CMFactory cMFactory;
    public ElementFactory(Controller controller, DragDropController dragDropController, CopyPasteController copyPasteController, HierarchyView owner){
        this.owner = owner;
        ddcon = dragDropController;
        this.cMFactory = new CMFactory(copyPasteController, controller ,owner);
    }
    public <T extends ConnoisseurObject> HierarchyElement createHierarchyElement(T sc){
        HierarchyElement element = new HierarchyElement(sc);
        element.setOnAction(x -> {owner.highlight(element);owner.getEditor().select(sc);});
        element.setContextMenu(cMFactory.makeContextMenu(sc));
        if(isContainer(sc)) {
            element.setCollapsible(true);
            for (ConnoisseurObject child : getChildren((Container<? extends ConnoisseurObject>) sc)) {
                if(shouldShowInHierarchy(child))
                    element.addSubElement(createHierarchyElement(child));
            }
            element.collapse();
        }
        applyDragDropBehaviour(element);
        return element;
    }

    private void applyDragDropBehaviour(HierarchyElement element) {
        ddcon.register(element);
    }

    private <T extends ConnoisseurObject> List<T> getChildren(Container<T> sc) {
        return sc.getList();
    }

    private boolean isContainer(ConnoisseurObject sc) {
        //NOTE! Not ideal!
        return sc instanceof Container &&
                !(sc instanceof MappingContainer) &&
                !(sc instanceof Elements) &&
                !(sc instanceof Dialect);
    }

    private boolean shouldShowInHierarchy(ConnoisseurObject c){
        if(c instanceof Concept) return false;
        else if(c instanceof Domain) return false;
        else return true;

    }
}
