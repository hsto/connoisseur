package msc.connoisseur.view.hierarchy;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import msc.connoisseur.control.Controller;
import msc.connoisseur.control.CopyPasteController;
import msc.connoisseur.model.*;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.DynamicContainer;
import msc.connoisseur.model.dialect.Dialect;
import msc.connoisseur.model.intent.Intent;
import msc.connoisseur.model.language.Language;
import msc.connoisseur.model.language.semantics.Elements;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.language.syntax.CompoundGrapheme;
import msc.connoisseur.model.language.syntax.Grapheme;

public class CMFactory {
	private final CopyPasteController copyPasteController;
	private final HierarchyView owner;
	private final Controller controller;

	public CMFactory(CopyPasteController copyPasteController, Controller controller, HierarchyView owner){
		this.controller = controller;
		this.copyPasteController = copyPasteController;
		this.owner = owner;
	}
	public ContextMenu makeContextMenu(ConnoisseurObject object){
		ContextMenu cm = new ContextMenu();
		MenuItem copy = new MenuItem("Copy");
		copy.setOnAction(x -> {copyPasteController.copy(object);});
		MenuItem paste = new MenuItem("Paste");
		paste.disableProperty().bind(copyPasteController.hasCopied.not());
		paste.setOnAction(x -> copyPasteController.paste(object));
		if(object instanceof DynamicContainer && !(object instanceof Elements)){
			MenuItem[] elementOperations = makeContainerItems(object);
			cm.getItems().addAll(elementOperations);
		}else if(object instanceof AtomicGrapheme){
			Menu add = new Menu("Add");
			MenuItem grapheme = new MenuItem("Grapheme");
			grapheme.setOnAction(x -> controller.add((AtomicGrapheme)object,new AtomicGrapheme()));
			MenuItem compound = new MenuItem("Compound Grapheme");
			compound.setOnAction(x -> controller.add((AtomicGrapheme)object,new CompoundGrapheme()));
			add.getItems().addAll(grapheme,compound);
			cm.getItems().addAll(add,new SeparatorMenuItem());
		}
		if(object.getParent() instanceof DynamicContainer){

			MenuItem delete = new MenuItem("Delete");
			delete.setOnAction(x -> controller.remove((DynamicContainer)object.getParent(),object));
			cm.getItems().addAll(delete,new SeparatorMenuItem());
		}
		cm.getItems().addAll(copy,paste);
		return cm;
	}

	private MenuItem[] makeContainerItems(ConnoisseurObject object) {
		DynamicContainer container = (DynamicContainer) object;
		Menu addMenu = new Menu("New");
		if(container.getContainedClass() == Grapheme.class) {
			MenuItem grapheme = new MenuItem("Grapheme");
			grapheme.setOnAction(x -> {
				controller.add(container, new AtomicGrapheme());
				expandUponAdding(object);
			});
			MenuItem compound = new MenuItem("Compound Grapheme");
			compound.setOnAction(x -> {
				controller.add(container, new CompoundGrapheme());
				expandUponAdding(object);
			});
			addMenu.getItems().addAll(grapheme, compound);
		}else if(container.getContainedClass() == ConnoisseurObject.class){
			MenuItem languageSystem = new MenuItem("Language System");
			languageSystem.setOnAction(x -> {
				controller.add(container, new LanguageSystem());
				expandUponAdding(object);
			});
			MenuItem language = new MenuItem("Language");
			language.setOnAction(x -> {
				controller.add(container, new Language());
				expandUponAdding(object);
			});
			MenuItem dialect = new MenuItem("Dialect");
			dialect.setOnAction(x -> {
				controller.add(container, new Dialect());
				expandUponAdding(object);
			});
			MenuItem intent = new MenuItem("Intent");
			intent.setOnAction(x -> {
				controller.add(container, new Intent());
				expandUponAdding(object);
			});
			MenuItem assessment = new MenuItem("Assessment");
			assessment.setOnAction(x -> {
				controller.add(container, new Assessment());
				expandUponAdding(object);
			});
			addMenu.getItems().addAll(languageSystem,language,dialect,intent,new SeparatorMenuItem(),assessment);
		}
		return new MenuItem[]{addMenu,new SeparatorMenuItem()};
	}
	private void expandUponAdding(ConnoisseurObject object){
		owner.expandElementOf(object);
	}



}
