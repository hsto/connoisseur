package msc.connoisseur.view.hierarchy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.VBox;
import msc.connoisseur.control.Controller;
import msc.connoisseur.control.CopyPasteController;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.ConnoisseurProject;
import msc.connoisseur.view.EditorPanel;
import msc.connoisseur.view.utility.DragDropController;

public class HierarchyView extends VBox {
	private EditorPanel parent;
	private ScrollPane scrollContainer;
	private VBox elementContainer;
	private ConnoisseurProject project;
	private HashMap<ConnoisseurObject,HierarchyElement> map = new HashMap<ConnoisseurObject,HierarchyElement>();
	private ElementFactory eFactory;
	public HierarchyView(DragDropController dragDropController,
						 CopyPasteController copyPasteController,
						 Controller controller,
						 EditorPanel parent){
		setPrefWidth(200);
		this.parent = parent;
		this.eFactory = new ElementFactory(controller, dragDropController, copyPasteController, this);
		initialize();
		//select(gsHeading);
		setId("HierarchyView");
	}
	private void initialize() {
		getChildren().clear();
		elementContainer = new VBox();
		scrollContainer = new ScrollPane();
		scrollContainer.setHbarPolicy(ScrollBarPolicy.NEVER);
		scrollContainer.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollContainer.setContent(elementContainer);
		getChildren().add(scrollContainer);
	}
	public void updateLabels(){
		for(Node n : elementContainer.getChildren())
			if(n instanceof HierarchyElement)
				((HierarchyElement) n).updateLabel();
	}
	private int addElement(int depth,HierarchyElement root) {
		int index = elementContainer.getChildren().size();
		elementContainer.getChildren().add(root);
		root.setDepth(depth);
		map.put(root.getModel(), root);
		for(HierarchyElement he: root.getSubElements())
			index = addElement(depth+1,he);
		return index;
	}
	private int addElement(int index,int depth,HierarchyElement root) {
		elementContainer.getChildren().add(index,root);
		root.setDepth(depth);
		map.put(root.getModel(), root);
		for(HierarchyElement he: root.getSubElements())
			index = addElement(++index,depth+1,he);
		return index;
	}


	void highlight(HierarchyElement element) {
		Node old = this.lookup("#currentHButton");
		if(old != null) old.setId("");
		element.setId("currentHButton");
		element.requestFocus();
	}

	public void reset() {
		ArrayList<HierarchyElement> oldElements = map.values()
				.stream().collect(Collectors.toCollection(ArrayList::new));
		HierarchyElement che = (HierarchyElement)this.lookup("#currentHButton");
		elementContainer.getChildren().clear();
		setProject(project);
		for(HierarchyElement element : oldElements){
			HierarchyElement object = map.get(element.getModel());
			object.mimicState(element);
		}
		if(che == null)return;
		ConnoisseurObject current = che.getModel();
		highlight(map.get(current));
	}
	public void setProject(ConnoisseurProject project){
		this.project = project;
		add(project);
	}
	public ConnoisseurProject getProject(){
		return this.project;
	}
	public void add(ConnoisseurObject sc){
		HierarchyElement container = map.get(sc.getParent());
		HierarchyElement element = eFactory.createHierarchyElement(sc);
		if(container == null){
			addElement(0,element);
		}else {
			container.addSubElement(element);
			addElement(elementContainer.getChildren().indexOf(container) + 1, container.getDepth() + 1, element);
		}
	}

	void remove(ConnoisseurObject item) {
		HierarchyElement element = map.get(item);
		remove(element);
	}
	void remove(HierarchyElement element){
		elementContainer.getChildren().remove(element);
		for(HierarchyElement child : element.getSubElements())
			remove(child);
	}
	public void updateLabel(ConnoisseurObject co) {
		HierarchyElement he =map.get(co);
		if(he != null)
			he.updateLabel();
	}
	EditorPanel getEditor(){
		return this.parent;
	}
	public void clear() {
		elementContainer.getChildren().clear();
		map.clear();
	}

	public void expandElementOf(ConnoisseurObject object) {
		map.get(object).expand();
	}
}