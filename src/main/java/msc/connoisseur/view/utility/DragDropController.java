package msc.connoisseur.view.utility;

import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import msc.connoisseur.control.Controller;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.common.DynamicContainer;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.language.syntax.Grapheme;
import msc.connoisseur.view.hierarchy.HierarchyElement;

public class DragDropController {
    private static final DataFormat connoisseurDataFormat = new DataFormat();
    private Controller controller;

    public DragDropController(Controller controller){

        this.controller = controller;
    }
    public void register(HierarchyElement element) {
        ConnoisseurObject model = element.getModel();
        registerDestination(element);
        if(model instanceof Grapheme)
            registerSubject(element,TransferMode.MOVE);
        else
            registerSubject(element,TransferMode.COPY);
    }
    public void registerSubject(HierarchyElement element,TransferMode tm){
        element.getElementButton().setOnDragDetected(x -> dragDetected(element,tm,x));
    }
    public void registerDestination(HierarchyElement element){
        element.getElementButton().setOnDragOver(x -> dragOver(element,x));
        element.getElementButton().setOnDragEntered(x -> dragEntered(element,x));
        element.getElementButton().setOnDragDropped(x -> dragDropped(element,x));
    }
    private void dragDetected(HierarchyElement element, TransferMode tm, MouseEvent x) {
        Dragboard db = element.startDragAndDrop(tm);
        ClipboardContent content = new ClipboardContent();
        content.put(connoisseurDataFormat, element.getModel());
        db.setContent(content);
        x.consume();
    }
    private void dragDropped(HierarchyElement element, DragEvent e) {
        HierarchyElement src = sourceElement(e);
        HierarchyElement tgt = element;
    	if(validDragDropOperation(src,tgt)){
    		if(validGraphemeTransfer(src,tgt)){
                controller.move((DynamicContainer<Grapheme>) tgt.getModel(),(Grapheme)src.getModel());
            }else if(validGraphemeStacking(src,tgt)){
                controller.add((AtomicGrapheme) tgt.getModel(),(Grapheme)src.getModel());
            }else if(similarSingleton(src,tgt)){
                controller.cloneInto(tgt.getModel(),src.getModel());
            }else if(validParent(src,tgt)){
                Container<ConnoisseurObject> prnt = (Container<ConnoisseurObject>) tgt.getModel();
                for(ConnoisseurObject c : prnt.getList()) if(c.getClass() == src.getModel().getClass()){
                    c.loadInto(src.getModel());
                    break;
                }
            }
    		e.setDropCompleted(true);
    	}else{
    		e.setDropCompleted(false);
    	}
    	e.consume();
    }

    private void dragEntered(HierarchyElement element, DragEvent e) {
    	 if(validDragDropOperation(sourceElement(e),element)){

         }
         e.consume();
    }
    private void dragOver(HierarchyElement element, DragEvent e) {
        HierarchyElement src = sourceElement(e);
        HierarchyElement tgt = element;
        if(validGraphemeTransfer(src,tgt) || validGraphemeStacking(src,tgt)){
            e.acceptTransferModes(TransferMode.MOVE);
        }else if(similarSingleton(src,tgt) || validParent(src,tgt)){
            e.acceptTransferModes(TransferMode.COPY);
        }
        e.consume();
    }

    private HierarchyElement sourceElement(DragEvent e){
        return (HierarchyElement) e.getGestureSource();
    }
    private boolean validDragDropOperation(HierarchyElement src, HierarchyElement tgt){
        if(src.getModel() instanceof Grapheme){
            return validGraphemeTransfer(src,tgt) || validGraphemeStacking(src,tgt);
        }else{
            return validParent(src,tgt) || similarSingleton(src,tgt);
        }
    }
    private boolean validParent(HierarchyElement source,HierarchyElement target){
        return target.getModel().getClass() == source.getModel().getParent().getClass();
    }
    private boolean similarSingleton(HierarchyElement source,HierarchyElement target){
        return source != target && source.getModel().getClass() == target.getModel().getClass();
    }
    private boolean validGraphemeTransfer(HierarchyElement source, HierarchyElement target){
        return source.getModel() instanceof Grapheme &&
                target.getModel() instanceof DynamicContainer &&
                ((DynamicContainer) target.getModel()).getContainedClass() == Grapheme.class;
    }
    private boolean validGraphemeStacking(HierarchyElement source, HierarchyElement target){
        return source.getModel() instanceof Grapheme &&
                target.getModel() instanceof AtomicGrapheme;
    }
}
