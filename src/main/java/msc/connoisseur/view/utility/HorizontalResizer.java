package msc.connoisseur.view.utility;

import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;

public class HorizontalResizer {
	private final double MARGIN = 4;
	private Region hitarea;
	private Region resizee;
	private double x;
	private boolean dragging = false;
	private HorizontalResizer(Region hitarea,Region resizee){
		this.hitarea = hitarea;
		this.resizee = resizee;
	}
	public static void makeResizable(Region hitarea,Region resizee){
		HorizontalResizer hr = new HorizontalResizer(hitarea,resizee);
		hitarea.setOnMouseMoved(x -> hr.hover(x));
		hitarea.setOnMousePressed(x -> hr.click(x));
		hitarea.setOnMouseReleased(x -> hr.release(x));
		hitarea.setOnMouseDragged(x -> hr.drag(x));
	}
	private void drag(MouseEvent event) {
		if(dragging){
			double newX = event.getSceneX();
			if(newX>500) newX = 500;
			double newWidth = resizee.getMinWidth()+newX-x;
			resizee.setMinWidth(newWidth);
			x = newX;
		}
	}
	private void release(MouseEvent event) {
		dragging = false;
		hitarea.setCursor(Cursor.DEFAULT);
	}
	private void click(MouseEvent event) {
		if(withinMargin(event)){
			dragging = true;
			x = event.getSceneX();
			resizee.setMinWidth(resizee.getWidth());
		}
	}
	private void hover(MouseEvent event) {
		if(withinMargin(event) || dragging) {
			hitarea.setCursor(Cursor.H_RESIZE);
		} else {
			hitarea.setCursor(Cursor.DEFAULT);
		}
	}
	private boolean withinMargin(MouseEvent event) {
		return event.getX()<MARGIN;
	}
}
