package msc.connoisseur.view;

import javafx.beans.value.ObservableObjectValue;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import msc.connoisseur.control.Controller;
import msc.connoisseur.control.EditorAction;
import msc.connoisseur.control.UndoRedoController;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.LanguageSystem;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.common.DynamicContainer;
import msc.connoisseur.model.dialect.Dialect;
import msc.connoisseur.model.intent.Intent;
import msc.connoisseur.model.language.Language;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import static javafx.scene.input.KeyCode.*;

public class EditorTopBar extends MenuBar {
    private Controller controller;
    private UndoRedoController undoRedoController;
    EditorPanel parent;

    public EditorTopBar(Controller controller, UndoRedoController undoRedoController, EditorPanel parent) {
        this.controller = controller;
        this.undoRedoController = undoRedoController;
        this.parent = parent;
        initialize();
        setId("TopBar");
    }

    private void initialize() {
        Menu file = new Menu("Project"); //--------------------

        MenuItem project = new MenuItem("New");//---------------
        project.setOnAction(x -> controller.newProject(true));

        MenuItem open = new MenuItem("Open");
        open.setGraphic(new ImageView(new Image(this.getClass().getResourceAsStream("/img/glyphicons-416-disk-open.png"))));
        open.setOnAction(x -> controller.open());
        open.setAccelerator(new KeyCodeCombination(O, KeyCombination.CONTROL_DOWN));

        MenuItem merge = new MenuItem("Merge");
        merge.setOnAction(x -> controller.merge());

        MenuItem save = new MenuItem("Save");
        save.setAccelerator(new KeyCodeCombination(S, KeyCombination.CONTROL_DOWN));
        save.setGraphic(new ImageView(new Image(this.getClass().getResourceAsStream("/img/glyphicons-415-disk-save.png"))));
        save.setOnAction(x -> controller.save(parent.getProject()));
        save.disableProperty().bind(controller.getDirtyProperty().not());

        MenuItem saveAs = new MenuItem("Save As");
        saveAs.setOnAction(x -> controller.saveAs(parent.getProject()));
        saveAs.setAccelerator(new KeyCodeCombination(S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));

        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(x -> controller.exit());

        file.getItems().addAll(project, new SeparatorMenuItem(), open, merge, save, saveAs, new SeparatorMenuItem(), exit);


        Menu add = new Menu("Add"); //----------------------------------------------------

        MenuItem languageSystem = new MenuItem("Language System");
        languageSystem.setOnAction(x -> controller.add(parent.getProject(), new LanguageSystem()));
        languageSystem.setAccelerator(new KeyCodeCombination(S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));

        MenuItem language = new MenuItem("Language");
        language.setAccelerator(new KeyCodeCombination(L, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        language.setOnAction(x -> controller.add(parent.getProject(), new Language()));

        MenuItem dialect = new MenuItem("Dialect");
        dialect.setOnAction(x -> controller.add(parent.getProject(), new Dialect()));
        dialect.setAccelerator(new KeyCodeCombination(D, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));

        MenuItem intent = new MenuItem("Intent");
        intent.setOnAction(x -> controller.add(parent.getProject(), new Intent()));
        intent.setAccelerator(new KeyCodeCombination(I, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));

        add.getItems().addAll(languageSystem, language, dialect, intent);


        Menu edit = new Menu("Edit");//------------------------------------------

        MenuItem undo = new MenuItem("Undo");
        undo.setAccelerator(new KeyCodeCombination(Z, KeyCombination.CONTROL_DOWN));
        undo.setGraphic(new ImageView(new Image(this.getClass().getResourceAsStream("/img/glyphicons-436-undo.png"))));
        undo.setOnAction(x -> undoRedoController.undo(controller));

        MenuItem redo = new MenuItem("Redo");
        redo.setAccelerator(new KeyCodeCombination(Y, KeyCombination.CONTROL_DOWN));
        redo.setGraphic(new ImageView(new Image(this.getClass().getResourceAsStream("/img/glyphicons-435-redo.png"))));
        redo.setOnAction(x -> undoRedoController.redo(controller));

        MenuItem moveUp = new MenuItem("Move Up");
        moveUp.setOnAction(x -> controller.moveUp((DynamicContainer) parent.getCurrentElement().getParent(), parent.getCurrentElement()));
        moveUp.setAccelerator(new KeyCodeCombination(UP, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        ((ObservableObjectValue<ConnoisseurObject>) parent.getElementWrapper()).addListener((x, y, z) -> moveUp.setDisable(!(z.getParent() instanceof DynamicContainer)));

        MenuItem moveDown = new MenuItem("Move Down");
        moveDown.setAccelerator(new KeyCodeCombination(DOWN, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        moveDown.setOnAction(x -> controller.moveDown((DynamicContainer) parent.getCurrentElement().getParent(), parent.getCurrentElement()));
        ((ObservableObjectValue<ConnoisseurObject>) parent.getElementWrapper()).addListener((x, y, z) -> moveDown.setDisable(!(z.getParent() instanceof DynamicContainer)));

        edit.getItems().addAll(undo, redo, new SeparatorMenuItem(), moveUp, moveDown);


        Menu view = new Menu("View");//------------------------------------------

        MenuItem refresh = new MenuItem("Refresh View");
        refresh.setOnAction(x -> controller.resetView());
        refresh.setAccelerator(new KeyCodeCombination(F5, KeyCombination.CONTROL_DOWN));
        view.getItems().addAll(refresh, new SeparatorMenuItem());


        Menu assessmentMenu = new Menu("Assessment");//------------------------------------------

        MenuItem assessment = new MenuItem("Add Assessment");
        assessment.setOnAction(x -> controller.add(parent.getProject(), new Assessment()));
        assessment.setAccelerator(new KeyCodeCombination(A, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));

        assessmentMenu.getItems().addAll(assessment);


        Menu settings = new Menu("Settings");//-----------------------------------
        settings.setOnAction(x -> controller.setOptions());

        MenuItem options = new MenuItem("Options");
        options.setAccelerator(new KeyCodeCombination(O, KeyCombination.CONTROL_DOWN));
        options.setOnAction(x -> controller.setOptions());
        settings.getItems().addAll(options);

        Menu about = new Menu("About");//-----------------------------------
        MenuItem info = new MenuItem("Info");
        info.setOnAction(x -> controller.resetView());
        //info.setAccelerator(new KeyCodeCombination(F1, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));
        info.setOnAction(x -> controller.openAboutDialog());

        MenuItem online = new MenuItem("Online");
        online.setOnAction(x -> controller.openWebResource());

        MenuItem demo = new MenuItem("Open demo");
        demo.setGraphic(new ImageView(new Image(this.getClass().getResourceAsStream("/img/glyphicons-416-disk-open.png"))));
        demo.setOnAction(x -> controller.openDemo());

        MenuItem help = new MenuItem("Help");
        help.setAccelerator(KeyCombination.keyCombination("F1"));
        help.setOnAction(x -> controller.openHelpDialog());
        help.setAccelerator(new KeyCodeCombination(F1, KeyCombination.CONTROL_DOWN));

        about.getItems().addAll(info, online, new SeparatorMenuItem(), demo, help);


        undoRedoController.getUndoList().addListener(new ListChangeListener<EditorAction>() {
            @Override
            public void onChanged(javafx.collections.ListChangeListener.Change<? extends EditorAction> c) {
                if (c.getList().isEmpty())
                    undo.setText("Undo");
                else
                    undo.setText("Undo - " + c.getList().get(0).getName());
            }
        });
        undoRedoController.getRedoList().addListener(new ListChangeListener<EditorAction>() {
            @Override
            public void onChanged(javafx.collections.ListChangeListener.Change<? extends EditorAction> c) {
                if (c.getList().isEmpty())
                    redo.setText("Redo");
                else
                    redo.setText("Redo - " + c.getList().get(0).getName());
            }
        });
        this.getMenus().addAll(file, add, edit, view, assessmentMenu, settings, about);
    }

    private Preferences preferences = Preferences.userRoot().node("Connoisseur");

    public void openPreferencesDialog() {
        Stage preferencesDialog = new Stage();
        preferencesDialog.setTitle("User preferences");

        // User: name, affiliation, email
        Label userInputLabel = new Label("Enter your personal details to appear in assessment reports");
        Label userNameLabel = new Label("Username");
        TextField userNameInput = new TextField(preferences.get("UserName", ""));
        userNameInput.setMinSize(200, 25);

        Label userAffLabel = new Label("Affiliation");
        TextField userAffInput = new TextField(preferences.get("UserAffiliation", ""));
        userAffInput.setMinSize(200, 25);

        Label userMailLabel = new Label("E-mail");
        TextField userMailInput = new TextField(preferences.get("UserEmail", ""));
        userMailInput.setMinSize(200, 25);

        // Directories: home, demo
        Label homeDirectoryLabel = new Label("Home directory");
        TextField homeDirectoryName = new TextField(preferences.get("HomeDirectory", ""));
        Button homeDirectoryButton = new Button("...");
        homeDirectoryButton.setOnAction(x -> {
            DirectoryChooser dir = new DirectoryChooser();
            File homeDir = new File(preferences.get("HomeDirectory", ""));
            if (homeDir.exists()) {
                dir.setInitialDirectory(homeDir);
            }
            try {
                File newDirectory = dir.showDialog(preferencesDialog); // hier steigt er aus mit IllegalArgument, wenn HomeDirectory mit "" gefüllt ist.
                homeDirectoryName.setText(newDirectory.toString());
                dir.setInitialDirectory(homeDir);
            } catch (Exception e) {
            } // no choice/poor choice: no change
        });

        Label demoDirectoryLabel = new Label("Demo directory");
        DirectoryChooser demoDirectory = new DirectoryChooser();
        TextField demoDirectoryName = new TextField(preferences.get("DemoDirectory", ""));
        Button demoDirectoryButton = new Button("...");
        demoDirectoryButton.setOnAction(x -> {
            DirectoryChooser dir = new DirectoryChooser();
            File homeDir = new File(preferences.get("DemoDirectory", ""));
            if (homeDir.exists()) {
                dir.setInitialDirectory(homeDir);
            }
            try {
                File newDirectory = dir.showDialog(preferencesDialog); // hier steigt er aus mit IllegalArgument, wenn HomeDirectory mit "" gefüllt ist.
                homeDirectoryName.setText(newDirectory.toString());
                dir.setInitialDirectory(homeDir);
            } catch (Exception e) {
            } // no choice/poor choice: no change
        });


        // Element options
        Label elementOptions = new Label("Element options");

        // Tree: show size
        CheckBox showSize = new CheckBox();
        showSize.setText("Show size per element");
        showSize.setSelected(preferences.getBoolean("ShowSize", false));

        // Tree: show assessment result
        CheckBox showAssessment = new CheckBox();
        showAssessment.setText("Show assessment per element");
        showAssessment.setSelected(preferences.getBoolean("ShowAssessment", false));

        // Tree/Editor: show CO-type
        CheckBox showCOType = new CheckBox();
        showCOType.setText("Show element type");
        showCOType.setSelected(preferences.getBoolean("ShowCOType", true));

        // Tree/Editor: show CO-ID
        CheckBox showCOID = new CheckBox();
        showCOID.setText("Show element id");
        showCOID.setSelected(preferences.getBoolean("ShowCOID", false));


        // Window options
        Label windowOptions = new Label("Window options");

        // Window: start maximized
        CheckBox startMax = new CheckBox();
        startMax.setText("Maximize window on start");
        startMax.setSelected(preferences.getBoolean("StartMaximized", false));

        // startOnRecent
        CheckBox startOnRecent = new CheckBox();
        startOnRecent.setText("Start with recently used file");
        startOnRecent.setSelected(preferences.getBoolean("StartOnRecent", false));

        // showNoAssessment
        CheckBox hideEmptyAssessment = new CheckBox();
        hideEmptyAssessment.setText("Hide zero assessment");
        hideEmptyAssessment.setSelected(preferences.getBoolean("HideEmptyAssessment", true));

        // CSVOutput
        CheckBox csvOutput = new CheckBox();
        csvOutput.setText("CSV output");
        csvOutput.setSelected(preferences.getBoolean("CSVOutput", false));


        // Control surfaces
        Button applyButton = new Button();
        applyButton.setText("Apply");
        applyButton.setOnAction(x -> {
            preferences.put("UserName", userNameInput.getText());
            preferences.put("UserAffiliation", userAffInput.getText());
            preferences.put("UserEmail", userMailInput.getText());

            preferences.put("HomeDirectory", homeDirectoryName.getText());
            preferences.put("DemoDirectory", demoDirectoryName.getText());

            preferences.putBoolean("ShowAssessment", showAssessment.isSelected());
            preferences.putBoolean("ShowSize", showSize.isSelected());
            preferences.putBoolean("ShowCOType", showCOType.isSelected());
            preferences.putBoolean("ShowCOID", showCOID.isSelected());

            preferences.putBoolean("StartMaximized", startMax.isSelected());
            preferences.putBoolean("StartOnRecent", startOnRecent.isSelected());
            preferences.putBoolean("HideEmptyAssessment", hideEmptyAssessment.isSelected());
            preferences.putBoolean("CSVOutput", csvOutput.isSelected());

            controller.resetView(); // apply changes of presentation options
            preferencesDialog.hide();
        });
        applyButton.setMaxWidth(80);
        applyButton.setMinWidth(80);

        Button cancelButton = new Button();
        cancelButton.setText("Cancel");
        cancelButton.setOnAction(x -> preferencesDialog.hide());
        cancelButton.setMaxWidth(80);
        cancelButton.setMinWidth(80);

		Button resetButton = new Button();
		resetButton.setText("Reset");
		resetButton.setOnAction(x -> {
            System.out.println("Reset Settings");
            BigDecimal xxx = new BigDecimal(0.4444444444444444197728216749965213239192962646484375).setScale(2, RoundingMode.HALF_UP);
            System.out.println(xxx);

            BigDecimal xx = new BigDecimal(0.4444444444444444197728216749965213239192962646484375).multiply(new BigDecimal(100));
            //BigDecimal yy = xx.multiply(new BigDecimal(100));
            BigDecimal yy = xx.setScale(2, RoundingMode.HALF_UP);
            System.out.println(yy+"%");
        });
		resetButton.setMaxWidth(80);
		resetButton.setMinWidth(80);

        Button showButton = new Button();
        showButton.setText("Show");
        showButton.setOnAction(x -> {
            System.out.println("All preferences:------");
            try {
                String[] theKeys = preferences.keys();
                for (String k : theKeys) {
                    System.out.print(k);
                    System.out.print(": ");
                    System.out.println(preferences.get(k, "undefined"));
                }
            } catch (Exception e) {
                System.out.println("No preferences because of exception");
            } ;
            System.out.println("----------------------");
        });
        showButton.setMaxWidth(80);
        showButton.setMinWidth(80);


        Button clearButton = new Button();
        clearButton.setText("Clear");
        clearButton.setOnAction(x -> {
            userNameInput.setText("");
            userAffInput.setText("");
            userMailInput.setText("");

            showAssessment.setSelected(false);
            showSize.setSelected(false);
            showCOType.setSelected(false);
            showCOID.setSelected(false);

            startMax.setSelected(false);
            startOnRecent.setSelected(false);
            hideEmptyAssessment.setSelected(false);
            csvOutput.setSelected(false);

            homeDirectoryName.setText("");
            demoDirectoryName.setText("");

            try {
                preferences.clear();
            } catch (BackingStoreException e) {
            }
        });
        clearButton.setMaxWidth(80);
        clearButton.setMinWidth(80);


        //VBox preferencesLayout = new VBox();
        GridPane preferencesLayout = new GridPane();
        preferencesLayout.setPadding(new Insets(20, 0, 20, 20));
        preferencesLayout.setHgap(10);
        preferencesLayout.setVgap(5);

        preferencesLayout.add(userInputLabel, 0, 0, 5, 1);
        preferencesLayout.add(userNameLabel,  0, 1, 1, 1);
        preferencesLayout.add(userAffLabel,   0, 2, 1, 1);
        preferencesLayout.add(userMailLabel,  0, 3, 1, 1);

        preferencesLayout.add(userNameInput, 1, 1, 2, 1);
        preferencesLayout.add(userAffInput,  1, 2, 2, 1);
        preferencesLayout.add(userMailInput, 1, 3, 2, 1);

        preferencesLayout.add(homeDirectoryLabel, 0, 5, 1, 1);
        preferencesLayout.add(homeDirectoryName,  1, 5, 4, 1);
        preferencesLayout.add(homeDirectoryButton, 5, 5, 1, 1);

        preferencesLayout.add(demoDirectoryLabel, 0, 6, 1, 1);
        preferencesLayout.add(demoDirectoryName,  1, 6, 4, 1);
        preferencesLayout.add(demoDirectoryButton, 5, 6, 1, 1);

        // Element options
        preferencesLayout.add(elementOptions, 0, 8, 3, 1);
        preferencesLayout.add(showAssessment, 0, 9, 3, 1);
        preferencesLayout.add(showSize,       0, 10, 3, 1);
        preferencesLayout.add(showCOType,     0, 11, 3, 1);
        preferencesLayout.add(showCOID,       0, 12, 3, 1);

        // Window options
        preferencesLayout.add(windowOptions, 3, 8,  3, 1);
        preferencesLayout.add(startMax,      3, 9,  3, 1);
        preferencesLayout.add(startOnRecent, 3, 10, 3, 1);
        preferencesLayout.add(hideEmptyAssessment, 3, 11, 3, 1);
        preferencesLayout.add(csvOutput,     3, 12, 3, 1);

        preferencesLayout.add(cancelButton, 0, 14, 1, 1);
        preferencesLayout.add(showButton,   1, 14, 1, 1);
        preferencesLayout.add(resetButton,  2, 14, 1, 1);
        preferencesLayout.add(clearButton,  3, 14, 1, 1);
        preferencesLayout.add(applyButton,  4, 14, 1, 1);

        Scene preferencesScene = new Scene(preferencesLayout, 500, 360);

        preferencesDialog.setScene(preferencesScene);
        preferencesDialog.show();
    }

    public void openInfoBox(String title, String header, String content) {
        Alert infoBox = new Alert(Alert.AlertType.INFORMATION);
        infoBox.setTitle(title);
        infoBox.setHeaderText(header);
        infoBox.setContentText(content);
        infoBox.showAndWait();
    }
}
