package msc.connoisseur.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import msc.connoisseur.control.Controller;

public class ConnoiseurPanel extends FlowPane {
	private Controller controller;
	public ConnoiseurPanel(Controller controller){
		this.controller = controller;
		setPadding(new Insets(20));
		setHgap(10);
		setVgap(0);

	}
	protected Controller getController(){
		return controller;
	}
	protected GridPane titledContainer(String title){
		GridPane grid = new GridPane();
		Label titleLabel = new Label(title);
		titleLabel.getStyleClass().add("title");
		grid.add(titleLabel, 0, 0);
		grid.setHgap(15);
		grid.setVgap(5);
		grid.getColumnConstraints().addAll(subColumnConstraints());
		GridPane.setHalignment(titleLabel, HPos.LEFT);
		GridPane.setValignment(grid, VPos.TOP);
		GridPane.setVgrow(grid, Priority.NEVER);
		grid.getStyleClass().add("titled-pane");
		return grid;
	}
	protected GridPane titledContainer(String title, Node[][] layout){
		GridPane grid = titledContainer(title);
		for(int col = 0; col < layout.length; col++){
			for(int row = 0; row < layout[col].length; row++){
				if(layout[col][row]!=null)
					grid.add(layout[col][row], col, row+1);
					/*takes account for the first row being the title*/
			}
		}
		return grid;
	}
//	protected GridPane titledContainer(String title, Node[][] layout, String info){
//		GridPane grid = titledContainer(title,layout);
//		Label dArea = new Label(info);
//		ImageView icon = new ImageView(new Image(ConnoiseurPanel.class.getResourceAsStream("/msc/connoisseur/resource/info.png")));
//		icon.setOpacity(0.4);
//		dArea.setGraphic(icon);
//		dArea.setWrapText(true);
//		dArea.getStyleClass().add("description");
//		Optional<Node> result = grid.getChildren().stream().collect(Collectors.maxBy((x,y) -> GridPane.getRowIndex(x)-GridPane.getRowIndex(y)));
//		int maxRow = result.isPresent()?GridPane.getRowIndex(result.get()):0;
//		grid.add(dArea, 0, maxRow+1, GridPane.REMAINING,GridPane.REMAINING);
//		return grid;
//	}
	protected GridPane simpleFormField(String title, Node node){
		GridPane grid = new GridPane();
		grid.getStyleClass().add("simple-form-field");
		Label l = new Label(title);
		l.getStyleClass().add("title");
		grid.add(l, 0, 0);
		grid.add(node, 0, 1);
		return grid;
	}
	protected ColumnConstraints[] subColumnConstraints(){
		ColumnConstraints[] colc = {new ColumnConstraints(),new ColumnConstraints()};
		colc[0].setHalignment(HPos.RIGHT);
		colc[0].setHgrow(Priority.ALWAYS);
		colc[1].setHalignment(HPos.RIGHT);
		colc[1].setMaxWidth(50);
		return colc;
	}
	protected ColumnConstraints[] columnConstraints(){
		ColumnConstraints[] colc = {new ColumnConstraints(),new ColumnConstraints()};
		colc[0].setFillWidth(true);
		colc[1].setFillWidth(true);
		return colc;
	}
	protected void add(Node n){
		this.getChildren().add(n);
	}
}
