package msc.connoisseur.view.assessment;

import javafx.geometry.Pos;
import javafx.print.PrinterJob;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.intent.Intent;
import msc.connoisseur.view.EditorPanel;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.prefs.Preferences;

public class AssessorView extends BorderPane {
    TextArea textArea;
    HBox buttonPane;
    private Preferences prefs = Preferences.userRoot().node("Connoisseur");

    public AssessorView(EditorPanel editorPanel) {
        //setStyle("-fx-background: #000000;");
        setId("AssessorView");
        textArea = new TextArea();
        textArea.setEditable(false);

        //buttonPane = new HBox();
        buttonPane = new HBox();
        buttonPane.setAlignment(Pos.CENTER_RIGHT);

        // Assessment
        Button assess = new Button("Assess");
        assess.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/img/wine_small.png"))));
        assess.setOnAction(x -> {
            editorPanel.getProject().getList().stream()
                    .filter(co -> co instanceof Assessment)
                    .forEach(co -> post((Assessment) co));
            System.out.println("posted overview");
        });
        buttonPane.getChildren().add(assess);

        // Details
        Button report = new Button("Details");
        report.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/img/details_small.png"))));
        report.setOnAction(x -> {
            editorPanel.getProject().getList().stream()
                    .filter(co -> co instanceof Assessment)
                    .forEach(co -> postDetails((Assessment) co));
            System.out.println("posted details");
        });
        buttonPane.getChildren().add(report);

        // Size
        Button size = new Button("Size");
        size.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/img/ruler.png"))));
        size.setOnAction(x -> {
            editorPanel.getProject().getList().stream()
                    .filter(co -> co instanceof Assessment)
                    .forEach(co -> postSizes((Assessment) co));
            System.out.println("posted sizes");
        });
        buttonPane.getChildren().add(size);

        // Copying
        Button copy = new Button("Copy");
        copy.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/img/copy.png"))));
        copy.setOnAction(x -> {
            StringSelection str = new StringSelection(textArea.getText());
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, str);
        });
        buttonPane.getChildren().add(copy);

        // Printing
        Button print = new Button("Print");
        print.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/img/print.png"))));
        print.setOnAction(x -> {
            PrinterJob job = PrinterJob.createPrinterJob();
            Label text = new Label(textArea.getText());
            text.setFont(new Font(10));
            VBox page = new VBox(text);
            if (job != null && job.showPrintDialog(this.getScene().getWindow())) {
                boolean success = job.printPage(page);
                if (success) {
                    job.endJob();
                }
            }
        });
        buttonPane.getChildren().add(print);

        // Clear panel
        Button clear = new Button("Clear");
        clear.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/img/erase.png"))));
        clear.setOnAction(x -> textArea.clear());
        buttonPane.getChildren().add(clear);

        // put whole window toegether (?)
        buttonPane.setId("AssessorButtonBar");
        Label element = new Label("Output Console");
        element.setAlignment(Pos.CENTER_LEFT);
        element.setTextAlignment(TextAlignment.LEFT);
        element.setId("TopLabel");
        setTop(new VBox(element, buttonPane));
        setCenter(textArea);
    }

    private void post(Assessment assessment) {
        System.out.println("AssessorView.post: ");

        writeLine("Assessment: " + assessment);
        writeLine("Scope: " + assessment.getScope());
        Intent assumptions = assessment.getAssumptions();
        if (assumptions != null)
            writeLine("Assumptions: " + assessment.getAssumptions());
        writeLine("Result: " + assessment.assess(assessment));
        newLine();
    }

    private void postSizes(Assessment assessment) {
        System.out.println("AssessorView.postSizes: ");
    };

    private void postDetails(Assessment assessment) {
        System.out.println("AssessorView.postDetails: ");

        // Header
        writeLine("----------------------------------------------");
        writeLine("Connoisseur v" + prefs.get("ConnoisseurVersion", "0") + " (c) 2015-2020, Harald St\u00F6rrle");

        // Scope
        ConnoisseurObject co = assessment.getScope();
        writeLine("assessment " + assessment.getName() + " of " + co.getName());

        // Credit
        String _nam = prefs.get("UserName", "");
        String name;
        if (!_nam.equals("")) {
            name = "by " + _nam;
        } else {
            name = "";
        }

        String _afl = prefs.get("UserAffiliation", "");
        String affl;
        if (!_afl.equals("")) {
            affl = ", " + _afl;
        } else {
            affl = "";
        }

        String _mal = prefs.get("UserEmail", "");
        String mail;
        if (!_mal.equals("")) {
            mail = " (" + _mal + ")";
        } else {
            mail = "";
        }
        writeLine(name + affl + mail);

        // Tracing
        String now = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss Z").format(Calendar.getInstance().getTime());
        writeLine("at " + now);

        // output assumptions

        // output result
        writeLine(co.toValueString() + " (overall visual quality)");
        writeLine("----------------------------------------------");
        newLine();
    }

    private void newLine() {
        writeLine("");
    }

    private void writeLine(String text) {
        textArea.setText(textArea.getText() + text + "\n");
    }

}
