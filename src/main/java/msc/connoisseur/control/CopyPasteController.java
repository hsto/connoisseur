package msc.connoisseur.control;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.common.DynamicContainer;

public class CopyPasteController {
    private Controller controller;
	private ConnoisseurObject heldObject = null;
	public BooleanProperty hasCopied = new SimpleBooleanProperty(false);
    public CopyPasteController(Controller controller){
        this.controller = controller;
    }
	public void copy(ConnoisseurObject object){
		hasCopied.setValue(true);
		heldObject = object.clone();
	}
	public boolean paste(ConnoisseurObject destination){
		ConnoisseurObject source = heldObject;//lol
		if(destination instanceof DynamicContainer && ((DynamicContainer)destination).getContainedClass().isAssignableFrom(source.getClass())){
			DynamicContainer container = (DynamicContainer) destination;
            controller.add(container,source);
			return true;
		}else if(destination.getClass() == source.getClass()){
            controller.cloneInto(destination,source);
            return true;
        }
		return false;
	}

}
