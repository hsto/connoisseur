package msc.connoisseur.control;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.ConnoisseurProject;
import msc.connoisseur.model.LanguageSystem;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.assessment.Estimatable;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.bertin.VisualVariable;
import msc.connoisseur.model.common.DynamicContainer;
import msc.connoisseur.model.intent.Intent;
import msc.connoisseur.model.language.Language;
import msc.connoisseur.model.language.semantics.Concept;
import msc.connoisseur.model.language.semantics.Domain;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.language.syntax.CompoundGrapheme;
import msc.connoisseur.model.language.syntax.Grapheme;
import msc.connoisseur.view.EditorPanel;

import java.awt.*;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Optional;
import java.util.prefs.Preferences;

public class Controller {
    private final UndoRedoController undoRedoController;
    private EditorPanel registeredEditor = null;
    private HashMap<ConnoisseurObject, String> pathMapping = new HashMap<>();
    private SimpleBooleanProperty dirty = new SimpleBooleanProperty();

    public Controller(EditorPanel editor, UndoRedoController undoRedoController) {
        registerEditor(editor);
        this.undoRedoController = undoRedoController;
        dirty.setValue(false);
    }


    private Preferences preferences = Preferences.userRoot().node("Connoisseur");

    public void setOptions() {
        registeredEditor.etb.openPreferencesDialog();
    }

    public <T extends ConnoisseurObject> void move(DynamicContainer<T> target, T source) {
        EditorAction ea = new EditorAction() {
            DynamicContainer<T> oldParent = (DynamicContainer<T>) source.getParent();
            int oldIndex = oldParent.indexOf(source);

            @Override
            public void apply() {
                oldParent.remove(source);
                target.add(source);
                resetView();
            }

            @Override
            public void revert() {
                target.remove(source);
                oldParent.add(oldIndex, source);
                resetView();
            }

            @Override
            public String getName() {
                return "move \'" + source + "\'";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public <T extends ConnoisseurObject> void move(DynamicContainer<T> target, T source, int index) {
        EditorAction ea = new EditorAction() {
            DynamicContainer<T> oldParent = (DynamicContainer<T>) source.getParent();
            int oldIndex = oldParent.indexOf(source);

            @Override
            public void apply() {
                oldParent.remove(source);
                target.add(index, source);
                resetView();
            }

            @Override
            public void revert() {
                target.remove(source);
                oldParent.add(oldIndex, source);
                resetView();
            }

            @Override
            public String getName() {
                return "move \'" + source + "\'";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public <T> void add(DynamicContainer<T> destination, T object) {
        EditorAction ea = new EditorAction() {
            @Override
            public void apply() {
                destination.add(object);
                resetView();
            }

            @Override
            public void revert() {
                destination.remove(object);
                resetView();
            }

            @Override
            public String getName() {
                return "add \'" + object + "\'";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public void add(LanguageSystem destination, Language object) {
        EditorAction ea = new EditorAction() {
            @Override
            public void apply() {
                destination.getLanguages().add(object);
                resetView();
            }

            @Override
            public void revert() {
                destination.getLanguages().remove(object);
                resetView();
            }

            @Override
            public String getName() {
                return "add \'" + object + "\'";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public <T> void remove(DynamicContainer<T> parent, T child) {
        EditorAction ea = new EditorAction() {
            int index = parent.indexOf(child);

            @Override
            public void apply() {
                parent.remove(child);
                resetView();
            }

            @Override
            public void revert() {
                parent.add(index, child);
                resetView();
            }

            @Override
            public String getName() {
                return "delete \'" + child + "\'";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public void remove(LanguageSystem languageSystem, Language language) {
        EditorAction ea = new EditorAction() {
            int index = languageSystem.getLanguages().indexOf(language);

            @Override
            public void apply() {
                languageSystem.getLanguages().remove(language);
                resetView();
            }

            @Override
            public void revert() {
                languageSystem.getLanguages().add(index, language);
                resetView();
            }

            @Override
            public String getName() {
                return "delete \'" + language + "\'";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public void replace(LanguageSystem languageSystem, Language oldValue, Language newValue) {
        EditorAction ea = new EditorAction() {
            @Override
            public void apply() {
                int index = languageSystem.getLanguages().indexOf(oldValue);
                languageSystem.getLanguages().remove(oldValue);
                languageSystem.getLanguages().add(index, newValue);
                resetView();
            }

            @Override
            public void revert() {
                int index = languageSystem.getLanguages().indexOf(newValue);
                languageSystem.getLanguages().remove(newValue);
                languageSystem.getLanguages().add(index, oldValue);
                resetView();
            }

            @Override
            public String getName() {
                return "replace \'" + oldValue + "\'";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public <T> void setValue(VisualVariable<T> var, T value) {
        EditorAction ea = new EditorAction() {
            T oldValue = var.getValue();

            @Override
            public void apply() {
                var.setValue(value);

            }

            @Override
            public void revert() {
                var.setValue(oldValue);
            }

            @Override
            public String getName() {
                return "set " + var;
            }

        };
        undoRedoController.applyAction(ea, this);
    }

    public void resetPropertiesView() {
        registeredEditor.resetPropertiesView();

    }

    public void changeName(ConnoisseurObject co, String text) {
        EditorAction ea = new EditorAction() {
            String oldVal = co.getName();

            @Override
            public void apply() {
                co.setName(text);
                resetLabel(co);
            }

            @Override
            public void revert() {
                co.setName(oldVal);
                resetLabel(co);
            }

            @Override
            public String getName() {
                return "set name";
            }

        };
        undoRedoController.applyAction(ea, this);

    }

    public <T extends ConnoisseurObject> void moveDown(DynamicContainer<T> parent, T object) {
        EditorAction ea = new EditorAction() {

            @Override
            public void apply() {
                int index = parent.indexOf(object);
                if (parent.getList().contains(object) && index < parent.getList().size() - 1) {
                    //System.out.println("move down");
                    parent.remove(object);
                    parent.add(++index, object);
                }
                resetView();
            }

            @Override
            public void revert() {
                int index = parent.indexOf(object);
                if (parent.getList().contains(object) && index > 0) {
                    parent.remove(object);
                    parent.add(--index, object);
                }
                resetView();
            }

            @Override
            public String getName() {
                return "Move " + object + " down";
            }

        };
        undoRedoController.applyAction(ea, this);

    }

    public <T extends ConnoisseurObject> void moveUp(DynamicContainer<T> parent, T object) {
        EditorAction ea = new EditorAction() {

            @Override
            public void apply() {
                int index = parent.indexOf(object);
                if (parent.getList().contains(object) && index > 0) {
                    parent.remove(object);
                    parent.add(--index, object);
                }
                resetView();
            }

            @Override
            public void revert() {
                int index = parent.indexOf(object);
                if (parent.getList().contains(object) && index < parent.getList().size() - 1) {
                    parent.remove(object);
                    parent.add(++index, object);
                }
                resetView();
            }

            @Override
            public String getName() {
                return "Move " + object + " up";
            }

        };
        undoRedoController.applyAction(ea, this);

    }

    public void save(ConnoisseurProject project) {
        if (project == null) return;
        String path = pathMapping.get(project);
        if (path == null) {
            saveAs(project);
        } else {
            try {
                XMLEncoder e = new XMLEncoder(
                        new BufferedOutputStream(
                                new FileOutputStream(path)));
                e.writeObject(project);
                e.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            preferences.put("LastDirectory", path);
        }
        dirty.setValue(false);

    }

    public void saveAs(ConnoisseurProject project) {
        if (project == null) return;
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Connoisseur Project File", "*.con"));
        File file = fileChooser.showSaveDialog(registeredEditor.getScene().getWindow());
        if (file != null) {
            pathMapping.remove(project);
            pathMapping.put(project, file.getAbsolutePath());
            save(project);
            preferences.put("LastDirectory", file.getParent()); // toString?!
        }
    }

    public void openDemo() {
        File file = openLastDirectory("DemoDirectory");
        if (file != null) {
            try {
                XMLDecoder d = new XMLDecoder(
                        new BufferedInputStream(
                                new FileInputStream(file.getAbsolutePath())));
                ConnoisseurProject project = (ConnoisseurProject) d.readObject();
                pathMapping.put(project, file.getAbsolutePath());
                registeredEditor.setProject(project);
                d.close();
                dirty.setValue(false);
            } catch (FileNotFoundException e) {
                // e.printStackTrace(); // simply ignore this - user didn't choose a file which is perfectly ok.
            }
            preferences.put("LastDirectory", file.getParent()); // toString?!
        }
    }

    public void openLastFile() {
        File file = new File(preferences.get("LastFile", ""));
        if (file.exists()) {

        } else {
            file = openLastDirectory("LastDirectory");
        };
        if (file != null) {
            try {
                preferences.put("LastDirectory", file.getParent()); // store directory now to persist path, even if parsing fails
                XMLDecoder d = new XMLDecoder(
                        new BufferedInputStream(
                                new FileInputStream(file.getAbsolutePath())));
                ConnoisseurProject project = (ConnoisseurProject) d.readObject();
                pathMapping.put(project, file.getAbsolutePath());
                registeredEditor.setProject(project);
                d.close();
                dirty.setValue(false);
            } catch (IllegalArgumentException e) {
                // not a valid file
            } catch (IOException e) {
                // whatever it takes to make IntelliJ happy
            }
        }
    }

    public void open() {
        if (dirty.get()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Open Project");
            alert.setHeaderText("There are unsaved changes in your current project");
            alert.setContentText("Discard changes and proceed?");
            Optional<ButtonType> result = alert.showAndWait();
            if (!result.isPresent() || result.get() != ButtonType.OK) return;
        }
        File file = openLastDirectory("LastDirectory");

        if (file != null) {
            try {
                preferences.put("LastDirectory", file.getParent()); // store directory now to persist path, even if parsing fails
                preferences.put("LastFile", file.getPath());
                XMLDecoder d = new XMLDecoder(
                        new BufferedInputStream(
                                new FileInputStream(file.getAbsolutePath())));
                ConnoisseurProject project = (ConnoisseurProject) d.readObject();
                pathMapping.put(project, file.getAbsolutePath());
                registeredEditor.setProject(project);
                d.close();
                dirty.setValue(false);
            } catch (IllegalArgumentException e) {
                // not a valid file
            } catch (IOException e) {
                // whatever it takes to make IntelliJ happy
            }
        }
    }

    public void merge() {
        // open another specification
        File file = openLastDirectory("LastDirectory");

        // parse
        if (file != null) {
            try {
                XMLDecoder d = new XMLDecoder(
                        new BufferedInputStream(
                                new FileInputStream(file.getAbsolutePath())));
                ConnoisseurProject mergeProject = (ConnoisseurProject) d.readObject();
                ConnoisseurProject baseProject = (ConnoisseurProject) registeredEditor.getProject();

                // merge
                baseProject.appendList(mergeProject.getList()); // add elements of mergeProject to baseProject
                resetView(); // update tree view

                d.close();
                dirty.setValue(true);
            } catch (IllegalArgumentException e) {
                // not a valid file
            } catch (IOException e) {
                // whatever it takes to make IntelliJ happy
            }
        }
    }

    public File openLastDirectory(String preferredDirectory) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Connoisseur Project File", "*.con"));
        File prefDir = new File(preferences.get(preferredDirectory, ""));
        if (prefDir.exists()) {
            fileChooser.setInitialDirectory(prefDir);
        }
        File file = fileChooser.showOpenDialog(registeredEditor.getScene().getWindow());
        preferences.put("LastFile", file.getPath());
        return file;
    }

    public void exit() {
        javafx.application.Platform.exit();
    }

    public void openWebResource() {
        String page = preferences.get("OnlineInfo", "https://www.pst.ifi.lmu.de/~stoerrle/");
        try {
            Desktop.getDesktop().browse(new URI(page));
        } catch (Exception e) {
        }
    }

    public void openAboutDialog() {
        String header = "This is Connoisseur v" + preferences.get("ConnoisseurVersion", "")
                + " of " + preferences.get("ConnoisserDate", "") + "\n"
                + "(c) 2015-2020, Harald St\u00F6rrle";
        String content = "The connoisseur application supports the quality assessment of visual languages.\n"
                + " * Idea, design, project management, BitBucket administration\n"
                + "   and some coding by Harald St\u00F6rrle,\n"
                + " * Most of the initial coding by Jeppe Hartmund in his 2016\n"
                + "   MSc thesis at the DTU, Lyngby, Denmark,\n"
                + " * CI/CD set up Bernhard Saumweber.\n\n"
                + "As of 2020, this application is open source under the Apache 2.0 License.";
        registeredEditor.etb.openInfoBox("About Connoisseur", header, content);
    }//------------------------------------------------

    public void openHelpDialog() {
        String header = "This is Connoisseur v" + preferences.get("ConnoisseurVersion", "")
                + " of " + preferences.get("ConnoisserDate", "") + "\n"
                + "(c) 2015-2020, Harald St\u00F6rrle";
        String content =
                  "A connoisseur is 'a person who is especially competent to pass \n"
                + "critical judgments in an art, particularly one of the fine arts.',\n"
                + "and the design of visual languages is certainly a point in case, \n"
                + "as there there is currently little guidance and even less scientific \n"
                + "evidence as to what constitutes a 'good' visual language."
                + "\n  This application aspires to help with the repeatable and \n"
                + "objective evaluation of the cognitive complexity and visual \n"
                + "quality of visual languages (think UML)."
                + "\n  Connoisseur implements a theory inspired by Daniel Moody's \n"
                + "'Physics of Notation', but supersedes it in many ways. Read \n"
                + "'Towards an Operationalization of the \"Physics of Notations\"\n"
                + "for the Analysis of Visual Languages' by H. St\u00F6rrle and A. Fish \n"
                + "in Proc. MoDELS, ACM/IEEE 2013' for more background information.";
        registeredEditor.etb.openInfoBox("Help on using Connoisseur", header, content);
    }

    public void resetView() {
        if (registeredEditor == null) return;
        registeredEditor.reset();
    }

    public void resetLabel(ConnoisseurObject co) {
        if (registeredEditor == null) return;
        registeredEditor.updateHierarchyLabel(co);
    }

    public void registerEditor(EditorPanel ep) {
        registeredEditor = ep;
        newProject(false);
    }

    public void newProject(boolean showPrompt) {
        if (registeredEditor == null) return;
        if (showPrompt && dirty.get()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("New Project");
            alert.setHeaderText("There are unsaved changes in your current project");
            alert.setContentText("Discard changes and proceed?");
            Optional<ButtonType> result = alert.showAndWait();
            if (!result.isPresent() || result.get() != ButtonType.OK) return;
        }
        ConnoisseurProject project = new ConnoisseurProject();
        project.setName("Project");
        dirty.setValue(true);
        registeredEditor.setProject(project);
    }

    public void changeDescription(ConnoisseurObject model, String text) {
        EditorAction ea = new EditorAction() {
            String oldDescription = model.getDescription();

            @Override
            public void apply() {
                model.setDescription(text);
            }

            @Override
            public void revert() {
                model.setDescription(oldDescription);

            }

            @Override
            public String getName() {
                return "set description";
            }
        };
        undoRedoController.applyAction(ea, this);

    }

    public void changeId(ConnoisseurObject model, String id) {
        EditorAction ea = new EditorAction() {
            String old = model.getId();

            @Override
            public void apply() {
                model.setId(id);
                resetLabel(model);
            }

            @Override
            public void revert() {
                model.setId(old);
                resetLabel(model);
            }

            @Override
            public String getName() {
                return "set ID";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public void changeEstimate(Estimatable model, double value) {
        EditorAction ea = new EditorAction() {
            double old = model.getEstimate();

            @Override
            public void apply() {
                model.setEstimate(value);
            }

            @Override
            public void revert() {
                model.setEstimate(old);
            }

            @Override
            public String getName() {
                return "set estimate";
            }
        };
        undoRedoController.applyAction(ea, this);

    }

    public void changeReason(Estimatable model, String value) {
        EditorAction ea = new EditorAction() {
            String old = model.getReason();

            @Override
            public void apply() {
                model.setReason(value);
            }

            @Override
            public void revert() {
                model.setReason(old);
            }

            @Override
            public String getName() {
                return "set reason";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public void changeShape(AtomicGrapheme model, Shape s) {
        EditorAction ea = new EditorAction() {
            Shape old = model.getVisualVector().getShape();

            @Override
            public void apply() {
                model.setShape(s);
                s.getTexture().setValue(old.getTexture().getValue());
                resetLabel(model);
                resetPropertiesView();
            }

            @Override
            public void revert() {
                model.setShape(old);
                resetLabel(model);
                resetPropertiesView();
            }

            @Override
            public String getName() {
                return "change shape";
            }
        };
        undoRedoController.applyAction(ea, this);

    }

    public CompoundGrapheme convertToCompound(AtomicGrapheme g) {
        CompoundGrapheme newParent = new CompoundGrapheme();
        EditorAction ea = new EditorAction() {
            DynamicContainer<Grapheme> oldParent = (DynamicContainer<Grapheme>) g.getParent();
            int index = oldParent.indexOf(g);

            @Override
            public void apply() {
                oldParent.remove(g);
                oldParent.add(index, newParent);
                newParent.add(g);
                resetView();
            }

            @Override
            public void revert() {
                oldParent.remove(newParent);
                oldParent.add(index, g);
                newParent.remove(g);
                resetView();
            }

            @Override
            public String getName() {
                return "convert to compound";
            }
        };
        undoRedoController.applyAction(ea, this);
        return newParent;
    }

    public void add(AtomicGrapheme destination, Grapheme object) {
        CompoundGrapheme cg = convertToCompound(destination);
        add(cg, object);
    }

    public <T extends ConnoisseurObject> void cloneInto(T destination, T source) {
        EditorAction ea = new EditorAction() {
            T old = (T) destination.clone();

            @Override
            public void apply() {
                destination.loadInto(source);
                resetLabel(destination);
                resetPropertiesView();
            }

            @Override
            public void revert() {
                destination.loadInto(old);
                resetLabel(destination);
                resetPropertiesView();
            }

            @Override
            public String getName() {
                return "set " + destination;
            }
        };
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Replace object: " + destination);
        alert.setHeaderText("This action will discard the changes" +
                "\nmade to the target object");
        alert.setContentText("Proceed and replace?");
        Optional<ButtonType> result = alert.showAndWait();
        if (!result.isPresent() || result.get() != ButtonType.OK) return;
        undoRedoController.applyAction(ea, this);
    }

    public void setBaseGrapheme(CompoundGrapheme model, Grapheme newval) {
        EditorAction ea = new EditorAction() {
            Grapheme old = model.get(0);
            int oldIndex = model.indexOf(newval);

            @Override
            public void apply() {
                model.remove(newval);
                model.add(0, newval);
                resetView();
            }

            @Override
            public void revert() {
                model.remove(newval);
                model.add(oldIndex, newval);
                resetView();
            }

            @Override
            public String getName() {
                return "set base grapheme";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public void changeReference(Domain model, String value) {
        EditorAction ea = new EditorAction() {
            String old = model.getReference();

            @Override
            public void apply() {
                model.setReference(value);
            }

            @Override
            public void revert() {
                model.setReference(old);
            }

            @Override
            public String getName() {
                return "set Reference";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public void changeReference(Concept model, String value) {
        EditorAction ea = new EditorAction() {
            String old = model.getReference();

            @Override
            public void apply() {
                model.setReference(value);
            }

            @Override
            public void revert() {
                model.setReference(old);
            }

            @Override
            public String getName() {
                return "set Reference";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public void setWeight(Assessment assessment, ConnoisseurObject object, int value) {
        EditorAction ea = new EditorAction() {
            int oldValue = assessment.getWeights().getWeight(object);

            @Override
            public void apply() {
                assessment.getWeights().setWeight(object, value);
            }

            @Override
            public void revert() {
                assessment.getWeights().setWeight(object, oldValue);
            }

            @Override
            public String getName() {
                return "set weight";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public void setWeight(Assessment assessment, Class<? extends VisualVariable> object, int value) {
        EditorAction ea = new EditorAction() {
            int oldValue = assessment.getWeights().getWeight(object);

            @Override
            public void apply() {
                assessment.getWeights().setWeight(object, value);
            }

            @Override
            public void revert() {
                assessment.getWeights().setWeight(object, oldValue);
            }

            @Override
            public String getName() {
                return "set weight";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public SimpleBooleanProperty getDirtyProperty() {
        return dirty;
    }

    public void setScope(Assessment model, ConnoisseurObject newValue) {
        EditorAction ea = new EditorAction() {
            ConnoisseurObject oldValue = model.getScope();

            @Override
            public void apply() {
                model.setScope(newValue);
            }

            @Override
            public void revert() {
                model.setScope(oldValue);
            }

            @Override
            public String getName() {
                return "set scope";
            }
        };
        undoRedoController.applyAction(ea, this);
    }


    public void setAssumptions(Assessment model, Intent newValue) {
        EditorAction ea = new EditorAction() {
            Intent oldValue = model.getAssumptions();

            @Override
            public void apply() {
                model.setAssumptions(newValue);
            }

            @Override
            public void revert() {
                model.setAssumptions(oldValue);
            }

            @Override
            public String getName() {
                return "set assumptions";
            }
        };
        undoRedoController.applyAction(ea, this);
    }

    public void setAggregationMethod(Assessment assessment, Assessment.AggregationMethods.Method newValue) {
        EditorAction ea = new EditorAction() {
            Assessment.AggregationMethods.Method oldValue = assessment.getAggregationMethod();

            @Override
            public void apply() {
                assessment.setAggregationMethod(newValue);
            }

            @Override
            public void revert() {
                assessment.setAggregationMethod(oldValue);
            }

            @Override
            public String getName() {
                return "set aggregation method";
            }
        };
        undoRedoController.applyAction(ea, this);
    }
}
