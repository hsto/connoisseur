package msc.connoisseur.control;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class UndoRedoController {
	private final int UNDO_LENGTH = 10;
	private final int REDO_LENGTH = 10;
	private ObservableList<EditorAction> undoList = FXCollections.observableArrayList();
	private ObservableList<EditorAction> redoList = FXCollections.observableArrayList();

	public void applyAction(EditorAction ea,Controller controller){
        controller.getDirtyProperty().setValue(true);
		ea.apply();
		undoList.add(0,ea);
		redoList.clear();
	}
	public boolean undoPossible(){
		return !undoList.isEmpty();
	}
	public boolean redoPossible(){
		return !redoList.isEmpty();
	}
	public void undo(Controller controller){
		if(undoList.isEmpty())return;
		EditorAction ea = undoList.remove(0);
		ea.revert();
		redoList.add(0,ea);
		controller.resetView();
		trimRedoList();
	}
	public void redo(Controller controller){
		if(redoList.isEmpty())return;
		EditorAction ea = redoList.remove(0);
		ea.apply();
		undoList.add(0,ea);
		controller.resetView();
		trimUndoList();
	}
	@SuppressWarnings("unused")
	private void trimUndoList(){
		while(UNDO_LENGTH >= 0 && undoList.size() > UNDO_LENGTH)
			undoList.remove(undoList.size()-1);

	}
	@SuppressWarnings("unused")
	private void trimRedoList(){
		while(REDO_LENGTH >= 0 && redoList.size() > REDO_LENGTH)
			redoList.remove(redoList.size()-1);

	}
	public ObservableList<EditorAction> getUndoList() {
		return undoList;
	}
	public ObservableList<EditorAction> getRedoList() {
		return redoList;
	}
}
