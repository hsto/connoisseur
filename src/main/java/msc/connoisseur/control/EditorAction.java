package msc.connoisseur.control;

public abstract class EditorAction {
	public abstract void apply();
	public abstract void revert();
	public abstract String getName();
}
