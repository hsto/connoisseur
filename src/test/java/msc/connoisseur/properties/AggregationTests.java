package msc.connoisseur.properties;

import msc.connoisseur.control.Controller;
import msc.connoisseur.control.CopyPasteController;
import msc.connoisseur.control.UndoRedoController;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.bertin.Size;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.language.syntax.CompoundGrapheme;
import msc.connoisseur.model.visualvector.VisualVector;
import msc.connoisseur.builders.*;
import msc.connoisseur.builders.visual.SizeBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Jeppe on 31-10-2016.
 */
public class AggregationTests {
    private UndoRedoController undoRedoController;
    private Controller controller;
    private CopyPasteController copyPasteController;

    @Before
    public void setUp() throws Exception {
        undoRedoController = new UndoRedoController();
        controller = new Controller(null,undoRedoController);
        copyPasteController = new CopyPasteController(controller);
    }

    @After
    public void tearDown() throws Exception {
        undoRedoController = null;
        controller = null;
    }

    @Test
    public void assessment_aggregationbiggest() throws Exception {
        //arrange
        Shape region = new RegionShapeBuilder().build();
        Size sizeSmall = new SizeBuilder().withSize(Size.SizeValue.tiny).build();
        Size sizeHuge = new SizeBuilder().withSize(Size.SizeValue.huge).build();
        VisualVector v1 = new VisualVectorBuilder().withSize(sizeSmall).withShape(region.clone()).build();
        VisualVector v2 = new VisualVectorBuilder().withSize(sizeHuge).withShape(region.clone()).build();
        AtomicGrapheme a = new AtomicGraphemeBuilder().withVisualVector(v1).build();
        AtomicGrapheme b = new AtomicGraphemeBuilder().withVisualVector(v2).build();
        Assessment assessment = new AssessmentBuilder().withAggregationMethod(Assessment.AggregationMethods.Method.biggest).build();
        CompoundGrapheme c = new CompoundGraphemeBuilder().withChild(a).withChild(b).build();

        //act
        VisualVector v3 = assessment.aggregate(c);

        //assert
        Assert.assertEquals(v2,v3);
        Assert.assertNotEquals(v1,v2);
    }

    @Test
    public void assessment_aggregationsmallest() throws Exception {
        //arrange
        Shape region = new RegionShapeBuilder().build();
        Size sizeSmall = new SizeBuilder().withSize(Size.SizeValue.tiny).build();
        Size sizeHuge = new SizeBuilder().withSize(Size.SizeValue.huge).build();
        VisualVector v1 = new VisualVectorBuilder().withSize(sizeSmall).withShape(region.clone()).build();
        VisualVector v2 = new VisualVectorBuilder().withSize(sizeHuge).withShape(region.clone()).build();
        AtomicGrapheme a = new AtomicGraphemeBuilder().withVisualVector(v1).build();
        AtomicGrapheme b = new AtomicGraphemeBuilder().withVisualVector(v2).build();
        Assessment assessment = new AssessmentBuilder().withAggregationMethod(Assessment.AggregationMethods.Method.smallest).build();
        CompoundGrapheme c = new CompoundGraphemeBuilder().withChild(a).withChild(b).build();

        //act
        VisualVector v3 = assessment.aggregate(c);

        //assert
        Assert.assertEquals(v1,v3);
        Assert.assertNotEquals(v1,v2);
    }

    @Test
    public void assessment_aggregationbiggestregion() throws Exception {
        //arrange
        Shape region = new RegionShapeBuilder().build();
        Size sizeSmall = new SizeBuilder().withSize(Size.SizeValue.tiny).build();
        Size sizeHuge = new SizeBuilder().withSize(Size.SizeValue.huge).build();
        VisualVector v1 = new VisualVectorBuilder().withSize(sizeSmall).withShape(region.clone()).build();
        VisualVector v2 = new VisualVectorBuilder().withSize(sizeHuge).build();
        AtomicGrapheme a = new AtomicGraphemeBuilder().withVisualVector(v1).build();
        AtomicGrapheme b = new AtomicGraphemeBuilder().withVisualVector(v2).build();
        Assessment assessment = new AssessmentBuilder().withAggregationMethod(Assessment.AggregationMethods.Method.biggestregion).build();
        CompoundGrapheme c = new CompoundGraphemeBuilder().withChild(a).withChild(b).build();

        //act
        VisualVector v3 = assessment.aggregate(c);

        //assert
        Assert.assertEquals(v1,v3);
        Assert.assertNotEquals(v1,v2);
    }

    @Test
    public void assessment_aggregationbase() throws Exception {
        //arrange
        Shape region = new RegionShapeBuilder().build();
        Size sizeSmall = new SizeBuilder().withSize(Size.SizeValue.tiny).build();
        Size sizeHuge = new SizeBuilder().withSize(Size.SizeValue.huge).build();
        VisualVector v1 = new VisualVectorBuilder().withSize(sizeSmall).withShape(region.clone()).build();
        VisualVector v2 = new VisualVectorBuilder().withSize(sizeHuge).build();
        AtomicGrapheme a = new AtomicGraphemeBuilder().withVisualVector(v1).build();
        AtomicGrapheme b = new AtomicGraphemeBuilder().withVisualVector(v2).build();
        Assessment assessment = new AssessmentBuilder().withAggregationMethod(Assessment.AggregationMethods.Method.base).build();
        CompoundGrapheme c = new CompoundGraphemeBuilder().withChild(a).withChild(b).build();

        //act
        VisualVector v3 = assessment.aggregate(c);

        //assert
        Assert.assertEquals(v1,v3);
        Assert.assertNotEquals(v1,v2);
    }

}
