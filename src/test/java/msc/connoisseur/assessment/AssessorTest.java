package msc.connoisseur.assessment;


import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.common.Pair;
import msc.connoisseur.model.configuration.Weights;
import msc.connoisseur.model.language.semantics.Concept;
import msc.connoisseur.model.language.semantics.GraphemeMapping;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.language.syntax.Graphemes;
import msc.connoisseur.model.visualvector.VisualVector;
import msc.vino.Assessor;
import msc.connoisseur.builders.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Jeppe on 30-10-2016.
 */
public class AssessorTest {
    @Before
    public void setup(){

    }
    @After
    public void tearDown(){

    }

    @Test
    public void assesstest_graphemes_empty() throws Exception {
        Graphemes gs = new GraphemesBuilder().build();
        Weights w = new WeightsBuilder().build();
        Assessment a = new AssessmentBuilder().withWeights(w).build();
        double assessment = Assessor.assess(a,(ConnoisseurObject)gs);

        Assert.assertEquals(1.0,assessment,0.001);
    }

    @Test
    public void assesstest_graphemes_sameelements() throws Exception {
        Shape s = new RegionShapeBuilder().build();
        VisualVector v = new VisualVectorBuilder().withShape(s).build();
        AtomicGrapheme a = new AtomicGraphemeBuilder()
                .withVisualVector(v.clone())
                .build();
        AtomicGrapheme b = new AtomicGraphemeBuilder()
                .withVisualVector(v.clone())
                .build();
        Graphemes gs = new GraphemesBuilder()
                .withChild(a)
                .withChild(b)
                .build();
        Weights w = new WeightsBuilder().build();
        Assessment assessment = new AssessmentBuilder().withWeights(w).build();
        double res = Assessor.assess(assessment,(ConnoisseurObject)gs);

        Assert.assertEquals(1.0,res,0.001);
    }

    @Test
    public void assesstest_graphemes_differentelements() throws Exception {
        Shape s = new RegionShapeBuilder().build();
        Shape ss = new IconShapeBuilder().build();
        VisualVector v = new VisualVectorBuilder().withShape(s).build();
        VisualVector vv = new VisualVectorBuilder().withShape(ss).build();
        AtomicGrapheme a = new AtomicGraphemeBuilder()
                .withVisualVector(v.clone())
                .build();
        AtomicGrapheme b = new AtomicGraphemeBuilder()
                .withVisualVector(vv.clone())
                .build();
        Graphemes gs = new GraphemesBuilder()
                .withChild(a)
                .withChild(b)
                .build();
        Weights w = new WeightsBuilder()
                .withShapeWeight(7)
                .build();
        Assessment assessment = new AssessmentBuilder().withWeights(w).build();
        double res = Assessor.assess(assessment,(ConnoisseurObject)gs);

        Assert.assertTrue(res > 0);
    }

    @Test
    public void assesstest_mapping_bestcase() throws Exception {
        AtomicGrapheme a = new AtomicGraphemeBuilder().build();
        Concept c = new ConceptBuilder().build();
        GraphemeMapping gm = new GraphemeMappingBuilder()
                .withMapping(new Pair<>(a,c)).build();
        Weights w = new WeightsBuilder().build();
        Assessment assessment = new AssessmentBuilder().withWeights(w).build();
        double actual = Assessor.assess(assessment,gm);

        Assert.assertEquals(1d,actual,0.001);
    }

    @Test
    public void assesstest_mapping_symbolredundancy() throws Exception {
        AtomicGrapheme a = new AtomicGraphemeBuilder().build();
        AtomicGrapheme b = new AtomicGraphemeBuilder().build();
        Concept c = new ConceptBuilder().build();
        GraphemeMapping gm = new GraphemeMappingBuilder()
                .withMapping(new Pair<>(b,c))
                .withMapping(new Pair<>(a,c)).build();
        Weights w = new WeightsBuilder().build();
        Assessment assessment = new AssessmentBuilder().withWeights(w).build();
        double actual = Assessor.assess(assessment,gm);

        Assert.assertEquals(7d/8d,actual,0.001);
    }

    @Test
    public void assesstest_mapping_symboloverload() throws Exception {
        AtomicGrapheme a = new AtomicGraphemeBuilder().build();
        Concept b = new ConceptBuilder().build();
        Concept c = new ConceptBuilder().build();
        GraphemeMapping gm = new GraphemeMappingBuilder()
                .withMapping(new Pair<>(a,b))
                .withMapping(new Pair<>(a,c)).build();
        Weights w = new WeightsBuilder().build();
        Assessment assessment = new AssessmentBuilder().withWeights(w).build();
        double actual = Assessor.assess(assessment,gm);

        Assert.assertEquals(7d/8d,actual,0.001);
    }

    @Test
    public void assesstest_mapping_symbolexcess() throws Exception {
        AtomicGrapheme a = new AtomicGraphemeBuilder().build();
        AtomicGrapheme b = new AtomicGraphemeBuilder().build();
        Concept c = new ConceptBuilder().build();
        GraphemeMapping gm = new GraphemeMappingBuilder()
                .withUnmappedGrapheme(b)
                .withMapping(new Pair<>(a,c)).build();
        Weights w = new WeightsBuilder().build();
        Assessment assessment = new AssessmentBuilder().withWeights(w).build();
        double actual = Assessor.assess(assessment,gm);

        Assert.assertEquals(7d/8d,actual,0.001);
    }

    @Test
    public void assesstest_mapping_symboldeficit() throws Exception {
        AtomicGrapheme a = new AtomicGraphemeBuilder().build();
        Concept b = new ConceptBuilder().build();
        Concept c = new ConceptBuilder().build();
        GraphemeMapping gm = new GraphemeMappingBuilder()
                .withUnmappedConcept(b)
                .withMapping(new Pair<>(a,c)).build();
        Weights w = new WeightsBuilder().build();
        Assessment assessment = new AssessmentBuilder().withWeights(w).build();
        double actual = Assessor.assess(assessment,gm);

        Assert.assertEquals(7d/8d,actual,0.001);
    }

    @Test
    public void assesstest_mapping_symboldeficitandexcess() throws Exception {
        AtomicGrapheme a = new AtomicGraphemeBuilder().build();
        Concept b = new ConceptBuilder().build();
        GraphemeMapping gm = new GraphemeMappingBuilder()
                .withUnmappedConcept(b)
                .withUnmappedGrapheme(a).build();
        Weights w = new WeightsBuilder().build();
        Assessment assessment = new AssessmentBuilder().withWeights(w).build();
        double actual = Assessor.assess(assessment,gm);

        Assert.assertEquals(0.5,actual,0.001);
    }

    @Test
    public void assesstest_mapping_symboloverloadandredundancy() throws Exception {
        AtomicGrapheme a = new AtomicGraphemeBuilder().build();
        Concept b = new ConceptBuilder().build();
        AtomicGrapheme c = new AtomicGraphemeBuilder().build();
        Concept d = new ConceptBuilder().build();
        GraphemeMapping gm = new GraphemeMappingBuilder()
                .withMapping(new Pair<>(a,b))
                .withMapping(new Pair<>(c,d))
                .withMapping(new Pair<>(a,d))
                .withMapping(new Pair<>(c,b)).build();
        Weights w = new WeightsBuilder().build();
        Assessment assessment = new AssessmentBuilder().withWeights(w).build();
        double actual = Assessor.assess(assessment,gm);

        Assert.assertEquals(0.75,actual,0.001);
    }



}
