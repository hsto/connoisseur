package msc.connoisseur.control;

import msc.connoisseur.model.language.semantics.Concept;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.builders.AtomicGraphemeBuilder;
import msc.connoisseur.builders.ConceptBuilder;
import org.junit.Test;

import java.util.UUID;

/**
 * Created by hartmjep on 30-10-2016.
 */
public class ControllerGraphemeMappingTests {

    @Test
    public void controller_addmappingtest_ok(){
        String conceptid = UUID.randomUUID().toString();
        String graphemeid = UUID.randomUUID().toString();
        AtomicGrapheme a = new AtomicGraphemeBuilder().withId(graphemeid).build();
        Concept c = new ConceptBuilder().withId(conceptid).build();
    }

}
