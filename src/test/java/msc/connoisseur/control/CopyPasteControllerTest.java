package msc.connoisseur.control;

import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.common.Container;
import msc.connoisseur.model.common.DynamicContainer;
import msc.connoisseur.model.language.semantics.Semantics;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.language.syntax.CompoundGrapheme;
import msc.connoisseur.model.language.syntax.Grapheme;
import msc.connoisseur.builders.AtomicGraphemeBuilder;
import msc.connoisseur.builders.CompoundGraphemeBuilder;
import msc.connoisseur.builders.SemanticsBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

/**
 * Created by hartmjep on 21-10-2016.
 */
public class CopyPasteControllerTest {
    private UndoRedoController undoRedoController;
    private Controller controller;
    private CopyPasteController copyPasteController;

    @Before
    public void setUp() throws Exception {
        undoRedoController = new UndoRedoController();
        controller = new Controller(null,undoRedoController);
        copyPasteController = new CopyPasteController(controller);
    }

    @After
    public void tearDown() throws Exception {
        undoRedoController = null;
        controller = null;
    }

    @Test
    public void copyPasteController_copypasteaschild_ok() throws Exception {
        //arrange
        String name = UUID.randomUUID().toString();
        ConnoisseurObject co = new AtomicGraphemeBuilder().withName(name).build();
        ConnoisseurObject parent = new CompoundGraphemeBuilder().build();
        Controller controller = new Controller(null,new UndoRedoController());
        CopyPasteController copyPasteController = new CopyPasteController(controller);
        //act
        copyPasteController.copy(co);
        copyPasteController.paste(parent);

        //assert
        DynamicContainer<Grapheme> container = (DynamicContainer<Grapheme>) parent;
        Assert.assertEquals(co.getName(),container.get(0).getName());
    }

    @Test
    public void copyPasteController_copypasteaschild_invalidparent() throws Exception {
        //arrange
        String name = UUID.randomUUID().toString();
        ConnoisseurObject co = new AtomicGraphemeBuilder().withName(name).build();
        ConnoisseurObject parent = new SemanticsBuilder().build();

        //act
        copyPasteController.copy(co);
        copyPasteController.paste(parent);

        //assert
        Container<ConnoisseurObject> container = (Container<ConnoisseurObject>) parent;
        for (ConnoisseurObject child : container.getList()) {
            Assert.assertNotEquals(name,child.getName());
        }

    }

    @Test
    public void copyPasteController_copypastesingleton_clonessingleton() throws Exception {
        //arrange
        String name = UUID.randomUUID().toString();
        Semantics semanticsA = new SemanticsBuilder().withName(name).build();
        Semantics semanticsB = new SemanticsBuilder().build();
        Controller controllerMock = new Controller(null,undoRedoController){
            @Override
            public <T extends ConnoisseurObject> void cloneInto(T destination, T source) {
                destination.loadInto(source);
            }
        };
        CopyPasteController copyPaster = new CopyPasteController(controllerMock);

        //act
        copyPaster.copy(semanticsA);
        copyPaster.paste(semanticsB);

        //assert
        Assert.assertEquals(name,semanticsB.getName());

    }

    @Test
    public void copyPasteController_copypaste_cloneschildren() throws Exception {
        //arrange
        String name = UUID.randomUUID().toString();
        AtomicGrapheme child = new AtomicGraphemeBuilder().withName(name).build();
        CompoundGrapheme compA = new CompoundGraphemeBuilder().withChild(child).build();
        CompoundGrapheme compB = new CompoundGraphemeBuilder().build();

        //act
        copyPasteController.copy(compA);
        copyPasteController.paste(compB);

        //assert
        Assert.assertEquals(name,((CompoundGrapheme)compB.get(0)).get(0).getName());

    }

}
