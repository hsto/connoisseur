package msc.connoisseur.control;

import msc.connoisseur.model.language.syntax.Grapheme;
import msc.connoisseur.builders.AtomicGraphemeBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Jeppe on 29-10-2016.
 */
public class ControllerGraphemeTests {

    private UndoRedoController undoRedoController;
    private Controller controller;

    @Before
    public void setUp() throws Exception {
        undoRedoController = new UndoRedoController();
        controller = new Controller(null,undoRedoController);
    }

    @After
    public void tearDown() throws Exception {
        undoRedoController = null;
        controller = null;
    }

    @Test
    public void controllertest_setname_ok(){
        //arrange
        Grapheme grapheme = new AtomicGraphemeBuilder().withName("").build();
        String name = "testnamesomething";

        //act
        controller.changeName(grapheme,name);

        //assert
        Assert.assertEquals(name,grapheme.getName());
    }

    @Test
    public void controllertest_setdescription_ok(){
        //arrange
        Grapheme grapheme = new AtomicGraphemeBuilder().withDescription("").build();
        String s = "testnamesomething";

        //act
        controller.changeDescription(grapheme,s);

        //assert
        Assert.assertEquals(s,grapheme.getDescription());
    }

    @Test
    public void controllertest_setid_ok(){
        //arrange
        Grapheme grapheme = new AtomicGraphemeBuilder().withId("").build();
        String s = "testnamesomething";

        //act
        controller.changeId(grapheme,s);

        //assert
        Assert.assertEquals(s,grapheme.getId());
    }

}
