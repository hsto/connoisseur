package msc.connoisseur.control;

import msc.connoisseur.model.language.Language;
import msc.connoisseur.builders.LanguageBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Jeppe on 29-10-2016.
 */
public class ControllerLanguageTests {
    private UndoRedoController undoRedoController;
    private Controller controller;

    @Before
    public void setUp() throws Exception {
        undoRedoController = new UndoRedoController();
        controller = new Controller(null,undoRedoController);
    }

    @After
    public void tearDown() throws Exception {
        undoRedoController = null;
        controller = null;
    }

    @Test
    public void controllertest_setname_ok(){
        //arrange
        Language language = new LanguageBuilder().withName("").build();
        String name = "testnamesomething";

        //act
        controller.changeName(language,name);

        //assert
        Assert.assertEquals(name,language.getName());
    }

    @Test
    public void controllertest_setdescription_ok(){
        //arrange
        Language language = new LanguageBuilder().withDescription("").build();
        String s = "testnamesomething";

        //act
        controller.changeDescription(language,s);

        //assert
        Assert.assertEquals(s,language.getDescription());
    }

    @Test
    public void controllertest_setid_ok(){
        //arrange
        Language language = new LanguageBuilder().withId("").build();
        String s = "testnamesomething";

        //act
        controller.changeId(language,s);

        //assert
        Assert.assertEquals(s,language.getId());
    }

}
