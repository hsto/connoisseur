package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.language.syntax.Grapheme;
import msc.connoisseur.model.language.syntax.Graphemes;

/**
 * Created by Jeppe on 30-10-2016.
 */
public class GraphemesBuilder implements Builder<Graphemes> {
    private Graphemes graphemes;
    public GraphemesBuilder(){
        this.graphemes = new Graphemes();
    }

    @Override
    public Graphemes build() {
        return graphemes;
    }

    public GraphemesBuilder withChild(Grapheme a) {
        graphemes.add(a);
        return this;
    }
}
