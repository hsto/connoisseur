package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.bertin.RegionShape;

/**
 * Created by Jeppe on 30-10-2016.
 */
public class RegionShapeBuilder implements Builder<RegionShape> {
    RegionShape regionShape;
    public RegionShapeBuilder(){
        regionShape = new RegionShape();
    }

    @Override
    public RegionShape build() {
        return regionShape;
    }
}
