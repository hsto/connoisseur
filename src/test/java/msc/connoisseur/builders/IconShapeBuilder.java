package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.bertin.IconShape;

/**
 * Created by Jeppe on 30-10-2016.
 */
public class IconShapeBuilder implements Builder<IconShape> {
    private IconShape iconShape;
    public IconShapeBuilder(){
        iconShape = new IconShape();
    }
    @Override
    public IconShape build() {
        return this.iconShape;
    }
}
