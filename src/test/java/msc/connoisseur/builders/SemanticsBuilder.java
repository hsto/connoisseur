package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.language.semantics.Semantics;

/**
 * Created by hartmjep on 30-10-2016.
 */
public class SemanticsBuilder implements Builder<Semantics> {
    Semantics semantics;
    public SemanticsBuilder(){
        semantics = new Semantics();
    }
    @Override
    public Semantics build() {
        return semantics;
    }

    public Builder<Semantics> withName(String name) {
        semantics.setName(name);
        return this;
    }
}
