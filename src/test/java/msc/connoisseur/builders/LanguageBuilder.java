package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.language.Language;

/**
 * Created by hartmjep on 30-10-2016.
 */
public class LanguageBuilder implements Builder<Language> {
    private Language language;
    public LanguageBuilder(){
        language = new Language();
    }
    public Builder<Language> withName(String name){
        language.setName(name);
        return this;
    }
    public Builder<Language> withId(String id){
        language.setId(id);
        return this;
    }
    public Builder<Language> withDescription(String description){
        language.setDescription(description);
        return this;
    }
    @Override
    public Language build() {
        return language;
    }
}
