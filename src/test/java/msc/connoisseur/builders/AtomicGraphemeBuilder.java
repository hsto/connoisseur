package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.language.syntax.AtomicGrapheme;
import msc.connoisseur.model.visualvector.VisualVector;

/**
 * Created by Jeppe on 29-10-2016.
 */
public class AtomicGraphemeBuilder implements Builder<AtomicGrapheme> {
    AtomicGrapheme g;
    public AtomicGraphemeBuilder(){
        g = new AtomicGrapheme();
    }
    public AtomicGrapheme build(){
        return g;
    }

    public AtomicGraphemeBuilder withName(String s) {
        g.setName(s);
        return this;
    }
    public AtomicGraphemeBuilder withDescription(String s) {
        g.setDescription(s);
        return this;
    }
    public AtomicGraphemeBuilder withId(String s) {
        g.setId(s);
        return this;
    }

    public AtomicGraphemeBuilder withVisualVector(VisualVector visualVector) {
        g.setVisualVector(visualVector);
        return this;
    }
}
