package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.ConnoisseurObject;
import msc.connoisseur.model.assessment.Assessment;
import msc.connoisseur.model.configuration.Weights;

/**
 * Created by Jeppe on 30-10-2016.
 */
public class AssessmentBuilder implements Builder<Assessment> {
    private Assessment assessment;
    public AssessmentBuilder(){
        assessment = new Assessment();
    }
    public AssessmentBuilder withWeights(Weights weights){
        assessment.setWeights(weights);
        return this;
    }
    public AssessmentBuilder withScope(ConnoisseurObject scope){
        assessment.setScope(scope);
        return this;
    }
    @Override
    public Assessment build() {
        return assessment;
    }

    public AssessmentBuilder withAggregationMethod(Assessment.AggregationMethods.Method method) {
        assessment.setAggregationMethod(method);
        return this;
    }
}
