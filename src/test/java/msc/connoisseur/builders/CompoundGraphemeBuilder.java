package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.language.syntax.CompoundGrapheme;
import msc.connoisseur.model.language.syntax.Grapheme;

/**
 * Created by hartmjep on 30-10-2016.
 */
public class CompoundGraphemeBuilder implements Builder<CompoundGrapheme> {
    CompoundGrapheme compoundGrapheme;
    public CompoundGraphemeBuilder(){
        compoundGrapheme = new CompoundGrapheme();
    }
    @Override
    public CompoundGrapheme build() {
        return compoundGrapheme;
    }

    public CompoundGraphemeBuilder withChild(Grapheme child) {
        compoundGrapheme.add(child);
        return this;
    }
}
