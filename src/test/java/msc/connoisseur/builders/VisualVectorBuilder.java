package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.bertin.Size;
import msc.connoisseur.model.visualvector.VisualVector;

/**
 * Created by Jeppe on 30-10-2016.
 */
public class VisualVectorBuilder implements Builder<VisualVector> {
    private VisualVector visualVector;
    public VisualVectorBuilder(){
        visualVector = new VisualVector();
    }
    public VisualVectorBuilder withShape(Shape s){
        visualVector.setShape(s);
        return this;
    }

    public VisualVectorBuilder withSize(Size size) {
        visualVector.setSize(size);
        return this;
    }

    @Override
    public VisualVector build() {
        return visualVector;
    }
}
