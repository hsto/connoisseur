package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.language.semantics.Concept;

/**
 * Created by hartmjep on 30-10-2016.
 */
public class ConceptBuilder implements Builder<Concept> {

    private Concept concept;

    public ConceptBuilder(){
        concept = new Concept();
    }

    public Builder<Concept> withName(String name){
        concept.setName(name);
        return this;
    }

    public Builder<Concept> withDescription(String description){
        concept.setDescription(description);
        return this;
    }

    public Builder<Concept> withId(String id){
        concept.setId(id);
        return this;
    }

    public Builder<Concept> withReference(String reference){
        concept.setReference(reference);
        return this;
    }
    @Override
    public Concept build() {
        return concept;
    }
}
