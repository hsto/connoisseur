package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.common.Pair;
import msc.connoisseur.model.language.Language;
import msc.connoisseur.model.language.semantics.Concept;
import msc.connoisseur.model.language.semantics.GraphemeMapping;
import msc.connoisseur.model.language.syntax.Grapheme;

import java.util.List;

/**
 * Created by Jeppe on 30-10-2016.
 */
public class GraphemeMappingBuilder implements Builder<GraphemeMapping> {
    private GraphemeMapping graphemeMapping;
    public GraphemeMappingBuilder(){

        graphemeMapping = new GraphemeMapping();
        Language l = new Language();
        l.getSemantics().setGraphemeMapping(graphemeMapping);
    }

    @Override
    public GraphemeMapping build() {
        return graphemeMapping;
    }

    public GraphemeMappingBuilder withMapping(Pair<Grapheme, Concept> pair) {
        List<Concept> concepts = graphemeMapping.getParent().getElements().getConceptList();
        List<Grapheme> graphemes = graphemeMapping.getParent().getParent().getSyntax().getGraphemes().getList();
        if(!concepts.contains(pair.b))graphemeMapping.getParent().getElements().add(pair.b);
        if(!graphemes.contains(pair.a))graphemeMapping.getParent().getParent().getSyntax().getGraphemes().add(pair.a);
        graphemeMapping.add(pair);
        return this;
    }

    public GraphemeMappingBuilder withUnmappedGrapheme(Grapheme grapheme) {
        graphemeMapping.getParent().getParent().getSyntax().getGraphemes().add(grapheme);
        return this;
    }

    public GraphemeMappingBuilder withUnmappedConcept(Concept concept) {
        graphemeMapping.getParent().getElements().add(concept);
        return this;
    }
}
