package msc.connoisseur.builders;

import javafx.util.Builder;
import msc.connoisseur.model.bertin.Shape;
import msc.connoisseur.model.configuration.Weights;

/**
 * Created by Jeppe on 30-10-2016.
 */
public class WeightsBuilder implements Builder<Weights> {
    Weights weights;
    public WeightsBuilder(){
        weights = new Weights();
    }

    @Override
    public Weights build() {
        return weights;
    }

    public WeightsBuilder withShapeWeight(int i) {
        weights.setWeight(Shape.class,i);
        return this;
    }
}
