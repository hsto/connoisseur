package msc.connoisseur.builders.visual;

import javafx.util.Builder;
import javafx.util.Pair;
import msc.connoisseur.model.bertin.Size;
import msc.connoisseur.model.language.syntax.Grapheme;

/**
 * Created by Jeppe on 31-10-2016.
 */
public class SizeBuilder implements Builder<Size> {
    private Size size;
    public SizeBuilder(){
        size = new Size(Size.SizeValue.average,null);
    }
    public SizeBuilder withSize(Size.SizeValue size){
        this.size.setValue(new Pair<>(size,this.size.getValue().getValue()));
        return this;
    }
    public SizeBuilder withReference(Grapheme g){
        this.size.setValue(new Pair<>(this.size.getValue().getKey(),g));
        return this;
    }
    @Override
    public Size build() {
        return size;
    }
}
