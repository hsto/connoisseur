# Connoisseur

A tool to analyse the usability qualities of visual notations.

## Build

Install Java 11 and make sure that `java -version` points to the Java 11 installation. 

Run `gradlew tasks` for an overview of supported Gradle tasks. Usually you will need the `build` and `run` task.

You can create a distributable runnable application using `gradlew jlink` or `gradlew jlinkZip`.
